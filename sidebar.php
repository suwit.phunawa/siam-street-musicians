<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
		<i class="fas fa-receipt"></i>
		<div class="sidebar-brand-text mx-3">ขายสินค้า</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item <?php if ($activePage == "BILLING") {
							echo "active";
						} ?>">
		<a class="nav-link" href="./billing.php" style="font-size: 10px !important;">
			<i class="fas fa-receipt"></i>
			<span>สั่งสินค้า</span>
		</a>
	</li>


	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item <?php if ($activePage == "CUSTOMER") {
							echo "active";
						} ?>">
		<a class="nav-link" href="./customer.php">
			<i class="fas fa-users"></i>
			<span>จัดการลูกค้า</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item  <?php if ($activePage == "PRODUCT") {
								echo "active";
							} ?>">
		<a class="nav-link" href="./product.php">
			<i class="fas fa-box-open"></i>
			<span>จัดการสินค้า</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item  <?php if ($activePage == "PRICE") {
								echo "active";
							} ?>">
		<a class="nav-link" href="./price.php">
			<i class="fas fa-dollar-sign"></i>
			<span>กำหนดราคาสินค้า</span>
		</a>
	</li>




	<!-- Divider -->
	<hr class="sidebar-divider">


	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item  <?php if ($activePage == "ORDER") {
								echo "active";
							} ?>">
		<a class="nav-link" href="./order.php">
			<i class="fas fa-list-ul"></i>
			<span>รายการสั่งสินค้า</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item  <?php if ($activePage == "CHECKPRICE") {
								echo "active";
							} ?>">
		<a class="nav-link" href="./check_price.php">
			<i class="fas fa-check-square"></i>
			<span>เช็คราคาสินค้า</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item  <?php if ($activePage == "REPORT") {
								echo "active";
							} ?>">
		<a class="nav-link" href="./print_report.php">
			<i class="fas fa-chart-bar"></i>
			<span>ออกรายงาน</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link" href="./">
			<i class="fas fa-tv"></i>
			<span>กลับเมนูหลัก</span>
		</a>
	</li>


	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>