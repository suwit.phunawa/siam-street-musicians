<?php
@session_start();

require_once __DIR__ . '/mpdf8/vendor/autoload.php';
//require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
ob_start(); // ทำการเก็บค่า html นะครับ

include("function/FunctionDate.php");
include("function/connect.php");


$query = "
			SELECT a.od_date, b.cus_name, b.cus_add, b.cus_tel
			FROM order_p a, customer b
			WHERE a.cus_id = b.cus_id and a.od_code = '" . $_GET["code"] . "'
      GROUP BY a.od_date, b.cus_name, b.cus_add, b.cus_tel
      ORDER BY a.od_id ASC
	";
$result = mysqli_query($c, $query);
$row = mysqli_fetch_array($result);
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
</head>

<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="19" colspan="3"></td>
    </tr>
    <tr>
      <td width="110px">
        <img src="images/company_logo.jpg" width="100px" />
      </td>
      <td style="font-size: 14px;">
        บริษัท ทวีสิน เบเกอรี่ จำกัด<br />
        62 หมู่ 5 ต.ดอนไก่ดี อ.กระทุ่มแบน จ.สมุทรสาคร 74110<br />
        โทร. 034-878732 034-878552 086-8133039<br />
      </td>
      <td style=" border: solid 2px #999999; padding:5px;" width="255px">
        <table style="font-size: 14px;">
          <tr>
            <td width="65px"><b>วันที่</b></td>
            <td>:</td>
            <td><?php echo dateThai($row["od_date"]); ?></td>
          </tr>
          <tr>
            <td><b>ชื่อลูกค้า</b></td>
            <td>:</td>
            <td><?php echo $row["cus_name"]; ?></td>
          </tr>
          <tr>
            <td valign="top"><b>ที่อยู่</b></td>
            <td valign="top">:</td>
            <td><?php echo $row["cus_add"]; ?> </td>
          </tr>
          <tr>
            <td><b>โทร.</b></td>
            <td>:</td>
            <td><?php echo $row["cus_tel"]; ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><br></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">

        <?php
        include("function/connect.php");

        $currentPage = $_SERVER["PHP_SELF"];

        $maxRows_rsmem = 30;
        $pageNum_rsmem = 0;
        if (isset($_GET['pageNum_rsmem'])) {
          $pageNum_rsmem = $_GET['pageNum_rsmem'];
        }
        $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


        if (isset($_GET['pageNum_rsmem'])) {
          $pageNum_rsmem = $_GET['pageNum_rsmem'];
        }
        $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
        //คิวรี่ปกติ
        $query_rsmem = "
			SELECT *
			FROM order_p a, product b
			WHERE a.pd_id = b.pd_id and a.od_code = '$_GET[code]' ORDER BY a.od_id ASC
";
        $query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

        $rsmem = mysqli_query($c, $query_limit_rsmem);
        $row_rsmem = mysqli_fetch_assoc($rsmem);

        if (isset($_GET['totalRows_rsmem'])) {
          $totalRows_rsmem = $_GET['totalRows_rsmem'];
        } else {
          $all_rsmem = mysqli_query($c, $query_rsmem);
          $totalRows_rsmem = mysqli_num_rows($all_rsmem);
        }
        $totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

        $queryString_rsmem = "";
        if (!empty($_SERVER['QUERY_STRING'])) {
          $params = explode("&", $_SERVER['QUERY_STRING']);
          $newParams = array();
          foreach ($params as $param) {
            if (
              stristr($param, "pageNum_rsmem") == false &&
              stristr($param, "totalRows_rsmem") == false
            ) {
              array_push($newParams, $param);
            }
          }
          if (count($newParams) != 0) {
            $queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
          }
        }
        $queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

        ?>


      </td>
    </tr>
  </table>

  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr style="font-weight:bold; font-size:19px;">
      <td style="font-weight:bold; font-size:19px;" align="center">ลำดับ</td>
      <td style="font-weight:bold; font-size:19px;">ชื่อสินค้า</td>
      <td style="font-weight:bold; font-size:19px;" align="center">จำนวน</td>
      <td style="font-weight:bold; font-size:19px;" align="center">หน่วย</td>
      <td style="font-weight:bold; font-size:19px;" align="right">ราคา/หน่วย</td>
      <td style="font-weight:bold; font-size:19px;" align="right">จำนวนเงิน</td>
    </tr>
    <tr>
      <td colspan="6">
        <hr />
      </td>
    </tr>

    <?php
    $total_total = 0;
    $num = 0;
    //เริ่มวน

    if ($totalRows_rsmem > 0) { // Show if recordset not empty 
    ?>
      <?php do {

      ?>

        <tr>
          <td width="10%" align="center">
            <div><span style="font-size: 19px"><?php echo $num + 1; ?></span></div>
          </td>
          <td width="40%" height="30px">
            <div align="left"><span style="font-size: 19px"><?php echo "$row_rsmem[pd_name]"; ?></span></div>
          </td>
          <td width="10%" align="center">
            <div><span style="font-size: 19px"><?php echo "$row_rsmem[od_num]"; ?></span></div>
          </td>
          <td width="10%" align="center">
            <div><span style="font-size: 19px"><?php echo "$row_rsmem[pd_unit]"; ?></span></div>
          </td>
          <td width="15%" align="right">
            <div><span style="font-size: 19px"><?php echo number_format($row_rsmem["od_priceperunit"], 2); ?></span></div>
          </td>
          <td width="15%" align="right">
            <div><span style="font-size: 19px"><?php echo number_format($row_rsmem["od_total"], 2); ?></span></div>
          </td>
        </tr>
      <?php
        $total_total = $total_total + $row_rsmem["od_total"];
        $num++;
      } while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
    <?php } // Show if recordset not empty 
    ?>
    <?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
    ?>
    <?php } // Show if recordset empty 
    ?>


  </table>

  <div style="position: absolute; bottom:190px; right: 45px; left: 45px;">
    <table width="100%">
      <tr>
        <td colspan="2">
          <hr />
        </td>
      </tr>
      <tr>
        <td align="right" style="font-size:19px; font-weight:bold;">
          รวมเป็นเงิน :
        </td>
        <td width="20%" style="font-size: 23px; font-weight:bold;" align="right">
          <?php echo number_format($total_total, 2); ?>
        </td>
      </tr>
    </table>
  </div>
  <div style="position: absolute; bottom:85px; left: 45px; right:45px;">
    <table width="100%" border="0">
      <tr>
        <td width="1%"></td>
        <td width="43%"></td>
        <td width="11%"></td>
        <td width="1%"></td>
        <td width="43%"></td>
        <td width="1%"></td>
      </tr>
      <tr>
        <td colspan="3" style=" font-size:19px;">ผู้ส่ง</td>
        <td colspan="3" style=" font-size:19px;">ผู้รับ</td>
      </tr>
      <tr>
        <td style="height:40px;"></td>
        <td style="border-bottom:solid 1px black;"></td>
        <td></td>
        <td></td>
        <td style="border-bottom:solid 1px black;"></td>
        <td></td>
      </tr>
      <tr>
        <td style=" font-size:19px; padding-top:8px;">(</td>
        <td></td>
        <td style=" font-size:19px; padding-top:8px;">)</td>
        <td style=" font-size:19px; padding-top:8px;">(</td>
        <td></td>
        <td style=" font-size:19px; padding-top:8px;">)</td>
      </tr>
    </table>
  </div>
</body>

</html>

<?php

$html = ob_get_contents();
$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
ob_end_clean();
/*$pdf = new mPDF('th', '', '0', '',  5, 5, 5, 5, 0, 0); //การตั้งค่ากระดาษถ้าต้องการแนวตั้ง ก็ A4 เฉยๆครับ ถ้าต้องการแนวนอนเท่ากับ A4-L
$pdf->SetAutoFont();
$pdf->SetDisplayMode('fullpage');
$pdf->WriteHTML($html, 2);*/

$mpdf = new \Mpdf\Mpdf([
  'mode' => '',
  'format' => 'A4',
  'margin_left' => 10,
  'margin_right' => 10,
  'margin_top' => 10,
  'margin_bottom' => 10,
  'margin_header' => 6,
  'margin_footer' => 6,
  'orientation' => 'P',
]);
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont   = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
