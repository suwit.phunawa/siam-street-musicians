<?php @session_start();
include("function/connect.php");
?>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <style type="text/css">
    td {
      font-family: Tahoma;
      font-size: 14px;
      color: #828282;
      text-decoration: none;
      border: none;
    }

    font {
      font-family: Tahoma;
      font-size: 14px;
      color: #1890E1;
      text-decoration: none;
      border: none;
      font-weight: bold;
    }

    .f {
      font-family: Tahoma;
      font-size: 14px;
      color: #FF2D00;
      text-decoration: none;
      border: none;
      font-weight: normal;
    }

    a {
      font-family: Tahoma;
      font-size: 14px;
      font-weight: bold;
      color: #FB5E3C;
      text-decoration: none;
      border: none;
    }

    .a {
      font-family: Tahoma;
      font-size: 14px;
      font-weight: normal;
      color: #FFFFFF;
      text-decoration: none;
      border: none;
    }

    .a1 {
      font-family: Tahoma;
      font-size: 14px;
      color: #40A3D8;
      font-weight: normal;
      text-decoration: none;
      border: none;
    }

    img {
      text-decoration: none;
      border: none;
    }
  </style>

  <title>ORDER SYSTEM</title>
</head>

<body bgcolor="#FFFFFF">

  <body>

    <table width="200" border="0" align="center" cellpadding="1" cellspacing="1">
      <tr>
        <td colspan="2"><img src="images/top.jpg"></td>
      </tr>
      <tr>
        <td colspan="2">
          <img src="images/head.jpg" width="760" height="133" alt="" />
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <hr noshade="noshade" color="#C7C7C7">
        </td>
      </tr>
      <tr>
        <td background="images/bg.jpg" valign="top">
          <!--เริ่มตารางเมนู-->
          <?php include("menu.php"); ?>
          <!--จบตารางเมนู-->
        </td>
        <td valign="top">
          <!--เริ่มตารางเนื้อหา-->

          <table width="588" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td width="47"><img src="images/Book_edit.png" width="44" height="44" alt="" /></td>
              <td width="541"><span style="color: #F89302;">
                  <h2>รายการสั่งสินค้า</h2>
                </span></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">
                <hr noshade="noshade" color="#C7C7C7">
              </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2">

                <?php
                include("function/connect.php");

                $currentPage = $_SERVER["PHP_SELF"];

                $maxRows_rsmem = 30;
                $pageNum_rsmem = 0;
                if (isset($_GET['pageNum_rsmem'])) {
                  $pageNum_rsmem = $_GET['pageNum_rsmem'];
                }
                $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


                if (isset($_GET['pageNum_rsmem'])) {
                  $pageNum_rsmem = $_GET['pageNum_rsmem'];
                }
                $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
                //คิวรี่ปกติ
                $query_rsmem = "
                            SELECT a.od_code, a.cus_name, SUM(a.od_total) as total
                            FROM order_p a
                            LEFT JOIN customer b ON a.cus_id = b.cus_id
                            WHERE a.od_status = '2' and a.cus_name is not null
                            group by a.od_code, a.cus_name
                            order by a.od_code desc
                                  ";
                $query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

                $rsmem = mysqli_query($c, $query_limit_rsmem);
                $row_rsmem = mysqli_fetch_assoc($rsmem);

                if (isset($_GET['totalRows_rsmem'])) {
                  $totalRows_rsmem = $_GET['totalRows_rsmem'];
                } else {
                  $all_rsmem = mysqli_query($c, $query_rsmem);
                  $totalRows_rsmem = mysqli_num_rows($all_rsmem);
                }
                $totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

                $queryString_rsmem = "";
                if (!empty($_SERVER['QUERY_STRING'])) {
                  $params = explode("&", $_SERVER['QUERY_STRING']);
                  $newParams = array();
                  foreach ($params as $param) {
                    if (
                      stristr($param, "pageNum_rsmem") == false &&
                      stristr($param, "totalRows_rsmem") == false
                    ) {
                      array_push($newParams, $param);
                    }
                  }
                  if (count($newParams) != 0) {
                    $queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
                  }
                }
                $queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

                ?>
                <table id="box-table-a" width="100%" border="0" cellpadding="1" cellspacing="1" align="center">
                  <tr class="a">
                    <th width="145" align="center" valign="middle" bgcolor="#B5A8A8">
                      <h4>วันที่ยืนยัน</h4>
                    </th>
                    <th width="200" align="center" valign="middle" bgcolor="#B5A8A8">
                      <h4>ชื่อลูกค้า</h4>
                    </th>
                    <th width="88" align="center" valign="middle" bgcolor="#B5A8A8">
                      <h4>ราคารวม</h4>
                    </th>
                    <th width="60" align="center" valign="middle" bgcolor="#B5A8A8">
                      <h4>ลบ</h4>
                    </th>
                    <th width="180" align="center" valign="middle" bgcolor="#B5A8A8">
                      <h4>พิมพ์</h4>
                    </th>
                  </tr>

                  <?php


                  //เริ่มวน
                  $unit_total = 0;
                  $price_total = 0;

                  if ($totalRows_rsmem > 0) { // Show if recordset not empty 
                  ?>
                    <?php do {
                      $odCode = $row_rsmem["od_code"];
                    ?>
                      <tr>
                        <td>
                          <div align="left"><?php echo "$row_rsmem[od_code]"; ?></div>
                        </td>
                        <td>
                          <div align="left"><?php echo "$row_rsmem[cus_name]"; ?></div>
                        </td>
                        <td>
                          <div align="right"><?php echo number_format($row_rsmem["total"], 2, '.', ','); ?></div>
                        </td>
                        <td align="center">
                          <img src="images/delete_icon.png" width="30" height="30" onclick="deleteOrder('<?php echo $odCode; ?>')" />
                        </td>
                        <td>
                          <div align="center">
                            <a style="display:none;" href="prints.php?code=<?php echo "$row_rsmem[od_code]"; ?>" target="_blank"><img src="images/prints.png" width="32" height="32" alt="พิมพ์แบบฟอร์มเล็ก" /></a>
                            <a href="printl_general.php?code=<?php echo "$row_rsmem[od_code]"; ?>" target="_blank"><img src="images/printl.jpg" width="38" height="38" alt="พิมพ์แบบฟอร์มใหญ่" /></a>
                            <a href="printl_general_company.php?code=<?php echo "$row_rsmem[od_code]"; ?>" target="_blank"><img src="images/printl2.png" width="38" height="38" alt="พิมพ์แบบฟอร์มใหญ่" /></a>
                            <a href="printl_general_vat.php?code=<?php echo $row_rsmem["od_code"]; ?>" target="_blank"><img src="images/printl3.png" width="38" height="38" alt="พิมพ์แบบฟอร์มใหญ่" /></a>
                          </div>
                        </td>
                      </tr>
                    <?php
                    } while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
                  <?php } // Show if recordset not empty 
                  ?>
                  <?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
                  ?>
                  <?php } // Show if recordset empty 
                  ?>
                </table>
                <br />
                <p align="center" class="style16">&nbsp;
                  จำนวน <?php echo ($startRow_rsmem + 1) ?> ถึง <?php echo min($startRow_rsmem + $maxRows_rsmem, $totalRows_rsmem) ?> จาก <?php echo $totalRows_rsmem ?></p>
                <table border="0" width="50%" align="center">
                  <tr>
                    <td width="23%" height="47" align="center"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
                                                                ?>
                        <a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, 0, $queryString_rsmem); ?>" class="style16">หน้าแรก</a>
                      <?php } // Show if not first page 
                      ?>
                    </td>
                    <td width="31%" align="center"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
                                                    ?>
                        <a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, max(0, $pageNum_rsmem - 1), $queryString_rsmem); ?>" class="style16">ก่อนหน้า</a>
                      <?php } // Show if not first page 
                      ?>
                    </td>
                    <td width="23%" align="center"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
                                                    ?>
                        <a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, min($totalPages_rsmem, $pageNum_rsmem + 1), $queryString_rsmem); ?>" class="style16">ถัดไป</a>
                      <?php } // Show if not last page 
                      ?>
                    </td>
                    <td width="23%" align="center"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
                                                    ?>
                        <a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, $totalPages_rsmem, $queryString_rsmem); ?>" class="style16">หน้าสุดท้าย</a>
                      <?php } // Show if not last page 
                      ?>
                    </td>
                  </tr>
                </table>


              </td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;
              </td>
            </tr>
            <tr>
              <td height="100%" colspan="2"></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;
              </td>
            </tr>
          </table>

          <!--จบตารางเนื้อหา-->
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <!--เริ่มตารางเมนู-->
          <?php include("menu_under.php"); ?>
          <!--จบตารางเมนู-->
        </td>
      </tr>
    </table>


    <script>
      function deleteOrder(od_code) {
        if (confirm("ยืนยันการลบ รายการสั่งซื้อ ?")) {
          window.location = 'delete_order.php?od_code=' + od_code;
        }
      }
    </script>

  </body>

</html>