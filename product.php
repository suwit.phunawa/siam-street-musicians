<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: admin.php");
}

$activePage = "PRODUCT";

?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<title>ORDER SYSTEM</title>

	<!-- Custom fonts for this template-->
	<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

	<style type="text/css">
		td {
			font-family: Tahoma;
			font-size: 14px;
			color: #828282;
			text-decoration: none;
			border: none;
		}

		font {
			font-family: Tahoma;
			font-size: 14px;
			color: #1890E1;
			text-decoration: none;
			border: none;
			font-weight: bold;
		}

		.f {
			font-family: Tahoma;
			font-size: 14px;
			color: #FF2D00;
			text-decoration: none;
			border: none;
			font-weight: normal;
		}

		a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: bold;
			color: #FB5E3C;
			text-decoration: none;
			border: none;
		}

		.a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: normal;
			color: #FFFFFF;
			text-decoration: none;
			border: none;
		}

		.a1 {
			font-family: Tahoma;
			font-size: 14px;
			color: #40A3D8;
			font-weight: normal;
			text-decoration: none;
			border: none;
		}

		img {
			text-decoration: none;
			border: none;
		}
	</style>
	<script language="JavaScript">
		//****************************** Function ตรวจสอบข้อมูลใน TextBox ******************************
		function SubmitF() {
			var msg = '';
			var me = document.form1;
			if (me.txtName.value == '') {
				msg = ' กรุณาใส่ชื่อ - นามสกุล ! \n';
			}
			if (me.txtUnit.value == '') {
				msg += ' กรุณาใส่หน่วย ! \n';
			}
			if (msg != '') {
				alert(msg);
			} else {
				me.submit();
			}
		}

		function P_edit(id, name, unitp) {
			var me = document.form1;
			me.txtMode.value = 'edit';
			me.txtId.value = id;
			me.txtName.value = name;
			me.txtUnit.value = unitp;
		}

		function P_delete(id) {
			var me = document.form1;
			me.txtMode.value = 'delete';
			me.txtId.value = id;
			me.submit();
		}

		//***************************** End Function ***************************************************
	</script>
	<title>ORDER SYSTEM</title>
</head>

<body bgcolor="#FFFFFF">
	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "sidebar.php"; ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column ">

			<!-- Main Content -->
			<div class="container-fluid container-md">
				<br />
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">จัดการสินค้า</h1>
				</div>

				<!-- Content Row -->
				<div class="row">

					<!-- Earnings (Monthly) Card Example -->
					<div class="col-xl-12 col-md-12 mb-4">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">

								<div class="row no-gutters">
									<div class="col-md-12">
										<div class="p-0">

											<table width="90%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td colspan="2">
														<h4>เพิ่ม/แก้ไข สินค้า</h4>

														<form name="form1" method="post" action="p_product.php">
															<table width="450" border="0" cellspacing="0" cellpadding="5">
																<tr>
																	<td width="98">&nbsp;</td>
																	<td width="462"><input type="hidden" name="txtId" /><input type="hidden" name="txtMode" value="add" /></td>
																	<td width="24">&nbsp;</td>
																</tr>
																<tr>
																	<td>
																		<div align="right">ชื่อสินค้า <span class="style8" style="color: #F80307">* </span> : </div>
																	</td>
																	<td><input type="text" name="txtName" maxlength="200" size="60" class="form-control" /></td>
																</tr>
																<tr>
																	<td>
																		<div align="right">หน่วย <span class="style8" style="color: #F80307">* </span> : </div>
																	</td>
																	<td><input type="text" name="txtUnit" maxlength="20" size="25" class="form-control" /></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																	<td colspan="2">
																		<div align="center" class="mt-3">
																			<input type="button" name="submit1" value="  Save  " onClick="SubmitF();" class="btn btn-primary">
																			&nbsp;&nbsp;&nbsp;
																			<input type="reset" name="submit2" value="  Reset  " class="btn btn-dark">
																		</div>
																	</td>
																</tr>
															</table>
														</form>

													</td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;</td>
												</tr>
												<tr>
													<td colspan="2">
														<hr />
														<br /><br />
														<?php
															$titleName = "รายชื่อลูกค้า";
															include ('helper/search_by_product_name.php');
														?>

										</div>
										<?php
										include("function/connect.php");

										$currentPage = $_SERVER["PHP_SELF"];

										$maxRows_rsmem = 20;
										$pageNum_rsmem = 0;
										if (isset($_GET['pageNum_rsmem'])) {
											$pageNum_rsmem = $_GET['pageNum_rsmem'];
										}
										$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


										if (isset($_GET['pageNum_rsmem'])) {
											$pageNum_rsmem = $_GET['pageNum_rsmem'];
										}
										$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
										//คิวรี่ปกติ
										$query_rsmem = "SELECT *
											FROM product
											WHERE pd_status = '1' and pd_name like '%$searchText%' ";
										$query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

										$rsmem = mysqli_query($c, $query_limit_rsmem);
										$row_rsmem = mysqli_fetch_assoc($rsmem);

										if (isset($_GET['totalRows_rsmem'])) {
											$totalRows_rsmem = $_GET['totalRows_rsmem'];
										} else {
											$all_rsmem = mysqli_query($c, $query_rsmem);
											$totalRows_rsmem = mysqli_num_rows($all_rsmem);
										}
										$totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

										$queryString_rsmem = "";
										if (!empty($_SERVER['QUERY_STRING'])) {
											$params = explode("&", $_SERVER['QUERY_STRING']);
											$newParams = array();
											foreach ($params as $param) {
												if (
													stristr($param, "pageNum_rsmem") == false &&
													stristr($param, "totalRows_rsmem") == false
												) {
													array_push($newParams, $param);
												}
											}
											if (count($newParams) != 0) {
												$queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
											}
										}
										$queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

										?>
										<table id="box-table-a" width="100%" border="0" cellpadding="1" cellspacing="1" align="center" class="table table-bordered">
											<tr class="a">
												<th width="335" align="center" valign="middle" bgcolor="#B5A8A8">
													<h5>ชื่อสินค้า</h5>
												</th>
												<th width="177" align="center" valign="middle" bgcolor="#B5A8A8">
													<h5>หน่วย</h5>
												</th>
												<th width="66" align="center" valign="middle" bgcolor="#B5A8A8">
													<h5>จัดการ</h5>
												</th>
											</tr>
											<?php


											//เริ่มวน

											if ($totalRows_rsmem > 0) { // Show if recordset not empty 
											?>
												<?php do {
													$pdId = $row_rsmem["pd_id"];
													$pdName = $row_rsmem["pd_name"];
													$pdUnit = $row_rsmem["pd_unit"];
												?>
													<tr>
														<td>
															<div align="left"><?php echo $pdName; ?></div>
														</td>
														<td>
															<div align="left"><?php echo $pdUnit; ?></div>
														</td>
														<td>
															<div align="center" class="style10 ">
																<button type="button" class="btn btn-dark" alt="แก้ไข" width="16" height="16" style="cursor:hand" onclick="P_edit('<?php echo $pdId ?>','<?php echo $pdName; ?>','<?php echo $pdUnit; ?>');">
																	<i class="fas fa-pen"></i>
																</button>


																<button type="button" class="btn btn-danger" alt="ลบ" width="16" height="16" style="cursor:hand" onclick="P_delete('<?php echo $pdId; ?>');">
																	<i class="fas fa-trash"></i>
																</button>
															</div>
														</td>
													</tr>
												<?php } while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
											<?php } // Show if recordset not empty 
											?>
											<?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
											?>
											<?php } // Show if recordset empty 
											?>
										</table>

										<br />
										<p align="right" class="style16">&nbsp;
											จำนวน <?php echo ($startRow_rsmem + 1) ?> ถึง <?php echo min($startRow_rsmem + $maxRows_rsmem, $totalRows_rsmem) ?> จาก <?php echo $totalRows_rsmem ?></p>
										<table border="0" width="40%" align="right">
											<tr>
												<td width="25%" height="47" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																							?>
														<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, 0, $queryString_rsmem); ?>" class="style16">หน้าแรก</a>
													<?php } // Show if not first page 
													?>
												</td>
												<td width="23%" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																				?>
														<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, max(0, $pageNum_rsmem - 1), $queryString_rsmem); ?>" class="style16">ก่อนหน้า</a>
													<?php } // Show if not first page 
													?>
												</td>
												<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																				?>
														<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, min($totalPages_rsmem, $pageNum_rsmem + 1), $queryString_rsmem); ?>" class="style16">ถัดไป</a>
													<?php } // Show if not last page 
													?>
												</td>
												<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																				?>
														<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, $totalPages_rsmem, $queryString_rsmem); ?>" class="style16">หน้าสุดท้าย</a>
													<?php } // Show if not last page 
													?>
												</td>
											</tr>
										</table>


										</td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;
											</td>
										</tr>
										<tr>
											<td height="100%" colspan="2"></td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;
											</td>
										</tr>
										</table>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Main Content -->

	</div>
	<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="../#page-top">
		<i class="fas fa-angle-up"></i>
	</a>


	<!-- Bootstrap core JavaScript-->
	<script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="js/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="Scripts/swal.js"></script>

	<script>
		$(document).ready(function() {
			//$('#ddlProduct').select2();
			//$('#ddlCustomer').select2();
		});

		function handleEnter(event) {
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}
		}
	</script>

</body>