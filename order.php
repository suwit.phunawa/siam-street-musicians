<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: admin.php");
}

include("function/connect.php");
include("function/FunctionDate.php");

$activePage = "ORDER";
?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">



	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<title>ORDER SYSTEM</title>

	<!-- Custom fonts for this template-->
	<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">


	<style type="text/css">
		td {
			font-family: Tahoma;
			font-size: 14px;
			color: #828282;
			text-decoration: none;
			border: none;
		}

		font {
			font-family: Tahoma;
			font-size: 14px;
			color: #1890E1;
			text-decoration: none;
			border: none;
			font-weight: bold;
		}

		.f {
			font-family: Tahoma;
			font-size: 14px;
			color: #FF2D00;
			text-decoration: none;
			border: none;
			font-weight: normal;
		}

		a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: bold;
			color: #FB5E3C;
			text-decoration: none;
			border: none;
		}

		.a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: normal;
			color: #FFFFFF;
			text-decoration: none;
			border: none;
		}

		.a1 {
			font-family: Tahoma;
			font-size: 14px;
			color: #40A3D8;
			font-weight: normal;
			text-decoration: none;
			border: none;
		}

		img {
			text-decoration: none;
			border: none;
		}
	</style>
	<title>ORDER SYSTEM</title>

	<script>
		function deleteOrder(od_code) {
			if (confirm("ยืนยันการลบ รายการสั่งซื้อ ?")) {
				window.location = 'delete_order.php?od_code=' + od_code;
			}
		}
	</script>
</head>

<body bgcolor="#FFFFFF">
	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "sidebar.php"; ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column ">

			<!-- Main Content -->
			<div class="container-fluid container-md">
				<br />
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">รายการสั่งสินค้า</h1>
				</div>

				<!-- Content Row -->
				<div class="row">

					<!-- Earnings (Monthly) Card Example -->
					<div class="col-xl-12 col-md-12 mb-4">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">

								<div class="row no-gutters">
									<div class="col-md-12">
										<div class="p-0">

											<table width="90%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td colspan="2">

														<?php
														include("function/connect.php");

														$currentPage = $_SERVER["PHP_SELF"];

														$maxRows_rsmem = 30;
														$pageNum_rsmem = 0;
														if (isset($_GET['pageNum_rsmem'])) {
															$pageNum_rsmem = $_GET['pageNum_rsmem'];
														}
														$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


														if (isset($_GET['pageNum_rsmem'])) {
															$pageNum_rsmem = $_GET['pageNum_rsmem'];
														}
														$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
														//คิวรี่ปกติ
														$query_rsmem = "
                      SELECT a.od_code, b.cus_name, a.invoice_no, SUM(a.od_total) as total
                      FROM order_p a, customer b
                      WHERE a.od_status = '2' and a.cus_id = b.cus_id
                      group by a.od_code, b.cus_name, a.invoice_no
                      order by a.od_code desc
                      ";
														$query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

														$rsmem = mysqli_query($c, $query_limit_rsmem);
														$row_rsmem = mysqli_fetch_assoc($rsmem);

														if (isset($_GET['totalRows_rsmem'])) {
															$totalRows_rsmem = $_GET['totalRows_rsmem'];
														} else {
															$all_rsmem = mysqli_query($c, $query_rsmem);
															$totalRows_rsmem = mysqli_num_rows($all_rsmem);
														}

														$totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

														$queryString_rsmem = "";
														if (!empty($_SERVER['QUERY_STRING'])) {
															$params = explode("&", $_SERVER['QUERY_STRING']);
															$newParams = array();
															foreach ($params as $param) {
																if (
																	stristr($param, "pageNum_rsmem") == false &&
																	stristr($param, "totalRows_rsmem") == false
																) {
																	array_push($newParams, $param);
																}
															}
															if (count($newParams) != 0) {
																$queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
															}
														}

														$queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

														?>
														<table id="box-table-a" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="table table-bordered">
															<tr class="a">
																<th width="125" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>เลขที่ใบกำกับ</h5>
																</th>
																<th width="145" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>วันที่สั่งสินค้า</h5>
																</th>
																<th width="200" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>ชื่อลูกค้า</h5>
																</th>
																<td width="89" align="center" valign="middle" style="color: white;" bgcolor="#B5A8A8">
																	<h5>ราคารวม</h5>
																	</th>
																<th width="60" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>ลบ</h5>
																</th>
																<th width="180" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>พิมพ์</h5>
																</th>
															</tr>
															<?php


															//เริ่มวน
															$unit_total = 0;
															$price_total = 0;


															if ($totalRows_rsmem > 0) { // Show if recordset not empty 
															?>
																<?php do {
																	$odCode = $row_rsmem["od_code"];
																	$odId = str_replace(" ", "", $odCode);
																	$odId = str_replace("-", "", $odId);
																	$odId = str_replace(":", "", $odId);
																?>
																	<tr>
																		<td align="center">
																			<?php echo $row_rsmem["invoice_no"]; ?>
																		</td>
																		<td>
																			<div align="left"><?php echo $row_rsmem["od_code"]; ?></div>
																		</td>
																		<td>
																			<div align="left"><?php echo "$row_rsmem[cus_name]"; ?></div>
																		</td>
																		<td>
																			<div align="right"><?php echo number_format($row_rsmem["total"], 2, '.', ','); ?></div>
																		</td>
																		<td align="center">
																			<img src="images/delete_icon.png" width="30" height="30" onclick="deleteOrder('<?php echo $odCode; ?>')" />
																		</td>
																		<td valign="middle">

																			<div align="center">
																				<a style="display:none;" href="prints.php?code=<?php echo $row_rsmem["od_code"]; ?>" target="_blank"><img src="images/prints.png" width="32" height="32" alt="พิมพ์แบบฟอร์มเล็ก" /></a>

																				<a href="printl.php?code=<?php echo $row_rsmem["od_code"]; ?>" target="_blank">
																					<button class="btn btn-primary">
																						ใบเสร็จ
																					</button></a>
																				<a class="d-none" href="printl_company.php?code=<?php echo $row_rsmem["od_code"]; ?>" target="_blank"><img src="images/printl2.png" width="38" height="38" />

																				</a>
																				<a class="d-none" href="printl_vat.php?code=<?php echo $row_rsmem["od_code"]; ?>" target="_blank"><img src="images/printl3.png" width="38" height="38" /></a>

																				<button class="btn btn-danger" data-toggle="modal" data-target="#printOptionModal<?php echo $odId; ?>">
																					ใบกำกับ
																				</button>

																				<!-- Modal -->
																				<div class="modal fade" id="printOptionModal<?php echo $odId; ?>" tabindex="-1" aria-labelledby="printOptionModal" aria-hidden="true">
																					<div class="modal-dialog">
																						<div class="modal-content">
																							<div class="modal-header">
																								<h5 class="modal-title text-danger" id="printOptionModal">พิมพ์ใบกำกับภาษี</h5>
																								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																									<span aria-hidden="true">&times;</span>
																								</button>
																							</div>
																							<div class="modal-body pl-5 pr-5 pt-3">
																								<form class="user pl-3 pr-3" id="printVatBillForm_<?php echo $odId; ?>" method="GET" action="print_vat_action.php" target="_blank">
																									<input type="hidden" name="code" value="<?php echo $odCode; ?>" />
																									<input type="hidden" name="id" value="<?php echo $odId; ?>" />
																									<div class="row" style="display:none;">
																										<label class="p-0 m-0 font-weight-bold">
																											<h5>รูปแบบฟอร์ม</h5>
																										</label>
																									</div>
																									<div class="row" style="display: none;">
																										<div class="form-check ml-4">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="form_type_radio" checked value="NORMAL">ปกติ
																											</label>
																										</div>
																										<div class="form-check ml-4">
																											<label class="form-check-label">
																												<input type="radio" class="form-check-input" name="form_type_radio" value="VAT">ยื่นภาษี
																											</label>
																										</div>
																									</div>

																									<div class="row">
																										<label class="p-0 m-0 font-weight-bold">
																											<h5>รันเลขใบกำกับภาษี</h5>
																										</label>
																									</div>
																									<div class="row">
																										<div class="form-check ml-4">
																											<label class="form-check-label">

																											</label>
																										</div>
																									</div>
																									<br />
																									<div class="row mt-6">
																										<div class="form-check">
																											<label class="form-check-label">
																												<div class="row">
																													<div class="col-md-6 ml-1" style="margin-left:10px;" align="center">
																														<input type="radio" class="form-check-input" name="run_vat_no_radio" checked value="NO" onchange="changeRunVatNo('<?php echo $odId; ?>', this.value)">ไม่รันเลข
																													</div>
																												</div>
																												<br />
																												<div class="row mt-6">
																													<div class="col-md-6" align="center"><input type="radio" class="form-check-input" name="run_vat_no_radio" value="YES" onchange="changeRunVatNo('<?php echo $odId; ?>', this.value)">รันเลข</div>
																													<div class="col-md-6" align="left"><input type="text" class="form-control" name="run_vat_no_txt" disabled value="<?php echo $row_rsmem["invoice_no"]; ?>" id="invoice_no_txt_<?php echo $odId; ?>" /></div>
																												</div>
																											</label>
																										</div>
																									</div>

																								</form>
																								<span class="text-danger">**หากเลือกเป็นไม่รันเลข เลขใบกำกับปัจจุบันจะถูกลบให้เป็นค่าว่างๆ</span>
																							</div>
																							<div class="modal-footer">

																								<button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
																								<button type="button" class="btn btn-primary" onclick="submitForm('<?php echo $odId; ?>')">พิมพ์</button>
																							</div>
																						</div>
																					</div>
																				</div>


																			</div>
																		</td>
																	</tr>
																<?php
																} while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
															<?php } // Show if recordset not empty 
															?>
															<?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
															?>
															<?php } // Show if recordset empty 
															?>
														</table>
														<br />
														<p align="right" class="style16">&nbsp;
															จำนวน <?php echo ($startRow_rsmem + 1) ?> ถึง <?php echo min($startRow_rsmem + $maxRows_rsmem, $totalRows_rsmem) ?> จาก <?php echo $totalRows_rsmem ?></p>
														<table border="0" width="40%" align="right">
															<tr>
																<td width="25%" height="47" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																											?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, 0, $queryString_rsmem); ?>" class="style16">หน้าแรก</a>
																	<?php } // Show if not first page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, max(0, $pageNum_rsmem - 1), $queryString_rsmem); ?>" class="style16">ก่อนหน้า</a>
																	<?php } // Show if not first page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, min($totalPages_rsmem, $pageNum_rsmem + 1), $queryString_rsmem); ?>" class="style16">ถัดไป</a>
																	<?php } // Show if not last page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, $totalPages_rsmem, $queryString_rsmem); ?>" class="style16">หน้าสุดท้าย</a>
																	<?php } // Show if not last page 
																	?>
																</td>
															</tr>
														</table>


													</td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
												<tr>
													<td height="100%" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->


	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="../#page-top">
		<i class="fas fa-angle-up"></i>
	</a>


	<!-- Bootstrap core JavaScript-->
	<script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="js/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="Scripts/swal.js"></script>

	<script>
		$(document).ready(function() {
			//$('#ddlProduct').select2();
			//$('#ddlCustomer').select2();
		});

		function handleEnter(event) {
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}
		}

		function submitForm(printVatFormID) {
			$("#printOptionModal" + printVatFormID).modal("hide");
			$("#printVatBillForm_" + printVatFormID).submit();
		}

		function changeRunVatNo(odId, value) {
			const runVat = (value == "NO");
			console.log(runVat);
			$("#invoice_no_txt_" + odId).prop("disabled", runVat);
		}
	</script>

</body>