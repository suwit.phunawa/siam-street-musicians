<?php @session_start();
include("function/connect.php");
?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		td {
			font-family: Tahoma;
			font-size: 14px;
			color: #828282;
			text-decoration: none;
			border: none;
		}

		font {
			font-family: Tahoma;
			font-size: 14px;
			color: #1890E1;
			text-decoration: none;
			border: none;
			font-weight: bold;
		}

		.f {
			font-family: Tahoma;
			font-size: 14px;
			color: #FF2D00;
			text-decoration: none;
			border: none;
			font-weight: normal;
		}

		a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: bold;
			color: #FB5E3C;
			text-decoration: none;
			border: none;
		}

		.a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: normal;
			color: #FFFFFF;
			text-decoration: none;
			border: none;
		}

		.a1 {
			font-family: Tahoma;
			font-size: 14px;
			color: #40A3D8;
			font-weight: normal;
			text-decoration: none;
			border: none;
		}

		img {
			text-decoration: none;
			border: none;
		}
	</style>
	<script language="JavaScript">
		//****************************** Function ตรวจสอบข้อมูลใน TextBox ******************************

		function chkCus() {
			var msg = '';
			var me = document.form1;
			//alert(me.ddlCustomer.value);
			window.location = 'index.php?id=' + me.ddlCustomer.value;
		}

		function chkPd() {
			var msg = '';
			var me = document.form1;
			var data_p = me.ddlProduct.value;
			var res = data_p.split(",");
			//alert(me.ddlProduct.value);
			me.txtPdId.value = res[0];
			me.txtPrice.value = res[1];
			me.txtUnit.value = res[2];
			me.txtNum.value = '';
			me.txtTotal.value = '';
		}

		function chkTotal() {
			var msg = '';
			var me = document.form1;
			var total = 0;
			if (me.txtPrice.value !== '') {
				total = me.txtNum.value * me.txtPrice.value;
				me.txtTotal.value = total.toFixed(2);
			}
		}

		function SubmitF() {
			var msg = '';
			var me = document.form1;
			if (me.txtCustomer.value == '') {
				msg = ' กรุณากรอกชื่อลูกค้า ! \n';
			}
			if (me.txtProduct.value == '') {
				msg += ' กรุณากรอกชื่อสินค้า ! \n';
			}
			if (me.txtNum.value == '') {
				msg += ' กรุณาระบุจำนวนสินค้า ! \n';
			}
			if (me.txtTotal.value == '') {
				msg += ' กรุณาระบุจำนวนสินค้า ! \n';
			}
			if (msg != '') {
				alert(msg);
			} else {
				me.submit();
			}
		}

		function P_delete(id) {
			var me = document.form1;
			me.txtMode.value = 'delete';
			me.txtId.value = id;
			me.submit();
		}

		function ConfirmP(cus_id) {
			if (confirm('ยืนยันการสั่งสินค้า ?')) {
				window.location = 'p2_index.php?page=GENERAL&id=' + cus_id;
			}
		}

		//***************************** End Function ***************************************************
	</script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.13.2/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.13.2/jquery-ui.min.js"></script>
	<title>ORDER SYSTEM</title>
</head>

<body>
	<table width="200" border="0" align="center" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2"><img src="images/top.jpg"></td>
		</tr>
		<tr>
			<td colspan="2">
				<img src="images/head.jpg" width="760" height="133" alt="" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr noshade="noshade" color="#C7C7C7">
			</td>
		</tr>
		<tr>
			<td background="images/bg.jpg" valign="top">
				<!--เริ่มตารางเมนู-->
				<?php include("menu.php"); ?>
				<!--จบตารางเมนู-->
			</td>
			<td valign="top">
				<!--เริ่มตารางเนื้อหา-->

				<table width="588" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td width="47"><img src="images/Book_edit.png" width="44" height="44" alt="" /></td>
						<td width="541">
							<span style="color: #F89302;">
								<h2>สั่งสินค้า</h2>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<hr noshade="noshade" color="#C7C7C7">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<form name="form1" method="post" action="p_index.php" id="form1">
								<table width="584" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="140">&nbsp;</td>
										<td width="462">
											<input type="hidden" name="txtMode" value="add" />
											<input type="hidden" name="txtPage" value="GENERAL" />
											<input type="hidden" name="txtId" />
											<input type="hidden" name="txtPdId" />
										</td>
										<td width="24">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<div align="right">วันที่สั่งสินค้า : </div>
										</td>
										<td><input type="text" name="txtDate" value="<?php echo date("Y-m-d"); ?>" onfocus=" blur();" /></td>
										<td class="style8">&nbsp;</td>
									</tr>
									<tr>
										<td>
											<div align="right">ชื่อลูกค้า :</div>
										</td>
										<td>
											<input type="text" name="txtCustomer" id="txtCustomer" onkeydown="setCustomerAutoComplete(event)"  value="<?php echo $_GET["cusName"] ?? ""; ?>" onkeyup="setCustomerAutoComplete()" />
										</td>
										<td class="style8"><span class="style8" style="color: #F80307">* </span></td>
									</tr>
									<tr>
										<td>
											<div align="right">โทรศัพท์ (ถ้ามี):</div>
										</td>
										<td>
											<input type="text" name="txtCusPhone" value="<?php echo $_GET["cusPhone"] ?? ""; ?>" onkeydown='handleEnter(event)' />
										</td>
										<td class="style8"></td>
									</tr>
									<tr>
										<td>
											<div align="right">ที่อยู่ (ถ้ามี):</div>
										</td>
										<td>
											<textarea type="text" name="txtCusAddress" rows="4" cols="22" onkeydown='handleEnter(event)'><?php echo $_GET["cusAddress"] ?? ""; ?></textarea>
										</td>
										<td class="style8"></td>
									</tr>
									<tr>
										<td>
											<div align="right">เลขประจำตัวผู้เสียภาษี</div>
										</td>
										<td>
											<input type="text" name="txtCusTaxId" onkeydown='handleEnter(event)' value="<?php echo $_GET["cusTaxId"] ?? ""; ?>" />
										</td>
										<td class="style8"></td>
									</tr>
									<tr>
										<td>
											<div align="right">ชื่อสินค้า : </div>
										</td>
										<td>
											<input type="text" name="txtProduct" id="txtProduct" onkeydown="setProductAutoComplete(event)" />
										</td>
										<td class="style8"><span class="style8" style="color: #F80307">* </span></td>
									</tr>
									<tr>
										<td>
											<div align="right">จำนวน :</div>
										</td>
										<td><input type="number" name="txtNum" maxlength="20" size="25" onChange="chkTotal();" onkeydown='handleEnter(event)' /></td>
										<td><span class="style8" style="color: #F80307">* </span></td>
									</tr>
									<tr>
										<td>
											<div align="right">ราคา/หน่วย :</div>
										</td>
										<td><input type="number" name="txtPrice" maxlength="20" size="25" onChange="chkTotal();" onkeydown='handleEnter(event)' /></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<div align="right">หน่วย : </div>
										</td>
										<td><input type="text" name="txtUnit" maxlength="20" size="25" /></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<div align="right">ราคารวม :</div>
										</td>
										<td><input type="text" name="txtTotal" maxlength="20" size="25" onfocus=" blur();" /></td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td colspan="2">
											<div align="center">
												<input type="button" name="submit1" value="  Save  " onClick="SubmitF();">
												&nbsp;&nbsp;&nbsp;
												<input type="reset" name="submit2" value="  Reset  ">
											</div>
										</td>
									</tr>
								</table>
							</form>

						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">

							<?php
							if (isset($_GET["id"])) {

								$currentPage = $_SERVER["PHP_SELF"];

								$maxRows_rsmem = 30;
								$pageNum_rsmem = 0;
								if (isset($_GET['pageNum_rsmem'])) {
									$pageNum_rsmem = $_GET['pageNum_rsmem'];
								}
								$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


								if (isset($_GET['pageNum_rsmem'])) {
									$pageNum_rsmem = $_GET['pageNum_rsmem'];
								}
								$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
								//คิวรี่ปกติ
								$query_rsmem = "
												SELECT *
												FROM order_p a
												WHERE a.od_status = '1'  and a.cus_id = '0' and a.pd_id = '0' order by a.od_id ASC
									";




								$query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

								$rsmem = mysqli_query($c, $query_limit_rsmem);
								$row_rsmem = mysqli_fetch_assoc($rsmem);

								if (isset($_GET['totalRows_rsmem'])) {
									$totalRows_rsmem = $_GET['totalRows_rsmem'];
								} else {
									$all_rsmem = mysqli_query($c, $query_rsmem);
									$totalRows_rsmem = mysqli_num_rows($all_rsmem);
								}
								$totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

								$queryString_rsmem = "";
								if (!empty($_SERVER['QUERY_STRING'])) {
									$params = explode("&", $_SERVER['QUERY_STRING']);
									$newParams = array();
									foreach ($params as $param) {
										if (
											stristr($param, "pageNum_rsmem") == false &&
											stristr($param, "totalRows_rsmem") == false
										) {
											array_push($newParams, $param);
										}
									}
									if (count($newParams) != 0) {
										$queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
									}
								}
								$queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

							?>
								<table id="box-table-a" width="100%" border="0" cellpadding="1" cellspacing="1" align="center">
									<tr class="a">
										<th width="279" align="center" valign="middle" bgcolor="#B5A8A8">
											<h4>ชื่อสินค้า</h4>
										</th>
										<th width="68" align="center" valign="middle" bgcolor="#B5A8A8">จำนวน</th>
										<th width="84" align="center" valign="middle" bgcolor="#B5A8A8">ราคา/หน่วย</th>
										<th width="81" align="center" valign="middle" bgcolor="#B5A8A8">
											<h4>รวม</h4>
										</th>
										<th width="60" align="center" valign="middle" bgcolor="#B5A8A8">
											<h4>จัดการ</h4>
										</th>
									</tr>
									<?php


									//เริ่มวน
									$unit_total = 0;
									$price_total = 0;

									if ($totalRows_rsmem > 0) { // Show if recordset not empty 
									?>
										<?php do {

										?>
											<tr>
												<td>
													<div align="left"><?php echo "$row_rsmem[pd_name]"; ?></div>
												</td>
												<td>
													<div align="right"><?php echo "$row_rsmem[od_num]"; ?></div>
												</td>
												<td>
													<div align="right"><?php echo "$row_rsmem[od_priceperunit]"; ?></div>
												</td>
												<td>
													<div align="right"><?php echo "$row_rsmem[od_total]"; ?></div>
												</td>
												<td>
													<?php
													$odId = $row_rsmem["od_id"];
													?>
													<div align="center" class="style10 "><img src="images/user_delete.png" alt="ลบ" width="16" height="16" style="cursor:hand" onclick="P_delete('<?php echo $$odId; ?>');" /> </div>
												</td>
											</tr>
										<?php
											$unit_total += $row_rsmem["od_num"];
											$price_total += $row_rsmem["od_total"];
										} while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
									<?php } // Show if recordset not empty 
									?>
									<?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
									?>
									<?php } // Show if recordset empty 
									?>
									<tr>
										<td style="color: #050505; font-size: 16px; font-weight: bold;">
											<div align="left">รวมทั้งหมด</div>
										</td>
										<td style="color: #050505">
											<div align="right"><?php echo $unit_total; ?></div>
										</td>
										<td style="color: #050505">
											<div align="right"></div>
										</td>
										<td style="color: #050505">
											<div align="right"><?php echo number_format($price_total, 2, '.', ','); ?></div>
										</td>
										<td style="color: #050505">
											<div align="center" class="style10 "></div>
										</td>
									</tr>
								</table>


								<br>
								<?php if ($price_total > 0) { ?>
									<div align="right"><input type="button" name="submit3" value="  ยืนยันการสั่งสินค้า  " onClick="ConfirmP('<?php echo $_GET["id"]; ?>');"></div>
									<br>
								<?php } ?>
							<?php } ?>

						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;
						</td>
					</tr>
					<tr>
						<td height="100%" colspan="2"></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;
						</td>
					</tr>
				</table>

				<!--จบตารางเนื้อหา-->
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<!--เริ่มตารางเมนู-->
				<?php include("menu_under.php"); ?>
				<!--จบตารางเมนู-->
			</td>
		</tr>
	</table>

	<script>
		function setCustomerAutoComplete(event) {
			let textInput = $("#txtCustomer").val();

			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}

			$("#txtCustomer").autocomplete({
				source: "service/customer_data.php?inputText=" + textInput,
				select: function(event, ui) {
					event.preventDefault();
					console.log("ui.item.id", ui.item.id);
					$("#txtCustomer").val(ui.item.id);
				}
			});
		}

		function setProductAutoComplete(event) {
			let textInput = $("#txtProduct").val();
			
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}

			$("#txtProduct").autocomplete({
				source: "service/product_data.php?inputText=" + textInput,
				select: function(event, ui) {
					event.preventDefault();
					console.log("ui.item.id", ui.item.id);
					$("#txtProduct").val(ui.item.id);
				}
			});
		}

		function handleEnter(event) {
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}
		}
	</script>
</body>

</html>