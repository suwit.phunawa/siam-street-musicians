<?php
require_once("../function/connect.php");

$textInput = $_GET["inputText"] ?? "";

$sql = "(SELECT pd_name, pd_id FROM product
        WHERE (pd_status = '1' AND pd_name like '%$textInput%')
        LIMIT 0, 10)        
        UNION
        (SELECT pd_name, pd_id FROM order_p
        WHERE ( pd_name like '%$textInput%')
        LIMIT 0, 10)";

$query = mysqli_query($c, $sql);

$productData = array();

while ($row = mysqli_fetch_assoc($query)) {
    $data['id'] = $row['pd_name'];
    $data['value'] = $row['pd_name'];
    array_push($productData, $data);
}

echo json_encode($productData);
