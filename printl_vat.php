<?php
@session_start();

require_once __DIR__ . '/mpdf8/vendor/autoload.php';
//require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
ob_start(); // ทำการเก็บค่า html นะครับ

include("function/FunctionDate.php");
include("function/connect.php");


$query = "
			SELECT a.od_date, a.invoice_no, b.cus_name, b.cus_add, b.cus_tel, a.od_id, b.cus_id, b.cus_taxid
			FROM order_p a, customer b
			WHERE a.cus_id = b.cus_id and a.od_code = '" . $_GET["code"] . "'
      GROUP BY a.od_date, b.cus_name, b.cus_add, b.cus_tel, a.invoice_no
      ORDER BY a.od_id ASC
	";
$result = mysqli_query($c, $query);
$row = mysqli_fetch_array($result);

$orderDate = date_create($row["od_date"]);
$invoiceNo = $row["invoice_no"];
$invoiceNo = $invoiceNo == "" ? "I" . date_format($orderDate, "Ym") . $row["od_id"] : sprintf('%05d', $invoiceNo);
$curDate = date("Y-m-d");
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <style>
    #item_table {
      border-collapse: collapse;
      border-spacing: 1px !important;
    }

    #item_table th {
      border: 1px solid #999999;
      padding: 3px;
    }

    #item_table td {
      border-left: solid 1px #999999;
      border-right: solid 1px #999999;
      padding: 1px !important;
      height: 25px;
    }

    #footer_table {
      font-size: 13px;
      width: 100%;
      margin-top: 8px;
    }

    #footer_table td {
      padding: 5px;
      border: solid 1px #999999;
    }
  </style>
</head>

<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="110px" valign="top">
        <img src="images/company_logo.jpg" width="100px" />
      </td>
      <td valign="top">
        <span style="font-size: 20px; font-weight:700;">บริษัท ทวีสิน เบเกอรี่ จำกัด</span><br />
        62 หมู่ 5 ต.ดอนไก่ดี อ.กระทุ่มแบน <br />
        จ.สมุทรสาคร 74110<br />
        โทร. 034-878732 034-878552 086-8133039<br />
        เลขประจำตัวผู้เสียภาษี 0745561007339
      </td>
      <td width="295px">
        <table align="right">
          <tr>
            <td align="center">
              (ต้นฉบับ / Original)<br />
              <b>ใบกำกับภาษี / ใบส่งสินค้า</b><br />
              TAX INVOICE / DELIVERY ORDER
            </td>
          </tr>
          <tr>
            <td>
              <table style=" border: solid 1px #999999; padding:5px;font-size: 13px; width:100%; height:300px;" align="center">
                <tr style="padding-top: 20px;">
                  <td>
                    <b>
                      เลขที่ใบกำกับภาษี:<br />
                      Tax Invoice No.
                    </b>
                  </td>
                  <td><?php echo $invoiceNo; ?></td>
                </tr>
                <tr>
                  <td><b>วันที่ / Date:</b></td>
                  <td><?php echo dateThai($curDate); ?></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <table style=" border: solid 1px #999999; padding:5px;font-size: 13px; width:100%; height:300px; margin-top:8px;">
    <tr>
      <td width="50%">
        <b>ชื่อลูกค้า/Customer name:</b> <?php echo $row["cus_name"]; ?><br />
      </td>
      <td>
        <b>เลขประจำตัวผู้เสียภาษี/Tax Id:</b> <?php echo $row["cus_taxid"]; ?><br />
      </td>

    </tr>
    <tr>
      <td>
        <b>ที่อยู่/Address:</b> <?php echo $row["cus_add"]; ?> <br />
      </td>
      <td>
        <b>รหัสสมาชิก/Member code:</b> <?php echo "MB" . str_pad($row["cus_id"], 8, '0', STR_PAD_LEFT); ?><br />
      </td>

    </tr>
    <tr>
      <td>
        <b>โทรศัพท์/Contact:</b> <?php echo $row["cus_tel"]; ?><br />
      </td>
      <td>
        <b>เลขที่อ้างอิง/Ref No:</b> <?php echo "O" . $invoiceNo; ?><br />
      </td>
    </tr>
  </table>
  <?php
  include("function/connect.php");

  $currentPage = $_SERVER["PHP_SELF"];

  $maxRows_rsmem = 30;
  $pageNum_rsmem = 0;
  if (isset($_GET['pageNum_rsmem'])) {
    $pageNum_rsmem = $_GET['pageNum_rsmem'];
  }
  $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


  if (isset($_GET['pageNum_rsmem'])) {
    $pageNum_rsmem = $_GET['pageNum_rsmem'];
  }
  $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
  //คิวรี่ปกติ
  $query_rsmem = "
			SELECT *
			FROM order_p a, product b
			WHERE a.pd_id = b.pd_id and a.od_code = '$_GET[code]' ORDER BY a.od_id ASC
";
  $query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

  $rsmem = mysqli_query($c, $query_limit_rsmem);
  $row_rsmem = mysqli_fetch_assoc($rsmem);

  if (isset($_GET['totalRows_rsmem'])) {
    $totalRows_rsmem = $_GET['totalRows_rsmem'];
  } else {
    $all_rsmem = mysqli_query($c, $query_rsmem);
    $totalRows_rsmem = mysqli_num_rows($all_rsmem);
  }
  $totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

  $queryString_rsmem = "";
  if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
      if (
        stristr($param, "pageNum_rsmem") == false &&
        stristr($param, "totalRows_rsmem") == false
      ) {
        array_push($newParams, $param);
      }
    }
    if (count($newParams) != 0) {
      $queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
    }
  }
  $queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

  ?>

  <table width="100%" id="item_table" align="center" cellpadding="0" cellspacing="0" style="margin-top:8px;">
    <tr>
      <th align="center" style="font-size: 14px;">ลำดับที่<br />No.</th>
      <th align="center" style="font-size: 14px;">รายการสินค้า / บริการ<br />Description</th>
      <th align="center" style="font-size: 14px;">จำนวน<br />Quantity</th>
      <th align="center" style="font-size: 14px;">หน่วย<br />Unit</th>
      <th align="center" style="font-size: 14px;">ราคา/หน่วย<br />Unit Price</th>
      <th align="center" style="font-size: 14px;">จำนวนเงิน<br />Amount</th>
    </tr>

    <?php
    $total_total = 0;
    $num = 0;
    //เริ่มวน

    if ($totalRows_rsmem > 0) { // Show if recordset not empty 
    ?>
      <?php do {

      ?>

        <tr>
          <td width="10%" align="center">
            <div><span style="font-size: 14px"><?php echo $num + 1; ?></span></div>
          </td>
          <td width="40%" height="30px">
            <div align="left"><span style="font-size: 14px"><?php echo "$row_rsmem[pd_name]"; ?></span></div>
          </td>
          <td width="10%" align="center">
            <div><span style="font-size: 14px"><?php echo "$row_rsmem[od_num]"; ?></span></div>
          </td>
          <td width="10%" align="center">
            <div><span style="font-size: 14px"><?php echo "$row_rsmem[pd_unit]"; ?></span></div>
          </td>
          <td width="15%" align="right">
            <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_priceperunit"], 2); ?></span></div>
          </td>
          <td width="15%" align="right">
            <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_total"], 2); ?></span></div>
          </td>
        </tr>
      <?php
        $total_total = $total_total + $row_rsmem["od_total"];
        $num++;
      } while ($row_rsmem = mysqli_fetch_assoc($rsmem));
    }

    $totalRowInPage = 16;
    if ($num < $totalRowInPage) {
      $generateRow = $totalRowInPage - $num;

      for ($i = 0; $i < $generateRow; $i++) {
      ?>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    <?php
      }
    }


    $totalVat = $total_total * 0.07;

    ?>
    <tr>
      <th colspan="2" align="left" style="font-size: 14px;" valign="top">หมายเหตุ:</ะ>
      <th colspan="3" align="right" style="font-size: 14px;">
        รวมมูลค่าสินค้า (Total)<br /><br />
        หักส่วนลด (Discount)<br /><br />
        ยอดสุทธิ (Total Sale)<br /><br />
        ภาษีมูลค่าเพิ่ม (Vat)<br /><br />
        จำนวนเงินรวมทั้งสิ้น (Total Amount)
      </th>
      <th style="font-size: 14px; font-weight:0;" align="right">
        <?php echo number_format($total_total, 2); ?><br /><br />
        0.00<br /><br />
        0.00<br /><br />
        <?php echo number_format($totalVat, 2); ?><br /><br />
        <?php echo number_format($total_total + $totalVat, 2); ?>
      </th>
    </tr>
  </table>

  <table id="footer_table">
    <tr>
      <td align="center">
        <b>ได้รับสินค้าตามรายการข้างบนไว้<br />ถูกต้องในสภาพเรียบร้อยแล้ว<br /></b><br /><br />
        (_________________________________)<br /><br />
        ผู้รับสินค้า/Goode Received by<br />
        วันที่/Date ……/……/……. <br />
      </td>
      <td align="center"><br /></b><br /><br /><br />
        (_________________________________)<br /><br />
        ผู้ส่งสินค้า/Goods Delivery by<br />
        วันที่/Date ……/……/……. <br />
      </td>
      <td align="center" valign="top">
        <b>ในนาม "บริษัท ทวีสิน เบเกอรี่ จำกัด"</b><br /><br /><br /><br />
        (_________________________________)<br /><br />
        ผู้มีอำนาจลงนาม/Authorized Signature<br />
      </td>
    </tr>
  </table>
</body>

</html>

<?php

$html = ob_get_contents();
$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
ob_end_clean();

$mpdf = new \Mpdf\Mpdf([
  'mode' => '',
  'format' => 'A4',
  'margin_left' => 10,
  'margin_right' => 10,
  'margin_top' => 10,
  'margin_bottom' => 10,
  'margin_header' => 6,
  'margin_footer' => 6,
  'orientation' => 'P',
]);
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont   = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
