﻿<?php @session_start();
include("function/connect.php");
?>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0" />
    </script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>Tawesin Bakery</title>

    <style>
        body {
            height: 100%;
            background-color: #021934;
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='400' height='400' viewBox='0 0 800 800'%3E%3Cg fill='none' stroke='%231A1F44' stroke-width='1'%3E%3Cpath d='M769 229L1037 260.9M927 880L731 737 520 660 309 538 40 599 295 764 126.5 879.5 40 599-197 493 102 382-31 229 126.5 79.5-69-63'/%3E%3Cpath d='M-31 229L237 261 390 382 603 493 308.5 537.5 101.5 381.5M370 905L295 764'/%3E%3Cpath d='M520 660L578 842 731 737 840 599 603 493 520 660 295 764 309 538 390 382 539 269 769 229 577.5 41.5 370 105 295 -36 126.5 79.5 237 261 102 382 40 599 -69 737 127 880'/%3E%3Cpath d='M520-140L578.5 42.5 731-63M603 493L539 269 237 261 370 105M902 382L539 269M390 382L102 382'/%3E%3Cpath d='M-222 42L126.5 79.5 370 105 539 269 577.5 41.5 927 80 769 229 902 382 603 493 731 737M295-36L577.5 41.5M578 842L295 764M40-201L127 80M102 382L-261 269'/%3E%3C/g%3E%3Cg fill='%23063D55'%3E%3Ccircle cx='769' cy='229' r='5'/%3E%3Ccircle cx='539' cy='269' r='5'/%3E%3Ccircle cx='603' cy='493' r='5'/%3E%3Ccircle cx='731' cy='737' r='5'/%3E%3Ccircle cx='520' cy='660' r='5'/%3E%3Ccircle cx='309' cy='538' r='5'/%3E%3Ccircle cx='295' cy='764' r='5'/%3E%3Ccircle cx='40' cy='599' r='5'/%3E%3Ccircle cx='102' cy='382' r='5'/%3E%3Ccircle cx='127' cy='80' r='5'/%3E%3Ccircle cx='370' cy='105' r='5'/%3E%3Ccircle cx='578' cy='42' r='5'/%3E%3Ccircle cx='237' cy='261' r='5'/%3E%3Ccircle cx='390' cy='382' r='5'/%3E%3C/g%3E%3C/svg%3E");
        }

        .card {
            background: transparent;
            backdrop-filter: blur(10px);
            backdrop-filter: brightness(80%);
            backdrop-filter: contrast(60%);
            backdrop-filter: drop-shadow(4px 4px 10px blue);
            backdrop-filter: grayscale(20%);
            backdrop-filter: hue-rotate(120deg);
            backdrop-filter: invert(30%);
            backdrop-filter: opacity(40%);
            backdrop-filter: sepia(90%);
            backdrop-filter: saturate(80%);
            color: white;
            border: white solid 1px;
            text-align: center;
            font-size: 2vw;
            width: 100%;
            padding: 0px;
            height: 20vw;
        }

        .material-icons {
            font-size: 5svw;
        }

        .material-symbols-outlined {
            font-size: 5svw;
        }

        .card_logout {
            color: #D35400 !important;
        }

        .card_logout:hover {
            color: #306191 !important;
        }

        .card-body {
            padding-top: 5vw;
            cursor: pointer;
        }

        .card:hover {
            color: #306191;
            border: #306191 solid 1px;
        }

        .card:hover>a {
            color: #306191;
        }
    </style>
</head>

<body>
    <?php if (!isset($_SESSION["LOGGED_IN_USER"])) { ?>
        <div align="center">
            <img src="images/welcome.jpg" class="img-thumbnail" style="height: 100%;" />
        </div>
    <?php } else { ?>
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="row p-0 col-9 gx-4 gy-4">
                <div class="col-md-3">
                    <div class="card" onclick="javascript:location.href='billing.php'">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <span class="material-icons">
                                        receipt_long
                                    </span>
                                    <br /><br />
                                    ขายสินค้า
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card" onclick="javascript:location.href='po_system/purchase.php'">
                        <div class="card-body">
                            <span class="material-icons">
                                local_shipping
                            </span>
                            <br /><br />
                            สั่งสินค้า
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card" onclick="javascript:location.href='po_system/production.php'">
                        <div class="card-body">
                            <span class="material-icons">
                                precision_manufacturing
                            </span>
                            <br /><br />
                            สั่งผลิต
                        </div>
                    </div>
                </div>
                <div class="col-md-3" onclick="logout()">
                    <div class="card card_logout" onclick="javascript:location.href='po_system/production.php'">
                        <div class="card-body">
                            <span class="material-symbols-outlined">
                                logout
                            </span>
                            <br /><br />
                            ออกจากระบบ
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <script>
        function logout(){
            window.location = "logout.php";
        }
    </script>
</body>

</html>