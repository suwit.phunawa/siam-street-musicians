<?php
@session_start();

include("function/connect.php");
if ($_POST["txtMode"] == 'add') {

	$productName = isset($_POST["txtProduct"]) ? $_POST["txtProduct"] : "";
	$cusName = isset($_POST["txtCustomer"]) ? $_POST["txtCustomer"] : "";
	$cusPhone = isset($_POST["txtCusPhone"]) ? $_POST["txtCusPhone"] : "";
	$cusAddress = isset($_POST["txtCusAddress"]) ? $_POST["txtCusAddress"] : "";
	$cusTaxId = isset($_POST["txtCusTaxId"]) ? $_POST["txtCusTaxId"] : "";

	$ddlCustomer = isset($_POST["ddlCustomer"]) ? $_POST["ddlCustomer"] : "";
	$ddlProduct = isset($_POST["ddlProduct"]) ? $_POST["ddlProduct"] : "";


	$orderDate = isset($_POST["txtDate"]) ? $_POST["txtDate"] : "";
	$tmpDate = $orderDate;

	if ($orderDate != "") {
		$orderDates = explode("/", $orderDate);

		$curTime = date("H:i:s");
		$orderDate = date("Y-m-d", strtotime($orderDates[2] . "-" . $orderDates[1] . "-" . $orderDates[0]));
		$orderDate .= " $curTime";
	} else {
		$orderDate = date("Y-m-d H:i:s");
	}

	if (
		$_POST["txtDate"] == "" or ($ddlCustomer == "" && $cusName == "") or
		($ddlProduct == "" && $productName == "") or $_POST["txtNum"] == ""
		or $_POST["txtPrice"] == ""  or ($_POST["txtPdId"] == "" && $productName == "") or $_POST["txtTotal"] == ""
	) {
		echo "<script language='javascript'> alert('Data Invalid !'); window.history.back(); </script>";
	} else {

		$unitName = $_POST["txtUnit"];

		//Save Data
		$query = "insert into order_p ( od_date, cus_id, pd_id, od_num, od_priceperunit, od_total, od_status, cus_name, pd_name, unit_name, cus_phone, cus_address, cus_taxid ) ";
		$query = $query . " VALUES ( '$orderDate','$ddlCustomer','" . $_POST["txtPdId"] . "','" . $_POST["txtNum"] . "','";
		$query = $query . $_POST["txtPrice"] . "','" . $_POST["txtTotal"] . "','1','$cusName', '$productName',";
		$query = $query . "'$unitName', '$cusPhone', '$cusAddress', '$cusTaxId')";

		$result = mysqli_query($c, $query);
		mysqli_close($c);

		//echo "<script language='javascript'> alert('Save Complete.'); </script>";

		$redirectPage = $_POST["txtPage"] == "GENERAL" ? "index_general.php" : "billing.php";
		$customer = $_POST["txtPage"] == "GENERAL" ? 0 : $_POST["ddlCustomer"];


		echo "<meta  http-equiv='refresh' content='1;url=$redirectPage?id=$customer&cusName=$cusName&cusPhone=$cusPhone&cusAddress=$cusAddress&cusTaxId=$cusTaxId&txtDate=$tmpDate'>";
	}
} else if ($_POST["txtMode"] == 'delete') {

	$query = " DELETE FROM order_p WHERE od_id = '" . $_POST["txtId"] . "' ";
	//echo $query;
	//break;
	$result = mysqli_query($c, $query);

	echo "<script language='javascript'> alert('Delete Complete.'); </script>";
	echo "<meta  http-equiv='refresh' content='1;url=billing.php?id=" . $_POST["ddlCustomer"] . "'>";
}
