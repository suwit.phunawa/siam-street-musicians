﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <title>SIAM STREET MUSICIAN</title>

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.7.2.custom.css">

    <style>
        .submit_btn {
            padding: 10px;
            padding-left: 30px;
            padding-right: 30px;
        }

        body {
            background: #C5D8EC;
        }

        .header_text {
            display: flex;
            justify-content: center;
            flex-direction: column;
            text-align: center;
        }

        .title_text {
            color: #999999;
        }

        #birth_date_txt {
            background-color: #FFFFFF !important;
        }

        .member_card {
            color: red;
        }

        .view_rules_text {
            color: blue;
            text-decoration: underline;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php
    echo "ปิดปรับปรุง";
    exit();
    require_once 'connect.php';

    ///Province List
    $stmt = $conn->prepare("SELECT* FROM provinces");
    $stmt->execute();
    $result = $stmt->fetchAll();

    ?>

    <br />
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <img src="asset/banner.jpg" width="100%">
                        <br />
                        <br />
                        <h3 class="header_text">ลงทะเบียนเข้าร่วมชมรมนักดนตรีเปิดหมวกสายอาชีพ</h3>
                        <h5 class="header_text title_text">SIAM STREET MUSICIAN</h5>
                        <br />
                        <hr />

                        <br />
                        <img src="asset/member_card.jpg" width="100%">
                        <h4 class="header_text member_card">
                            *ตัวอย่างบัตรสมาชิก
                            <br />
                            <br />- ขนาดบัตร 10×15 Cm.
                            <br />- การเคลือบบัตร 2 หน้ากันน้ำ
                            <br />- พร้อมสายคล้องคอคุณภาพดี
                        </h4>
                        <br />
                        <hr />
                        <br />
                        <br />

                        <form method="post" enctype="multipart/form-data">
                            ประเภทการแสดง
                            <div class="row">
                                <div class="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="perform_music_chk" id="musicChkBox">
                                        <label class="form-check-label" for="musicChkBox">
                                            ดนตรี
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="perform_sing_chk" id="singChkBox">
                                        <label class="form-check-label" for="singChkBox">
                                            ร้องเพลง
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="perform_street_chk" id="streetChkBox">
                                        <label class="form-check-label" for="streetChkBox">
                                            Street Show
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="perform_other_chk" id="otherChkBox" onclick="toggleCheckbox(this, 'perform_other_content', 'perform_other_txt')">
                                        <label class="form-check-label" for="otherChkBox">
                                            อื่น ๆ
                                        </label>
                                    </div>
                                    <div class="col" id="perform_other_content" style="display:none;">
                                        <input type="text" name="perform_other_txt" id="perform_other_txt" class="form-control" placeholder="การแสดงอื่น ๆ">
                                    </div>
                                </div>
                            </div>

                            <br />
                            ขื่อผู้สมัคร<font color="red">*</font>
                            <div class="row">
                                <div class="col">
                                    <input type="text" name="first_name" required class="form-control" placeholder="ชื่อ">
                                </div>
                                <div class="col">
                                    <input type="text" name="last_name" required class="form-control" placeholder="นามสกุล">
                                </div>
                            </div>

                            <br />
                            ฉายา
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <input type="text" name="alias" class="form-control">
                                </div>
                            </div>

                            <br />
                            วัน/เดือน/ปี เกิด
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <input type="text" name="birth_date" id="birth_date_txt" class="form-control" readonly="readonly">
                                </div>
                            </div>

                            <br />
                            เลขบัตรประจำตัวประชาชน 13 หลัก<br />
                            (จะปกปิดเป็นความลับสูงสุด ไม่เผยแพร่ ในที่สาธารณะ)
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <input type="text" name="personal_id_txt" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <br />
                                    รูปบัตรผู้แสดงความสามารถพิเศษ ที่ทางพมจ. เป็นผู้ออกให้ <font color="red">*</font>
                                    <br />
                                    <input type="file" name="perform_card_image" required class="form-control" accept="image/jpeg, image/png, image/jpg"> <br>
                                </div>
                            </div>

                            รายละเอียดเพิ่มเติม (ลิงค์ Facebook, Youtube, Tiktok คลิปแสดงความสามารถ)
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <textarea name="remark_txt" class="form-control"></textarea>
                                </div>
                            </div>

                            <br />
                            <br />
                            <strong>ที่อยู่ ณ ปัจจุบัน</strong>
                            <br />
                            <br />
                            <div class="row">
                                <div class="col">
                                    บ้านเลขที่
                                </div>
                                <div class="col-8 col-lg-9">
                                    <input type="text" name="address_1_txt" class="form-control">
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col">
                                    จังหวัด
                                </div>
                                <div class="col-8 col-lg-9">
                                    <select type="text" name="province_ddl" id="province_ddl" class="form-control">
                                        <option value="" selected disabled></option>
                                        <?php
                                        foreach ($result as $row) {
                                        ?>
                                            <option value="<?= $row['id'] ?>"><?= $row['name_th'] ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col">
                                    อำเภอ
                                </div>
                                <div class="col-8 col-lg-9">
                                    <select type="text" name="district_ddl" id="district_ddl" class="form-control">
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col">
                                    ตำบล
                                </div>
                                <div class="col-8 col-lg-9">
                                    <select type="text" name="subdistrict_ddl" id="subdistrict_ddl" class="form-control">
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col">
                                    รหัสไปรษณีย์
                                </div>
                                <div class="col-8 col-lg-9">
                                    <input type="text" name="zipcode_txt" id="zipcode_txt" class="form-control">
                                </div>
                            </div>

                            <br />
                            <br />
                            เบอร์โทร
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <input type="text" name="telephone1_txt" class="form-control">
                                </div>
                            </div>


                            <br />
                            <br />
                            <div class="row">
                                <br />
                                <b>รูปถ่ายประจำตัว จำนวน 2 รูป เต็มตัว และภาพถ่ายใกล้ครึ่งตัว ที่คิดว่าสวยที่สุด<font color="red">*</font></b>
                                <br /><br />
                                <div class="col">
                                    รูปเต็มตัว :
                                </div>
                                <div class="col-8 col-lg-9">
                                    <input type="file" name="profile_image_1" required class="form-control" accept="image/jpeg, image/png, image/jpg"> <br>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    รูปครึ่งตัว :
                                </div>
                                <div class="col-8 col-lg-9">
                                    <input type="file" name="profile_image_2" required class="form-control" accept="image/jpeg, image/png, image/jpg"> <br>
                                </div>
                            </div>

                            <br />

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <h4>ช่องทางการชำระเงิน โอนผ่านธนาคารตามด้านล่างนี้เท่านั้นนะคะ</h4>
                                    <br />

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="register_type_rdb" value="200" id="register_type_1_rdb" checked>
                                        <label class="form-check-label" for="register_type_1_rdb" vakye="250">
                                            <h2>ค่าสมัครสมาชิกต่อท่าน 200 บาท</h2>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="register_type_rdb" value="250" id="register_type_2_rdb">
                                        <label class="form-check-label" for="register_type_2_rdb">
                                            <h2>สมัครสมาชิก+สติกเกอร์ 1 ดวง 250 บาท</h2>
                                        </label>
                                    </div>

                                    <br />
                                    <font color="red">
                                        **บัตรจะมีอายุ 1 ปีหลังจากวันที่ทางชมรมได้ออกบัตรให้กับสมาชิก
                                        <br />
                                        **ค่าใช้จ่ายส่วนที่เหลือหลังหักจากการทำบัตร จะนำไปเก็บไว้เป็นกองทุนสำหรับใช้จ่ายในชมรมเพื่อเกิดประโยชน์กับสมาชิกทุกท่านในชมรม
                                        <br />
                                        **ขอสงวนสิทธิ์หากเป็นบัญชีอื่นทางชมรมจะไม่รับผิดชอบใดๆ ทั้งสิ้น
                                        กรุณาเช็คเลขบัญชี ยอดชำระ และชื่อบัญชีก่อนกดยืนยันทุกครั้ง ขอบคุณค่ะ
                                        <br />
                                        **หากโอนชำระแล้ว โปรดแนบรูปภาพสลิปทางด้านล่างด้วยนะคะ
                                    </font>

                                    <br />
                                    <br />

                                    <h4>
                                        =======================
                                    </h4>

                                    <h3>
                                        <br />
                                        ธนาคาร Kbank (กสิกรไทย)
                                        <br />
                                        ชื่อบัญชี กฤตเมธ ศรีประภา
                                        <br />
                                        เลขบัญชี : 1373115688
                                    </h3>
                                    <br />
                                    <img src="asset/bank.jpg" width="300px">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <br />
                                    อัพโหลดรูปภาพสลิปโอนเงิน<font color="red">*</font>
                                    <br />
                                    <input type="file" name="slip_image" required class="form-control" accept="image/jpeg, image/png, image/jpg"> <br>
                                </div>
                            </div>


                            <br />
                            <hr />
                            <div class="row">
                                <div class="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="accept_consent_chk" id="accept_consent_chk" />

                                        ข้าพเจ้ายอมรับกฎระเบียบ 10 ประการของชมรมนักดนตรีเปิดหมวกสายอาชีพ (SIAM STREET MUSICIANS)

                                        <span onclick="viewRules()" class="view_rules_text">อ่านกฎ</span>
                                    </div>
                                </div>
                            </div>
                            <br />

                            <div class="d-flex justify-content-center">
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary submit_btn" id="submit_btn" disabled>ส่งข้อมูล</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <br>

                </div>
            </div>
        </div>
    </div>

    <!-- Rules Modal -->
    <div class="modal fade" id="rulesModal" tabindex="-1" aria-labelledby="rulesModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="rulesModalLabel">กฎระเบียบ 10 ประการ</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h4>กฎระเบียบ 10 ประการ</h4>
                    <h4>ของชมรมนักดนตรีเปิดหมวกสายอาชีพ (SIAM STREET MUSICIANS)</h4>
                    </br>
                    1. แต่งกายสุภาพเรียบร้อย (ห้ามสวมรองเท้าแตะทุกกรณี)</br></br>

                    2. พูดจาสุภาพ, มารยาทดี ระมัดระวังคำพูดในการสื่อสาร</br></br>

                    3.ไม่สร้างปัญหาหรือความเดือดร้อนใดๆ ให้กับพื้นที่ ที่ไปทำการแสดง</br></br>

                    4. ไม่สูบบุหรี่ หรือดื่มสุราและสิ่งเสพติดอื่นๆ ในพื้นที่ ที่ทำการแสดงโดยเด็ดขาด</br></br>
                    5.ต้องช่วยกันรักษาภาพลักษณ์และชื่อเสียงของชมรม ไม่สร้างปัญหาใดๆ ที่ทำให้ชมรมเสื่อมเสีย
                    เสียหาย เสียชื่อเสียงทั้งในทางตรง และทางอ้อม</br></br>

                    6.เมื่อได้รับมอบหมายให้ไปปฏิบัติหน้าที่ ต้องตรงต่อเวลา - เตรียมความพร้อมก่อนเสมอ 
                    และรับผิดชอบงานของทางชมรม (หากมีเหตุขัดข้องหรือมีเหตุจำเป็นต้องรีบแจ้งให้ทางชมรมทราบ
                    ในทันที) เพื่อจะได้แก้ไข และมอบหมายให้สมาชิกท่านอื่นไปปฏิบัติหน้าที่แทน</br></br>

                    7. สมาชิกต้องสื่อสาร ประชาสัมพันธ์
                    ชื่อเสียงของชมรม และสถานที่แสดงนั้นๆ หรือร้านค้าข้างเคียง ในทางที่ดี
                    และเป็นประโยชน์กับทุกฝ่าย</br></br>

                    8. สมาชิกต้องไม่ทำผิดกฎหมาย
                    ของบ้านเมือง และอยู่ในศีลธรรมอันดี
                    ช่วยสังคม สำนึก"รู้รักสามัคคีของคนในชาติ"</br></br>

                    9.สมาชิกยอมรับ และพร้อมปฏิบัติ
                    ในกฎข้อบังคับ และกฎระเบียบของสถานที่ ที่ไปทำการแสดงโดยเคร่งครัด</br></br>

                    10. หากสืบทราบว่าสมาชิกในชมรมละเมิดกฎข้อบังคับข้อใดข้อ หนึ่งของทางชมรม ทางชมรมมีสิทธิ์เพิกถอนการเป็นสมาชิกของท่านกับทางชมรมได้ทันที
                    </br></br>
                    บัตรจะมีอายุ 1 ปีหลังจากวันที่ทางชมรมได้ออกบัตรให้กับสมาชิก
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>

    <?php
    include "script/birth_date_picker.php";
    include "script/toggle_checkbox_display.php";
    include "feature/register.php";
    ?>

    <script type="text/javascript">
        $(function() {
            $("#profile_image").on("change", function() {
                if ($("#profile_image")[0].files.length > 2) {
                    swal({
                        title: "เลือกรูปถ่ายประจำตัวได้แค่ 2 รูปเท่านั้น",
                        type: "error"
                    });

                    $("#profile_image").val(null);
                }
            });

            $("#accept_consent_chk").click(function() { // เมื่อคลิกที่ checkbox id=i_accept
                if ($(this).attr("checked") == "checked") { // ถ้าเลือก
                    $("#submit_btn").removeAttr("disabled"); // ให้ปุ่ม id=continue_bt ทำงาน สามารถคลิกได้
                } else { // ยกเลิกไม่ทำการเลือก
                    $("#submit_btn").attr("disabled", "disabled"); // ให้ปุ่ม id=continue_bt ไม่ทำงาน
                }
            });
        });

        function viewRules() {
            $("#rulesModal").modal("show");
        }

        $('#province_ddl').change(function() {
            var id_province = $(this).val();

            $.ajax({
                type: "POST",
                url: "./script/address_ddl.php",
                data: {
                    id: id_province,
                    function: 'provinces'
                },
                success: function(data) {
                    $('#district_ddl').html(data);
                    $('#subdistrict_ddl').html(' ');
                    $('#subdistrict_ddl').val(' ');
                    $('#zipcode_txt').val(' ');
                }
            });
        });

        $('#district_ddl').change(function() {
            var id_amphures = $(this).val();

            $.ajax({
                type: "POST",
                url: "./script/address_ddl.php",
                data: {
                    id: id_amphures,
                    function: 'amphures'
                },
                success: function(data) {
                    $('#subdistrict_ddl').html(data);
                }
            });
        });

        $('#subdistrict_ddl').change(function() {
            var id_districts = $(this).val();

            $.ajax({
                type: "POST",
                url: "./script/address_ddl.php",
                data: {
                    id: id_districts,
                    function: 'districts'
                },
                success: function(data) {
                    $('#zipcode_txt').val(data)
                }
            });

        });
    </script>
</body>

</html>