<?php
session_start();
if (!isset($_SESSION['ACESSIBLE'])) {
    header("location:login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>(ADMIN) SIAM STREET MUSICIAN</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br />
                <h3>รายชื่อสมาชิก
                    <a href="index.php" class="btn btn-info btn-sm">+สมัครสมาชิก</a>
                </h3>
                <font color="red">คนที่สมัครล่าสุด ชื่ออยู่บนสุด</font>
                <br />
                <br />

                <table class="table table-striped  table-hover table-responsive table-bordered" id="user_table" style="border:1;">
                    <thead class="thead-dark">
                        <tr>
                            <th width="10%">รหัส</th>
                            <th width="15%">ประเภทการแสดง</th>
                            <th width="13%">รูปโปรไฟล์</th>
                            <th width="20%">ชื่อ-นามสกุล</th>
                            <th width="10%">ฉายา</th>
                            <th width="12%">ตำแหน่ง</th>
                            <th width="10%">วันที่ลงทะเบียน</th>
                            <th width="10%">เพิ่มเติม</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        require_once 'connect.php';
                        $stmt = $conn->prepare("SELECT user.*, prov.name_th province_name, 
                                                    amp.name_th district_name, dist.name_th subdistrict_name FROM user_tb user
                                                LEFT JOIN provinces prov ON user.province_id = prov.id
                                                LEFT JOIN amphures amp ON user.district_id = amp.id
                                                LEFT JOIN districts dist ON user.subdistrict_id = dist.id
                                                ORDER BY user.created_date DESC");
                        $stmt->execute();
                        $result = $stmt->fetchAll();
                        $rowNo = 0;

                        $performTypes = array("- ดนตรี<br/>" => "1", "- ร้องเพลง<br/>" => "2", "- Street Show<br/>" => "3", "- อื่น ๆ (:perform_type_other_str)" => "4");

                        foreach ($result as $row) {
                            $performText = "";
                            $dateFormat = "d/m/Y H:i:s";
                            $createdDateStr =  date_create($row['created_date']);
                            $brithDateStr =  date_create($row['birth_date']);
                            $brithDateStr =  date_create($row['birth_date']);

                            $createdDateFormat = date_format($createdDateStr, "Y-m-d H:i:s");
                            $createdDate = date($dateFormat, strtotime($createdDateFormat . " + 7 hours"));


                            $birthDate = date_format($brithDateStr, "d/m/Y");
                            $birthDateYear = date_format($brithDateStr, "d/m/Y");
                            $provinceId = $row["province_id"];
                            $provinceName = $row["province_name"];
                            $districtName = $row["district_name"];
                            $subDistrictName = $row["subdistrict_name"];
                            $zipcode = $row["zipcode"];
                            $userType = $row["user_type"];
                            $userTypeText = ($userType === 0 ? "ประธาน" : ($userType === 1 ? "คณะกรรมเการ" : ($userType === 200 ? "สมาชิก" : ($userType === 250 ? "สมาชิก+สติกเกอร์" : "สมาชิก"))));

                            if ($birthDateYear === '01/01/1970') {
                                $birthDate = "";
                            }


                            $fullAddress = $row['address_1'];
                            $fullAddress .= ($provinceId === "" ? "" : " จังหวัด$provinceName");
                            $fullAddress .= ($provinceId === "" || $districtName == "" ? "" : ($provinceId === "1" ? " เขต$districtName" : " อำเภอ$districtName"));
                            $fullAddress .= ($provinceId === "" || $subDistrictName == "" ? "" : ($subDistrictName === "1" ? " แขวง$subDistrictName" : " ตำบล$subDistrictName"));
                            $fullAddress .= " $zipcode";

                            if ($row["perform_type"] !== "") {
                                $performTypeIds = explode(",", $row["perform_type"]);

                                foreach ($performTypeIds as $performTypeId) {
                                    $performText .= array_search($performTypeId, $performTypes);

                                    if ($performTypeId === "4") {
                                        $performText = str_replace(":perform_type_other_str", $row["perform_type_other"], $performText);
                                    }
                                }
                            }

                            $rowNo++;
                        ?>
                            <tr>
                                <td class="text-center">TSM-<?= str_pad(strval($row['id']), 6, "0", STR_PAD_LEFT); ?></td>
                                <td><?= $performText; ?></td>
                                <td>
                                    <img onerror="this.onerror=null;this.src='asset/noimg.jpg'" src="upload/<?= $row['profile_image_1']; ?>" width="100px" onclick="viewFullImage('<?= $row['profile_image_1']; ?>')" />
                                </td>
                                <td><?= $row['first_name'] . " " . $row['last_name']; ?></td>
                                <td><?= $row['alias']; ?></td>
                                <td><?= $userTypeText; ?></td>
                                <td style="display: none;">
                                    <img onerror="this.onerror=null;this.src='asset/noimg.jpg';" src="upload/<?= $row['slip_image']; ?>" width="100px" onclick="viewFullImage('<?= $row['slip_image']; ?>')" />
                                </td>
                                <td><?= $createdDate; ?></td>
                                <td>
                                    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modeal_user_info_<?= $row['id']; ?>">
                                        View
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="modeal_user_info_<?= $row['id']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel_<?= $row['id']; ?>" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel_<?= $row['id']; ?>">ข้อมูลส่วนตัว</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container">
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">ชื่อ - นามสกุล :</div>
                                                            <div class="col-6 col-lg-7"><?= $row['first_name'] . " " . $row['last_name']; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">ฉายา :</div>
                                                            <div class="col-6 col-lg-7">
                                                                <?= $row['alias']; ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">ตำแหน่ง :</div>
                                                            <div class="col-6 col-lg-7"><?= $userTypeText; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">ประเภทการแสดง :</div>
                                                            <div class="col-6 col-lg-7"><?= $performText; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">เลขประจำตัวประชาชน :</div>
                                                            <div class="col-6 col-lg-7"><?= $row['personal_id']; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">วันเดือนปีเกิด :</div>
                                                            <div class="col-6 col-lg-7"><?= $birthDate; ?></div>
                                                        </div>

                                                        <hr />
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">เบอร์โทรศัพท์ :</div>
                                                            <div class="col-6 col-lg-7"><?= $row['telephone_1']; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">ที่อยู่ :</div>
                                                            <div class="col-6 col-lg-7"><?= $fullAddress; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">วันที่ลงทะเบียน :</div>
                                                            <div class="col-6 col-lg-7"> <?= $createdDate; ?></div>
                                                        </div>
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">รายละเอียดเพิ่มเติม :</div>
                                                            <div class="col-6 col-lg-7"> <?= $row["remark"]; ?></div>
                                                        </div>
                                                        <hr />
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">รูปบัตรแสดงความสามารถ :</div>
                                                            <div class="col-6 col-lg-7">
                                                                <?php if ($row['perform_card_image'] !== "") { ?>
                                                                    <img src="upload/<?= $row['perform_card_image']; ?>" width="150px" class="img-thumbnail m-2" onclick="viewFullImage('<?= $row['slip_image']; ?>')" />
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">รูปถ่ายประจำตัว :</div>
                                                            <div class="col-6 col-lg-7">
                                                                <?php
                                                                for ($i = 1; $i <= 5; $i++) {
                                                                    $profileImagePath = $row['profile_image_' . $i];
                                                                    if (!is_null($profileImagePath)) {

                                                                ?>
                                                                        <img src="upload/<?= $profileImagePath; ?>" width="100px" class="img-thumbnail m-2" onclick="viewFullImage('<?= $profileImagePath; ?>')" />
                                                                <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </div>
                                                        </div>

                                                        <hr />
                                                        <div class="row mb-2">
                                                            <div class="col fw-bold">หลักฐานการโอนเงิน :</div>
                                                            <div class="col-6 col-lg-7">
                                                                <img src="upload/<?= $row['slip_image']; ?>" width="150px" class="img-thumbnail m-2" onclick="viewFullImage('<?= $row['slip_image']; ?>')" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" onclick="deleteUser(<?= $row['id']; ?>, '<?= $row['first_name']; ?> <?= $row['last_name']; ?>')">
                                                        ลบสมาชิก
                                                    </button>
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ปิด</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>

                            <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modal_full_image" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <h5 class="modal-title" id="full_image_user_full_name"></h5>
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="toggleModalViewImage('modal_full_image')"></button>
                </div>
                <div class="modal-body">
                    <img onerror="this.onerror=null;this.src='asset/noimg.jpg'" id="full_img" src="" width="100%" />
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $("#user_table").DataTable();
        });

        function viewFullImage(imageFileName, fullName = "") {
            if (imageFileName === '') {
                $("#full_img").attr("src", "asset/noimg.jpg");
            } else {
                $("#full_img").attr("src", "upload/" + imageFileName);
            }

            toggleModalViewImage();
        }

        function toggleModalViewImage() {
            $('#modal_full_image').modal('toggle');
        }

        function deleteUser(id, fullName) {
            Swal.fire({
                title: "ต้องการลบช้อมูลสมาชิกหรือไม่?",
                text: "ยืนยันการลบข้อมูล: คุณ" + fullName,
                showCancelButton: true,
                confirmButtonText: "ยืนยัน",
                cancelButtonText: "ปิด",
                icon: "warning"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "./feature/delete_user.php",
                        data: {
                            id: id
                        },
                        success: function(data) {
                            Swal.fire({
                                title: "ลบข้อมูลเรียบร้อย",
                                confirmButtonText: 'ตกลง',
                                icon: "success"
                            }).then((result) => {
                                location.reload();
                            });
                        }
                    });

                }
            });
        }
    </script>
</body>

</html>