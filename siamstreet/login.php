<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>(ADMIN) SIAM STREET MUSICIAN</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style>
        body {
            background: #C5D8EC;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <br />
                <br />
                <br />
                <div class="card ">
                    <div class="card-body border" align="center">
                        <br />
                        <h3 class="text text-primary">เข้าสู่ระบบ Admin</h3>
                        <br />
                        <form method="post">
                            <div class="row">
                                <div class="col align-self-center">
                                    <input type="text" maxlength="6" minlength="6" required name="password" 
                                        class="form-control" style="width:250px;" placeholder="รหัสผ่าน">
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary col-3">ยืนยัน</button>
                                </div>
                            </div>
                            <div class="row" id="incorrect_content" style="display: none;">
                                <div class="col text-start">
                                    <br />
                                    <font color="red">**รหัสผ่านไม่ถูกต้อง</font>
                                </div>
                            </div>
                        </form>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    session_start();
    if (isset($_SESSION['ACESSIBLE'])) {
        header("location:admin.php");
        exit();
    }

    if (isset($_POST["password"])) {

        if ($_POST["password"] === "900109") {
            $_SESSION['ACESSIBLE'] = "PASS";
            header("location:admin.php");
        } else {
            echo '<script>$("#incorrect_content").show();</script>';
        }
    }

    ?>
</body>

</html>