<script>
    function toggleCheckbox(chkbox, obj, txtBox = "") {
        var el = document.getElementById(obj);

        if(txtBox !== "")
        {
            var txtEl = document.getElementById(txtBox);
            txtEl.value = "";
        }
        
        el.style.display = (chkbox.checked ? '' : 'none' );        
    }
</script>