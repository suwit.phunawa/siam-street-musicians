<?php
if (isset($_POST['first_name']) && isset($_POST['last_name'])) {
    require_once 'helper/resize_image.php';
    require_once 'helper/validate_file_size.php';
    
    $firstName = $_POST['first_name'];
    $lastName = $_POST['last_name'];
    $alias = $_POST['alias'];
    $personalId = $_POST['personal_id_txt'];
    $telephone1 = $_POST['telephone1_txt'];
    $remark = $_POST['remark_txt'];
    $userType = $_POST['register_type_rdb'];

    //Perform
    $performOtherText = $_POST["perform_other_txt"];
    $performIds = [];

    if (isset($_POST['perform_music_chk'])) {
        $performIds[] = 1;
    }

    if (isset($_POST['perform_sing_chk'])) {
        $performIds[] = 2;
    }

    if (isset($_POST['perform_street_chk'])) {
        $performIds[] = 3;
    }

    if (isset($_POST['perform_other_chk'])) {
        $performIds[] = 4;
    }


    $address1 = $_POST["address_1_txt"];
    $province = $_POST["province_ddl"];
    $district = $_POST["district_ddl"];
    $subdistrict = $_POST["subdistrict_ddl"];
    $zipcode = $_POST["zipcode_txt"];

    $birthDateStr = strval($_POST['birth_date']);
    $birthDateFormatted = "";


    if($birthDateStr !== "")
    {
        $birthDateStrs = explode("/", $birthDateStr);
        $birthDateTimstamp = strtotime($birthDateStrs[2]."/".$birthDateStrs[1]."/".$birthDateStrs[0]);
        $birthDateFormatted = date("Y-m-d", $birthDateTimstamp);
    }

   
    $path = "upload/";
    $imageDate = date("Ymd_His");
    $profileImageQuery = "";
    //$profileNumber = 1;
    //$file_ary_names = [];

    ///อัพโหลดภาพ Profile (หลายไฟล์)
    /*if ($_FILES['profile_image']) {
            require_once "helper/re_array_file.php";
            $file_ary = reArrayFiles($_FILES['profile_image']);                    

            foreach ($file_ary as $file) {                        

                validateFileSize($file);

                try{
                    $numrand = (mt_rand());
                    $typefile = strrchr($file['name'],".");       
                    $profileImageName = $numrand.$imageDate.$typefile;
                    $profilePath = $path.$profileImageName;

                    $profileImageQuery .= ", :profile_image_$profileNumber";

                    $file_ary_names[$profileNumber] = $profileImageName;
                    move_uploaded_file($file['tmp_name'],$profilePath); 
                    
                    $profileNumber++;    
                } 
                catch (Exception $ex)
                {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }                    
            }
        }*/

    if ($_FILES['profile_image_1']['size'] > 0) {                
        $profileImageQuery .= ", :profile_image_1";
        $file_ary_names[1] = createResizedImage($_FILES['profile_image_1']);        
    }

    if ($_FILES['profile_image_2']['size'] > 0) {                
        $profileImageQuery .= ", :profile_image_2";
        $file_ary_names[2] = createResizedImage($_FILES['profile_image_2']);        
    }

    ///Upload ภาพสลิปการโอนเงิน
    $slipImageName = "";

    if ($_FILES['slip_image']['size'] > 0) {
        $slipImageName = createResizedImage($_FILES['slip_image']);   
    }

    $performCardImage = "";

    if ($_FILES['perform_card_image']['size'] > 0) {
        $performCardImage = createResizedImage($_FILES['perform_card_image']);   
    }

    $insertUserStr = "INSERT INTO user_tb (perform_type, perform_type_other, first_name, last_name, alias, birth_date, address_1," .
        "province_id, district_id, subdistrict_id, zipcode, personal_id, telephone_1," .
        "created_date, created_by, updated_date, updated_by, perform_card_image, user_type" .
        str_replace(":", "", $profileImageQuery) .
        ", slip_image, remark) " .
        "VALUES (:perform_type, :perform_type_other, :first_name, :last_name, :alias, :birth_date, :address_1," .
        ":province_id, :district_id, :subdistrict_id, :zipcode, :personal_id, :telephone_1," .
        "NOW(), 'ADMIN', NOW(), 'ADMIN', '$performCardImage', :user_type" .
        $profileImageQuery .
        ", '$slipImageName', :remark)";

    $stmt = $conn->prepare($insertUserStr);

    $stmt->bindParam(':first_name', $firstName, PDO::PARAM_STR);
    $stmt->bindParam(':last_name', $lastName, PDO::PARAM_STR);
    $stmt->bindParam(':alias', $alias, PDO::PARAM_STR);
    $stmt->bindParam(':personal_id', $personalId, PDO::PARAM_STR);
    $stmt->bindParam(':telephone_1', $telephone1, PDO::PARAM_STR);
    $stmt->bindParam(':address_1', $address1, PDO::PARAM_STR);
    $stmt->bindParam(':province_id', $province, PDO::PARAM_INT);
    $stmt->bindParam(':district_id', $district, PDO::PARAM_INT);
    $stmt->bindParam(':subdistrict_id', $subdistrict, PDO::PARAM_INT);
    $stmt->bindParam(':zipcode', $zipcode, PDO::PARAM_STR);
    $stmt->bindParam(':birth_date', $birthDateFormatted, PDO::PARAM_STR);
    $stmt->bindParam(':perform_type_other', $performOtherText, PDO::PARAM_STR);
    $stmt->bindParam(':remark', $remark, PDO::PARAM_STR);
    $stmt->bindParam(':user_type', $userType, PDO::PARAM_INT);
    
    if (count($performIds) > 0) {
        $stmt->bindParam(':perform_type', implode(',', $performIds), PDO::PARAM_STR);
    } else {
        $perfromNull = "";
        $stmt->bindParam(':perform_type', $perfromNull, PDO::PARAM_STR);
    }


    if (count($file_ary_names) >= 1) {
        $stmt->bindParam(":profile_image_1", $file_ary_names[1]);
    }

    if (count($file_ary_names) >= 2) {
        $stmt->bindParam(":profile_image_2", $file_ary_names[2]);
    }

    /*
    if (count($file_ary_names) >= 3) {
        $stmt->bindParam(":profile_image_3", $file_ary_names[3]);
    }

    if (count($file_ary_names) >= 4) {
        $stmt->bindParam(":profile_image_4", $file_ary_names[4]);
    }

    if (count($file_ary_names) >= 5) {
        $stmt->bindParam(":profile_image_5", $file_ary_names[5]);
    }
    */

    $result = $stmt->execute();



    if ($result) {
        echo '<script>
                    setTimeout(function() {
                    swal({
                        title: "เพิ่มข้อมูลสำเร็จ",
                        type: "success"
                    }, function() {
                        window.location = "index.php";
                    });
                    }, 1000);
                </script>';
    } else {
        echo '<script>
                    setTimeout(function() {
                    swal({
                        title: "เกิดข้อผิดพลาด",
                        type: "error"
                    }, function() {
                        window.location = "index.php"; 
                    });
                    }, 1000);
                </script>';
    }

    $conn = null;
}
