<?php
require_once('../config/fruit_data.php');
require_once('../connect.php');
$curDateDB = date("Y-m-d");

$stmt = $conn->prepare("SELECT * FROM fruit_order_detail_tb WHERE created_date = '$curDateDB' ORDER BY id ASC");
$stmt->execute();
$result = $stmt->fetchAll();

$totalPrice = 0;

foreach ($result as $row) {
    $price = $row['price'];
    $totalPrice += $price;
    $orderId = $row['id'];
?>
    <div class="row item_row p-1">
        <div class="col-7">
            <?= $fruitNames[$row['item_id']]['NAME']; ?>
        </div>

        <div class="col text-end">
            <?= number_format($price, 0); ?>
        </div>

        <div class="col-2">
            <button class="btn btn-danger" onclick="deleteOrder('<?= $orderId; ?>')">ลบ</button>
        </div>
    </div>
<?php } ?>


<div class="row p-1 summary_footer mt-3">
    <div class="col-7">
        รวม
    </div>

    <div class="col text-end">
        <b><?= number_format($totalPrice, 0); ?></b>
    </div>

    <div class="col-2">

    </div>
</div>