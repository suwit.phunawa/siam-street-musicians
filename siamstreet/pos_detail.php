<?php
date_default_timezone_set('Asia/Bangkok');
$curDate = date("d/m/Y", strtotime('+543 years'));

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.7.2.custom.css">

    <style>
        body {
            background: #FFF8CA;
        }

        .item_row {
            border-bottom: solid 1px #ccc;
        }

        .sale_items {
            font-size: 1.5rem;
        }

        .sale_header {
            font-size: 1.5rem;
            top: 0;
            bottom: 0;
            position: sticky;
            overflow-y: scroll;
            overflow-x: hidden;
            background: white;
        }

        .summary_footer {
            color: green;
        }

        .add_order {
            position: fixed;
            bottom: 0;
            top: auto;
            padding: 10px;
            background-color: #CCC;
        }

        .sale_btn {
            font-size: 1.5rem;
        }

        .item_row {
            border: solid 1px #CCC;
        }
    </style>
</head>

<body>
    <div class="container p-1">
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <br />
                <div class="card ">
                    <div class="card-body border  card_body" style="background-color: white;">
                        <div class="sale_header p-1">
                            <div class="alert alert-success">
                                <h2>รายการขาย</h2>
                                <div class="row mt-3">
                                    <div class="col-4">วันที่ : </div>
                                    <div class="col-3 p-0">
                                        <input type="text" id="order_date_txt" style="width:150px;" value="<?= $curDate; ?>" />
                                    </div>
                                </div>
                            </div>


                            <div class="row p-1" style="background:#CCC;">
                                <div class="col-7">
                                    <b>รายการ</b>
                                </div>

                                <div class="col text-end">
                                    <b>ราคา</b>
                                </div>

                                <div class="col-2">
                                    <b></b>
                                </div>
                            </div>
                        </div>

                        <div class="sale_items p-2" id="sale_items">

                        </div>

                    </div>
                </div>

            </div>


        </div>
        <br />
        <br />
        <br />
        <br />
    </div>

    <div class="add_order col-12" align="center">
        <button class="btn btn-success sale_btn" onclick="goToSalePage()">ขาย</button>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>

    <script>
        function goToSalePage() {
            window.location = "pos.php";
        }

        function deleteOrder(orderId) {
            Swal.fire({
                title: "ต้องการลบรายการขายหรือไม่ ?",
                showCancelButton: true,
                confirmButtonText: "ยืนยัน",
                cancelButtonText: "ปิด",
                icon: "warning"
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location = "feature/pos_delete_order.php?orderId=" + orderId;
                }
            });
        }
    </script>

    <script>
        $(function() {
            $.ajax({
                type: "GET",
                url: "./feature/get_all_pos_order.php",
                success: function(data) {
                    $("#sale_items").html(data);

                    window.scrollTo(0, document.body.scrollHeight);
                }
            });

            var dateBefore = null;
            $("#order_date_txt").datepicker({
                dateFormat: 'dd/mm/yy',
                showOn: 'focus',
                buttonImageOnly: false,
                dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
                    'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
                ],
                changeMonth: true,
                changeYear: true,
                beforeShow: function() {
                    if ($(this).val() != "") {
                        var arrayDate = $(this).val().split("/");
                        arrayDate[2] = parseInt(arrayDate[2]) - 543;
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }
                    setTimeout(function() {
                        $.each($(".ui-datepicker-year option"), function(j, k) {
                            var textYear = parseInt($(".ui-datepicker-year option").eq(
                                j).val()) + 543;
                            $(".ui-datepicker-year option").eq(j).text(textYear);
                        });
                    }, 50);
                },
                onChangeMonthYear: function() {
                    setTimeout(function() {
                        $.each($(".ui-datepicker-year option"), function(j, k) {
                            var textYear = parseInt($(".ui-datepicker-year option").eq(
                                j).val()) + 543;
                            $(".ui-datepicker-year option").eq(j).text(textYear);
                        });
                    }, 50);
                },
                onClose: function() {
                    if ($(this).val() != "" && $(this).val() == dateBefore) {
                        var arrayDate = dateBefore.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]) + 543;
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }
                },
                onSelect: function(dateText, inst) {
                    dateBefore = $(this).val();
                    var arrayDate = dateText.split("/");
                    arrayDate[2] = parseInt(arrayDate[2]) + 543;
                    $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                }

            });


        });
    </script>



</body>

</html>