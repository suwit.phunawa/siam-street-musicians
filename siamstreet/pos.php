<?php
require_once('config/fruit_data.php');
require_once('connect.php');
$curDateDB = date("Y-m-d");

$stmt = $conn->prepare("SELECT SUM(price) totalPrice FROM fruit_order_detail_tb WHERE created_date = '$curDateDB' ORDER BY id ASC");
$stmt->execute();
$fetchResult = $stmt->fetch(PDO::FETCH_ASSOC);
$totalPrice = $fetchResult['totalPrice'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style>
        body {
            background: #FFF8CA;
        }

        .item-text {
            font-size: 22px;
        }

        .card_body {
            background: #581845;
        }

        .summary_bar {
            position: fixed;
            bottom: 0;
            background: #34495E;
            color: white;
            width: 100%;
        }

        .summary_text {
            font-size: 3rem;
        }

        .number {
            width: 100%;
            margin-bottom: 5px;
            font-size: 2rem;
            font-weight: 400;
        }

        .cal-symbol {
            width: 100%;
            margin-bottom: 5px;
            font-size: 2rem;
            font-weight: 400;
        }

        .action_btn {
            font-size: 1.5rem;
            font-weight: 400;
        }

        /* input */
        #input-wrap {
            position: relative;
            width: calc(100% - 4px);
            margin-bottom: 5px;
            padding: 0 5px;
            background: #a1bd66;
            text-align: right;
            box-shadow: inset 0px 0px 15px 0px rgba(0, 0, 0, 0.5);
        }

        #input {
            height: 5rem;
            line-height: 5rem;
            font-size: 2rem;
        }

        #tmp {
            position: absolute;
            right: 5px;
            top: 0.25rem;
            color: grey;
        }

        .fruit_img {
            height: 120px !important;
            width: 100% !important;
        }

        :root {
            touch-action: pan-x pan-y;
            height: 100%
        }

        @media screen and (-webkit-min-device-pixel-ratio:0) {

            select,
            textarea,
            input {
                font-size: 16px;
            }
        }

        @media screen and (-webkit-min-device-pixel-ratio:0) {

            select:focus,
            textarea:focus,
            input:focus {
                font-size: 16px;
            }
        }
    </style>
</head>

<body>
    <div class="container p-1">
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <br />
                <div class="card ">
                    <div class="card-body border card_body" align="center">

                        <?php
                        $itemInRow = 0;

                        foreach ($fruitNames as $key => $value) {

                            if ($itemInRow === 0) {
                                echo "<div class='row'>";
                            }

                        ?>

                            <div class="col p-1">
                                <div class="card ">
                                    <div class="card-body p-0">
                                        <img src="<?= $value['IMAGE']; ?>" onclick="openInputModal('<?= $key; ?>')" class="img-thumbnail fruit_img">
                                        <span class="item-text">
                                            <?= $value['NAME']; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        <?php


                            $itemInRow++;

                            if ($itemInRow === 3) {
                                echo "</div>";
                                $itemInRow = 0;
                            }
                        }



                        ?>

                    </div>
                </div>

                <div class="modal fade" id="inputModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div id="calculator">
                                    <div id="input-wrap">
                                        <div id="tmp"></div>
                                        <div id="input"></div>
                                    </div>
                                    <div id="button-wrap">
                                        <div class="row">
                                            <div class="col p-1"><button class="btn btn-dark cal-symbol" id="all-clear">AC</button></div>
                                            <div class="col p-1"><button class="btn btn-dark cal-symbol" id="clear">C</button></div>
                                            <div class="col p-1"><button class="btn btn-dark cal-symbol" id="sign">+/-</button></div>
                                            <div class="col p-1"><button class="btn btn-dark cal-symbol amt divide">/</button></div>
                                        </div>


                                        <div class="row">
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">7</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">8</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">9</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-dark amt times cal-symbol">*</button>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">4</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">5</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">6</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-dark amt minus cal-symbol">-</button>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">1</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">2</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-secondary number">3</button>
                                            </div>
                                            <div class="col p-1">
                                                <button class="btn btn-dark amt plus cal-symbol">+</button>
                                            </div>
                                        </div>




                                        <div class="row">
                                            <div class="col col-6 p-1">
                                                <button class="btn btn-secondary number num-0">0</button>
                                            </div>
                                            <div class="col p-1">
                                                <button id="dot" class="btn btn-dark cal-symbol">.</button>
                                            </div>
                                            <div class="col p-1">
                                                <button id="result" class="btn btn-warning cal-symbol">=</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger action_btn" data-bs-dismiss="modal">ปิด</button>
                                <button type="button" class="btn btn-success action_btn" onclick="confirmOrder()">ยืนยัน</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <br />
        <br />
        <br />
        <br />
    </div>


    <div class="summary_bar row m-0">
        <div class="col pt-2">
            <button class="btn btn-warning" onclick="goToOrderDetailPage()">
                <h4>รายการขาย</h4>
            </button>
        </div>
        <div class="col-7  text-end align-middle">
            <h1 class="summary_text"><?= number_format($totalPrice, 0); ?> ฿</h1>
        </div>

    </div>

    <script>
        const input = document.getElementById("input");
        const tmp = document.getElementById("tmp");
        let itemId = "";

        function openInputModal(id) {
            itemId = id;
            $("#inputModal").modal("show");
        }

        function confirmOrder() {
            input.innerText = eval(tmp.innerText + input.innerText);

            if (input.innerText && input.innerText !== "undefined") {
                empty(tmp);
                const price = document.getElementById("input").innerText;

                window.location = "feature/pos_add_order.php?price=" + price + "&itemId=" + itemId;
            } else {
                input.innerText = "";
            }

        }

        function goToOrderDetailPage() {
            window.location = "pos_detail.php";
        }


        const empty = (element) => {
            element.innerText = "";
        };

        document.getElementById("all-clear").addEventListener("click", () => {
            empty(input);
            empty(tmp);
        });

        document.getElementById("clear").addEventListener("click", () => {
            empty(input);
        });

        document.querySelectorAll(".number").forEach((element) => {
            element.addEventListener("click", () => {
                if (input.innerText.length > 19)
                    return alert("최대 입력 범위를 초과했습니다!");

                input.innerText += element.innerText;
            });
        });

        document.getElementById("dot").addEventListener("click", () => {
            if (input.innerText.includes(".")) return;

            input.innerText += ".";
        });

        document.getElementById("sign").addEventListener("click", () => {
            if (input.innerText.startsWith("-")) {
                input.innerText = input.innerText.slice(1);
            } else {
                input.innerText = `-${input.innerText}`;
            }
        });

        document.querySelectorAll(".amt").forEach((element) => {
            element.addEventListener("click", () => {
                if (input.innerText) {
                    if (tmp.innerText) {
                        tmp.innerText = `${tmp.innerText} ${input.innerText} ${element.innerText}`;
                    } else {
                        tmp.innerText = `${input.innerText} ${element.innerText}`;
                    }
                } else if (tmp.innerText.slice(-1).match(/-|\+|\*|\//)) {
                    let string = tmp.innerText.slice(0, -1);
                    string += element.innerText;

                    tmp.innerText = string;
                }

                empty(input);
            });
        });

        document.getElementById("result").addEventListener("click", () => {
            if (input.innerText) {
                input.innerText = eval(tmp.innerText + input.innerText);
                empty(tmp);
            }
        });
    </script>

    <script type="text/javascript">
        document.addEventListener('touchmove', function(event) {
            if (event.scale !== 1) {
                event.preventDefault();
            }
        }, {
            passive: false
        });
    </script>
</body>

</html>