<?php
    function validateFileSize($file)
    {        
        $maxsize = 10097152;

        if($file["size"] >= $maxsize)
        {
            echo '<script>
                    setTimeout(function() {
                        swal({
                            title: "เกิดข้อผิดพลาด",
                            text: "ไฟล์ภาพต้องไม่เกิน 4 mb",
                            type: "error"
                        });
                    }, 1000);
                </script>';
            exit();
        }
    }
?>
