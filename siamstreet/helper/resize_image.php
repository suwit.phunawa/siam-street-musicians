<?php
function imageResize($imageResourceId, $sourceProperties)
{
    $width = $sourceProperties[0];
    $height = $sourceProperties[1];

    $targetWidth = $width < 1280 ? $width : 1280;
    $targetHeight = ($height / $width) * $targetWidth;

    $targetLayer = imagecreatetruecolor($targetWidth, $targetHeight);
    imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);

    return $targetLayer;
}


function createResizedImage($file)
{
    require_once 'validate_file_size.php';
    validateFileSize($file);

    $fileName = $file['tmp_name'];
    $folderPath = "upload/";
    $imageDate = date("Ymd_His");
    $sourceProperties = getimagesize($fileName);
    $randTime = time();
    $randNum = rand(1000000, 9999999);
    $fileNewName = $randTime . $randNum . $imageDate;

    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
    $imageType = $sourceProperties[2];
    $orientation = 0;
    $targetPath = "$folderPath$fileNewName.$ext";

    if (function_exists('exif_read_data')) {

        $exif = @exif_read_data($fileName);

        if ($exif && isset($exif['Orientation'])) {
            $orientation = $exif['Orientation'];
        }
    };


    switch ($imageType) {
        case IMAGETYPE_PNG:
            $imageResourceId = imagecreatefrompng($fileName);
            $targetLayer = imageResize($imageResourceId, $sourceProperties);
            imagepng($targetLayer, $targetPath);
            break;

        case IMAGETYPE_GIF:
            $imageResourceId = imagecreatefromgif($fileName);
            $targetLayer = imageResize($imageResourceId, $sourceProperties);
            imagegif($targetLayer, $targetPath);
            break;

        case IMAGETYPE_JPEG:
            $imageResourceId = imagecreatefromjpeg($fileName);
            $targetLayer = imageResize($imageResourceId, $sourceProperties);
            imagejpeg($targetLayer, $targetPath);
            break;

        default:
            echo '<script>
                    setTimeout(function() {
                        swal({
                            title: "เกิดข้อผิดพลาด",
                            text: "กรุณาเลือกไฟล์ภาพ",
                            type: "error"
                        });
                    }, 1000);
                </script>';
            exit();
            exit;
            break;
    }

    correctImageOrientation($orientation, $targetPath);

    return "$fileNewName.$ext";
}

function imgSize($img)
{
    $targetWidth = $img[0] < 1280 ? $img[0] : 1280;
    $targetHeight = ($img[1] / $img[0]) * $targetWidth;
    return [round($targetWidth, 2), round($targetHeight, 2)];
}

function correctImageOrientation($orientation, $resizedFile)
{
    $orientations = array(180 => 3, 270 => 6, 90 => 8);
    $deg = array_search($orientation, $orientations);

    if ($deg) {
        $sourceProperties = getimagesize($resizedFile);
        $imageType = $sourceProperties[2];

        switch ($imageType) {
            case IMAGETYPE_PNG:
                $img = imagecreatefrompng($resizedFile);
                $img = imagerotate($img, $deg, 0);
                imagejpeg($img, $resizedFile, 95);
                break;

            case IMAGETYPE_GIF:
                $img = imagecreatefromgif($resizedFile);
                $img = imagerotate($img, $deg, 0);
                imagejpeg($img, $resizedFile, 95);
                break;

            case IMAGETYPE_JPEG:
                $img = imagecreatefromjpeg($resizedFile);
                $img = imagerotate($img, $deg, 0);
                imagejpeg($img, $resizedFile, 95);
                break;

            default:
                exit;
                break;
        }
    }
}
