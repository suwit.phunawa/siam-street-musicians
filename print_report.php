<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: admin.php");
}

include("function/connect.php");
include("function/FunctionDate.php");

$activePage = "REPORT";

$startDate = isset($_GET["txtStartDate"]) ? $_GET["txtStartDate"] : "";
$endDate = isset($_GET["txtEndDate"]) ? $_GET["txtEndDate"] : "";
$searchTimePeriodType = isset($_GET["search_time_period_type"]) ? $_GET["search_time_period_type"] : "DAY";

$curMonth = isset($_GET["search_month_ddl"]) ? $_GET["search_month_ddl"] : date("m") - 1;
$curYear = isset($_GET["search_year_ddl"]) ? $_GET["search_year_ddl"] : date("Y");

if ($searchTimePeriodType == "YEAR") {
  $startDate = "01/01/$curYear";
  $endDate = "31/12/$curYear";
} else if ($searchTimePeriodType == "MONTH") {
  $month = str_pad($curMonth + 1, 2, '0', STR_PAD_LEFT);
  $engMonth = $engMonths[$curMonth];
  $monthYearStr = ($curYear) . "-" . ($curMonth + 1) . "-01";
  $monthYearTime  = strtotime($monthYearStr);
  $monthYearTime = strtotime('-1 second', strtotime('+1 month', $monthYearTime));
  $lastDayOfMonth = date('d/m/Y', $monthYearTime);

  $startDate = "01/$month/$curYear";
  $endDate = $lastDayOfMonth;
}

$yearOptionsContent = "";

for ($i = 2023; $i <= 2030; $i++) {
  $yearOptionsContent .= "<option value='$i'";
  $yearOptionsContent .= ($i == $curYear ? " selected " : "");
  $yearOptionsContent .= " >$i</option>";
}
?>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">


  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <title>ORDER SYSTEM</title>

  <!-- Custom fonts for this template-->
  <link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">


  <script language="JavaScript">
    function SubmitF(viewMode) {
      var msg = '';
      var me = document.form1;
      if (me.txtStartDate.value == '') {
        msg += ' กรุณาเลือกวันที่เริ่มต้น ! \n';
      }
      if (me.txtEndDate.value == '') {
        msg += ' กรุณาเลือกวันที่สิ้นสุด ! \n';
      }
      if (msg != '') {
        alert(msg);
      } else {

        $("#viewMode").val(viewMode);

        if (viewMode === "EXCEL") {
          $("form").removeAttr('target');
        } else {
          $("form").attr('target', '_blank');
        }

        me.submit();
      }
    }
  </script>

  <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <title>ORDER SYSTEM</title>

  <style type="text/css">
    td {
      font-family: Tahoma;
      font-size: 14px;
      color: #828282;
      text-decoration: none;
      border: none;
    }

    font {
      font-family: Tahoma;
      font-size: 14px;
      color: #1890E1;
      text-decoration: none;
      border: none;
      font-weight: bold;
    }

    .f {
      font-family: Tahoma;
      font-size: 14px;
      color: #FF2D00;
      text-decoration: none;
      border: none;
      font-weight: normal;
    }

    a {
      font-family: Tahoma;
      font-size: 14px;
      font-weight: bold;
      color: #FB5E3C;
      text-decoration: none;
      border: none;
    }

    .a {
      font-family: Tahoma;
      font-size: 14px;
      font-weight: normal;
      color: #FFFFFF;
      text-decoration: none;
      border: none;
    }

    .a1 {
      font-family: Tahoma;
      font-size: 14px;
      color: #40A3D8;
      font-weight: normal;
      text-decoration: none;
      border: none;
    }

    img {
      text-decoration: none;
      border: none;
    }

    .input_readonly {
      background-color: #FFFFFF !important;
    }

    .select2-container--default .select2-selection--single {
      padding: 2px;
      height: 35px !important;
      text-align: left;
      width: 100% !important;
      display: block !important;
    }

    .select2-container {
      width: 100% !important;
      height: 35px !important;
      display: block !important;

    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
      font-size: 30px;
      height: 30px;
      margin-left: 5px;
      position: absolute;
    }
  </style>
</head>

<body bgcolor="#FFFFFF">

  <div id="wrapper">

    <!-- Sidebar -->
    <?php include "sidebar.php"; ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column ">

      <!-- Main Content -->
      <div class="container-fluid container-md">
        <br />
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">ออกรายงาน</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

          <!-- Earnings (Monthly) Card Example -->
          <div class="col-xl-12 col-md-12 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
              <div class="card-body">

                <div class="row no-gutters">
                  <div class="col-md-12">
                    <div class="p-0">

                      <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                          <td>
                            <form name="form1" method="GET" action="" id="search_form">
                              <input type="hidden" name="txtCusName" id="txtCusName" value="" />
                              <input type="hidden" id="viewMode" name="act" value="EXCEL" />
                              <input type="hidden" name="hideStartDate" id="hideStartDate" value="<?php echo $startDate; ?>" class="form-control ml-1">
                              <input type="hidden" name="hideEndDate" id="hideEndDate" value="<?php echo $endDate; ?>" class="form-control ml-1">

                              <div class="form-inline">
                                <div class="form-group mb-2">
                                  <b>ช่วงเวลา</b>
                                </div>
                                <div class="form-group mx-sm-2 mb-2">
                                  <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary active">
                                      <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option1" value="DAY" autocomplete="off" <?php if ($searchTimePeriodType == "DAY") {
                                                                                                                                                                    echo " checked ";
                                                                                                                                                                  } ?>>
                                      วันที่
                                    </label>
                                    <label class="btn btn-secondary">
                                      <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option2" value="MONTH" autocomplete="off" <?php if ($searchTimePeriodType == "MONTH") {
                                                                                                                                                                      echo " checked ";
                                                                                                                                                                    } ?>>
                                      เดือน
                                    </label>
                                    <label class="btn btn-secondary">
                                      <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option3" value="YEAR" autocomplete="off" <?php if ($searchTimePeriodType == "YEAR") {
                                                                                                                                                                      echo " checked ";
                                                                                                                                                                    } ?>>
                                      ปี
                                    </label>
                                  </div>
                                </div>
                              </div>



                              <div class="form-inline">
                                <i class="fas fa-level-up-alt flipped ml-3 mb-2 text-primary"></i>

                                <?php if ($searchTimePeriodType == "DAY") { ?>
                                  <div class="form-group mb-2 ml-4">
                                    วันที่เริ่มต้น :
                                    <input type="text" name="txtStartDate" id="txtStartDate" value="<?php echo $startDate; ?>" class="form-control ml-1">
                                  </div>
                                  <div class="form-group mx-sm-1 mb-2 pl-3">
                                    สิ้นสุด :
                                    <input type="text" name="txtEndDate" id="txtEndDate" value="<?php echo $endDate; ?>" class="form-control ml-1">
                                  </div>
                                <?php } else if ($searchTimePeriodType == "MONTH") { ?>
                                  <div class="form-group mb-2 ml-4">
                                    เดือน :
                                    <select name="search_month_ddl" class="form-control ml-2" onchange="submitSearchForm()">
                                      <?php foreach ($thaiMonths as $key => $value) {
                                        echo "<option value='$key'";
                                        echo $curMonth == $key ? " selected " : "";
                                        echo ">$value</option>";
                                      }
                                      ?>
                                    </select>
                                  </div>
                                <?php }
                                if ($searchTimePeriodType == "YEAR" || $searchTimePeriodType == "MONTH") { ?>
                                  <div class="form-group mb-2 ml-4">
                                    ปี :
                                    <select name="search_year_ddl" class="form-control ml-2" onchange="submitSearchForm()">
                                      <?php echo $yearOptionsContent; ?>
                                    </select>
                                  </div>
                                <?php } ?>
                              </div>

                              <br />
                              <table width="60%" border="0" cellspacing="0" cellpadding="5">
                                <tr height="35px">
                                  <td>
                                    <div align="left">ประเภทรายงาน : </div>
                                  </td>
                                  <td>
                                    <select name="reportType" class="form-control" id="report_type_ddl">
                                      <option value="ORDER">ใบวางบิล</option>
                                      <option value="CUSTOMER_SALE">ยอดขาย รายลูกค้า</option>
                                      <option value="DAILY_SALE">ยอดขาย รายวัน</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr height="35px">
                                  <td>
                                    <div align="left">ประเภทใบสั่งซื้อ : </div>
                                  </td>
                                  <td>
                                    <select name="orderType" class="form-control" id="order_type_ddl">
                                      <option value="ALL">ทั้งหมด</option>
                                      <option value="EXCLUDE_INVOICE">"ไม่มี" เลขใบกำกับภาษี</option>
                                      <option value="INCLUDE_INVOICE">"มี" เลขใบกำกับภาษี (ยื่นภาษี)</option>
                                    </select>
                                  </td>
                                </tr>
                                <tr height="35px">
                                  <td>
                                    <div align="left">ลูกค้า : </div>
                                  </td>
                                  <td>
                                    <select name="ddlCustomer" id="ddlCustomer" class="form-control">
                                      <option value="">-- ลูกค้าทุกคน --</option>
                                      <?php

                                      $query = "(SELECT cus_name, cus_id FROM customer
                                  WHERE (cus_status = '1'))        
                                  UNION        
                                  (SELECT cus_name, cus_id FROM order_p 
                                  WHERE cus_name IS NOT NULL AND cus_name <> ''
                                  GROUP BY cus_name, cus_id)";
                                      $result = mysqli_query($c, $query);

                                      while ($row = mysqli_fetch_array($result)) {

                                      ?>
                                        <option value="<?php echo $row["cus_id"]; ?>" <?php if (isset($_GET["id"]) && ($_GET["id"] == $row["cus_id"])) {
                                                                                        echo "selected";
                                                                                      } ?>><?php echo $row["cus_name"]; ?></option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </td>
                                </tr>
                                
                                <tr height="35px">
                                  <td>&nbsp;</td>
                                  <td colspan="2">
                                    <button class="btn btn-light" style="border:1px solid #ccc;" type="button" onclick="exportExcel('VIEW')">เรียกดู</button>
                                    <button class="btn btn-primary" type="button" onclick="exportExcel('EXPORT')">ออกรายงาน</button>
                                  </td>
                                </tr>
                              </table>
                            </form>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="../#page-top">
    <i class="fas fa-angle-up"></i>
  </a>



  <!-- Bootstrap core JavaScript-->
  <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="js/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  <script src="Scripts/swal.js"></script>

  <script>
    $(document).ready(function() {
      $('#ddlCustomer').select2();

      $("#ddlCustomer").change(function() {
        var cusName = this.options[this.selectedIndex].text;
        $("#txtCusName").val(cusName);
      });

      $("#txtStartDate").datepicker({
        dateFormat: 'dd/mm/yy'
      });

      $("#txtEndDate").datepicker({
        dateFormat: 'dd/mm/yy'
      });

    });

    function exportExcel(viewMode) {
      const reportType = $("#report_type_ddl :selected").val();
      const orderType = $("#order_type_ddl :selected").val();
      let startDate = $("#txtStartDate").val();
      let endDate = $("#txtEndDate").val();
      let cusId = $("#ddlCustomer :selected").val();;
      let cusName = $("#txtCusName").val();;


      if (!startDate) {
        startDate = $("#hideStartDate").val();
        endDate = $("#hideEndDate").val();
      }

      const exportUrl = "function/exportExcel.php?txtStartDate=" + startDate + "&txtEndDate=" + endDate +
        "&ddlCustomer=" + cusId + "&txtCusName=" + cusName + "&reportType=" + reportType + "&orderType=" + orderType +
        "&viewMode=" + viewMode;

      window.open(exportUrl, "_url");
    }

    function submitSearchForm() {
      $("#search_form").submit();
    }
  </script>


</body>

</html>