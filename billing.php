<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: admin.php");
}

include("function/connect.php");
$activePage = "BILLING";

$orderDate = isset($_GET["txtDate"]) ? $_GET["txtDate"] : date("d/m/Y");

?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		td {
			font-family: Tahoma;
			font-size: 14px;
			color: #828282;
			text-decoration: none;
			border: none;
		}

		font {
			font-family: Tahoma;
			font-size: 14px;
			color: #1890E1;
			text-decoration: none;
			border: none;
			font-weight: bold;
		}

		.f {
			font-family: Tahoma;
			font-size: 14px;
			color: #FF2D00;
			text-decoration: none;
			border: none;
			font-weight: normal;
		}

		a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: bold;
			color: #FB5E3C;
			text-decoration: none;
			border: none;
		}

		.a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: normal;
			color: #FFFFFF;
			text-decoration: none;
			border: none;
		}

		.a1 {
			font-family: Tahoma;
			font-size: 14px;
			color: #40A3D8;
			font-weight: normal;
			text-decoration: none;
			border: none;
		}

		img {
			text-decoration: none;
			border: none;
		}

		.select2-container--default .select2-selection--single {
			padding: 2px;
			height: 35px !important;
			text-align: left;
			width: 100% !important;
			display: block !important;
		}

		.select2-container {
			width: 100% !important;
			height: 35px !important;
			display: block !important;

		}

		.select2-container--default .select2-selection--single .select2-selection__arrow {
			font-size: 30px;
			height: 50px;
			margin-right: 5px;
			position: absolute;
		}

		.select2-container--default .select2-results>.select2-results__options {
			height: 800px !important;
			max-height: 800px !important;
		}
	</style>

	<!-- Custom fonts for this template-->
	<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

	<script language="JavaScript">
		//****************************** Function ตรวจสอบข้อมูลใน TextBox ******************************

		function chkCus() {
			var msg = '';
			var me = document.form1;
			var txtDate = $("#txtDate").val();
			//alert(me.ddlCustomer.value);
			window.location = 'billing.php?id=' + me.ddlCustomer.value + '&txtDate=' + txtDate;
		}

		function chkPd() {
			var msg = '';
			var me = document.form1;
			var data_p = me.ddlProduct.value;
			var res = data_p.split(",");
			//alert(me.ddlProduct.value);
			me.txtPdId.value = res[0];
			me.txtPrice.value = res[1];
			me.txtUnit.value = res[2];
			me.txtNum.value = '';
			me.txtTotal.value = '';
		}

		function chkTotal() {
			var msg = '';
			var me = document.form1;
			var total = 0;
			if (me.txtPrice.value == '') {
				alert('กรุณาเลือกสินค้าก่อน');
				me.txtNum.value = '';
			} else {
				total = me.txtNum.value * me.txtPrice.value;
				me.txtTotal.value = total.toFixed(2);
			}
		}

		function SubmitF() {
			var msg = '';
			var me = document.form1;
			if (me.ddlCustomer.value == '') {
				msg = ' กรุณาเลือกลูกค้า ! \n';
			}
			if (me.ddlProduct.value == '') {
				msg += ' กรุณาเลือกสินค้า ! \n';
			}
			if (me.txtNum.value == '') {
				msg += ' กรุณาระบุจำนวนสินค้า ! \n';
			}
			if (me.txtTotal.value == '') {
				msg += ' กรุณาระบุจำนวนสินค้า ! \n';
			}
			if (msg != '') {
				alert(msg);
			} else {
				me.submit();
			}
		}

		function P_delete(id) {
			var me = document.form1;
			me.txtMode.value = 'delete';
			me.txtId.value = id;
			me.submit();
		}

		function ConfirmP(cus_id) {
			if (confirm('ยืนยันการสั่งสินค้า ?')) {
				var txtDate = $("#txtDate").val();
				window.location = 'p2_index.php?page=INDEX&id=' + cus_id + '&txtDate=' + txtDate;
			}
		}

		//***************************** End Function ***************************************************
	</script>

	<title>ORDER SYSTEM</title>
</head>

<body bgcolor="#FFFFFF">

	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "sidebar.php"; ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column ">

			<!-- Main Content -->
			<div class="container-fluid container-md">
				<br />
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">สั่งสินค้า</h1>
				</div>

				<!-- Content Row -->
				<div class="row">

					<!-- Earnings (Monthly) Card Example -->
					<div class="col-xl-12 col-md-12 mb-4">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">

								<div class="row no-gutters">
									<div class="col-md-5">
										<div class="p-0">

											<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
												<tr>
													<td colspan="2">
														<form name="form1" method="post" action="p_index.php">
															<table width="90%" border="0" cellspacing="5" cellpadding="5" align="center">
																<tr>
																	<td width="30%">&nbsp;</td>
																	<td width="70%">
																		<input type="hidden" name="txtMode" value="add" />
																		<input type="hidden" name="txtPage" value="NORMAL" />
																		<input type="hidden" name="txtId" />
																		<input type="hidden" name="txtPdId" />
																	</td>
																</tr>
																<tr>
																	<td>
																		<div align="right">วันที่สั่งสินค้า : </div>
																	</td>

																	<td><input type="text" class="form-control ml-3" name="txtDate" id="txtDate" value="<?php echo $orderDate; ?>" /></td>
																</tr>
																<tr>
																	<td>
																		<div align="right">ชื่อลูกค้า <span class="style8" style="color: #F80307">* </span> :</div>
																	</td>
																	<td class="pl-4 pr-0">
																		<select name="ddlCustomer" onChange="chkCus();" id="ddlCustomer" class="form-control">
																			<option value="">-- โปรดเลือกลูกค้า --</option>
																			<?php

																			$query = "	select * 
																from customer a 
																where a.cus_status = '1' 
															";
																			$result = mysqli_query($c, $query);

																			while ($row = mysqli_fetch_array($result)) {

																			?>
																				<option value="<?php echo $row["cus_id"]; ?>" <?php if (isset($_GET["id"]) && ($_GET["id"] == $row["cus_id"])) {
																																	echo "selected";
																																} ?>><?php echo $row["cus_name"]; ?></option>
																			<?php
																			}
																			?>
																		</select>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div align="right">ชื่อสินค้า <span class="style8" style="color: #F80307">* </span> : </div>
																	</td>
																	<td class="pl-4 pr-0"><select name="ddlProduct" onChange="chkPd();" id="ddlProduct" class="ddl_select">
																			<option value=",,">-- โปรดเลือกสินค้า --</option>
																			<?php
																			if (isset($_GET["id"])) {

																				$query = "	SELECT * 
																	FROM customer_price a, product b
																	WHERE b.pd_status = '1' and a.pd_id = b.pd_id and a.cus_id = '";
																				$query .= $_GET["id"] . "' order by a.pd_id";
																				$result = mysqli_query($c, $query);

																				while ($row = mysqli_fetch_array($result)) {

																			?>
																					<option value="<?php echo $row["pd_id"] . ',' . $row["cp_price"] . ',' . $row["pd_unit"]; ?>">
																						<?php echo $row["pd_name"]; ?></option>
																			<?php
																				}
																			}
																			?>
																		</select>
																		<?php if (!isset($_GET["id"])) { ?>
																			<span class="style8" style="color: #F80307">*กรุณาเลือกลูกค้าก่อน</span>
																		<?php } ?>
																	</td>
																</tr>
																<tr>
																	<td>
																		<div align="right">จำนวน <span class="style8" style="color: #F80307">* </span> :</div>
																	</td>
																	<td><input type="text" name="txtNum" maxlength="20" size="25" onChange="chkTotal();" class="form-control ml-3" /></td>
																</tr>
																<tr>
																	<td>
																		<div align="right">ราคา/หน่วย :</div>
																	</td>
																	<td><input type="text" name="txtPrice" maxlength="20" size="25" onChange="chkTotal();" class="form-control ml-3" /></td>
																</tr>
																<tr>
																	<td>
																		<div align="right">หน่วย : </div>
																	</td>
																	<td><input type="text" name="txtUnit" maxlength="20" size="25" onfocus=" blur();" class="form-control ml-3" /></td>
																</tr>
																<tr>
																	<td>
																		<div align="right">ราคารวม :</div>
																	</td>
																	<td><input type="text" name="txtTotal" maxlength="20" size="25" onfocus=" blur();" class="form-control ml-3" /></td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																	<td colspan="2">
																		<div align="center">
																			<input type="button" name="submit1" class="btn btn-primary" value="  Save  " onClick="SubmitF();">
																			&nbsp;&nbsp;&nbsp;
																			<input type="reset" name="submit2" class="btn btn-dark" value="  Reset  ">
																		</div>
																	</td>
																</tr>
															</table>
														</form>

													</td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;</td>
												</tr>
												<tr>
													<td colspan="2">

													</td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
												<tr>
													<td height="100%" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
											</table>
										</div>
									</div>

									<div class="col-md-7">
										<div class="p-3">

											<?php
											if (isset($_GET["id"])) {
												$price_total = 0;
												$currentPage = $_SERVER["PHP_SELF"];

												$maxRows_rsmem = 30;
												$pageNum_rsmem = 0;
												if (isset($_GET['pageNum_rsmem'])) {
													$pageNum_rsmem = $_GET['pageNum_rsmem'];
												}
												$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


												if (isset($_GET['pageNum_rsmem'])) {
													$pageNum_rsmem = $_GET['pageNum_rsmem'];
												}
												$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
												//คิวรี่ปกติ
												$query_rsmem = "SELECT *
													FROM order_p a, product b
													WHERE a.od_status = '1' AND a.pd_id = b.pd_id and a.cus_id = '$_GET[id]' order by a.od_id ASC
													";
												$query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

												$rsmem = mysqli_query($c, $query_limit_rsmem);
												$row_rsmem = mysqli_fetch_assoc($rsmem);

												if (isset($_GET['totalRows_rsmem'])) {
													$totalRows_rsmem = $_GET['totalRows_rsmem'];
												} else {
													$all_rsmem = mysqli_query($c, $query_rsmem);
													$totalRows_rsmem = mysqli_num_rows($all_rsmem);
												}
												$totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

												$queryString_rsmem = "";
												if (!empty($_SERVER['QUERY_STRING'])) {
													$params = explode("&", $_SERVER['QUERY_STRING']);
													$newParams = array();
													foreach ($params as $param) {
														if (
															stristr($param, "pageNum_rsmem") == false &&
															stristr($param, "totalRows_rsmem") == false
														) {
															array_push($newParams, $param);
														}
													}
													if (count($newParams) != 0) {
														$queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
													}
												}
												$queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

											?>

												<h4>บิลรอยืนยัน</h4>

												<table class="table table-bordered" id="box-table-a" width="80%" border="0" cellpadding="1" cellspacing="1" align="center">
													<tr class="a">
														<th width="279" align="center" valign="middle" bgcolor="#B5A8A8">
															ชื่อสินค้า
														</th>
														<th width="68" align="center" valign="middle" bgcolor="#B5A8A8">
															จำนวน
														</th>
														<th width="84" align="center" valign="middle" bgcolor="#B5A8A8">
															ราคา/หน่วย
														</th>
														<th width="81" align="center" valign="middle" bgcolor="#B5A8A8">
															รวม
														</th>
														<th width="60" align="center" valign="middle" bgcolor="#B5A8A8">
															จัดการ
														</th>
													</tr>
													<?php


													//เริ่มวน
													$unit_total = 0;
													$price_total = 0;

													if ($totalRows_rsmem > 0) { // Show if recordset not empty 
													?>
														<?php do {

														?>
															<tr>
																<td>
																	<div align="left"><?php echo "$row_rsmem[pd_name]"; ?></div>
																</td>
																<td>
																	<div align="right"><?php echo "$row_rsmem[od_num]"; ?></div>
																</td>
																<td>
																	<div align="right"><?php echo "$row_rsmem[od_priceperunit]"; ?></div>
																</td>
																<td>
																	<div align="right"><?php echo "$row_rsmem[od_total]"; ?></div>
																</td>
																<td>
																	<?php
																	$odId = $row_rsmem["od_id"];
																	?>
																	<div align="center" class="style10 ">
																		<button type="button" class="btn btn-danger" width="16" height="16" style="cursor:hand" onclick="P_delete('<?php echo $odId; ?>');">
																			<i class="fas fa-trash"></i>
																		</button>
																	</div>
																</td>
															</tr>
													<?php
															$unit_total += $row_rsmem["od_num"];
															$price_total += $row_rsmem["od_total"];
														} while ($row_rsmem = mysqli_fetch_assoc($rsmem));
													} // Show if recordset not empty 
													?>

													<tr>
														<td style="color: #050505; font-size: 16px; font-weight: bold;">
															<div align="left">รวมทั้งหมด</div>
														</td>
														<td style="color: #050505">
															<div align="right"><?php echo $unit_total; ?></div>
														</td>
														<td style="color: #050505">
															<div align="right"></div>
														</td>
														<td style="color: #050505">
															<div align="right"><?php echo number_format($price_total, 2, '.', ','); ?>
															</div>
														</td>
														<td style="color: #050505">
															<div align="center" class="style10 "></div>
														</td>
													</tr>
												</table>
												<br>
												<?php if ($price_total > 0) { ?>
													<div align="right">
														<input type="button" name="submit3" class="btn btn-success" value="  ยืนยันการสั่งสินค้า  " onClick="ConfirmP('<?php echo $_GET["id"]; ?>');">
													</div>
													<br>
												<?php } ?>
											<?php } ?>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="../#page-top">
		<i class="fas fa-angle-up"></i>
	</a>


	<!-- Bootstrap core JavaScript-->
	<script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="js/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="Scripts/swal.js"></script>

	<script src="js/datatables/jquery.dataTables.min.js"></script>
	<script src="js/datatables/dataTables.bootstrap4.min.js"></script>


	<script>
		$(document).ready(function() {
			$('#ddlProduct').select2();
			$('#ddlCustomer').select2();

			$("#txtDate").datepicker({
				dateFormat: 'dd/mm/yy'
			});
		});

		function handleEnter(event) {
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}
		}
	</script>

</body>

</html>