<?php
@session_start();

require_once __DIR__ . '/mpdf8/vendor/autoload.php';
//require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
ob_start(); // ทำการเก็บค่า html นะครับ

include("function/FunctionDate.php");
include("function/connect.php");
include("function/helper.php");


$query = "
      SELECT a.od_date, a.invoice_no, b.cus_name, b.cus_add, b.cus_tel, a.od_id, b.cus_id, b.cus_taxid, b.company_name
      FROM order_p a, customer b
      WHERE a.cus_id = b.cus_id and a.od_code = '" . $_GET["code"] . "'
      GROUP BY a.od_date, b.cus_name, b.cus_add, b.cus_tel, a.invoice_no, b.company_name
      ORDER BY a.od_id ASC
	";
$result = mysqli_query($c, $query);
$row = mysqli_fetch_array($result);

$orderDate = date_create($row["od_date"]);
$orderDate = date_format($orderDate, "d/m/Y");
$orderId = $row["od_id"];
$invoiceNo = $row["invoice_no"];
//$invoiceNo = $invoiceNo == "" ? "I" . date_format($orderDate, "Ym") . $orderId : sprintf('%05d', $invoiceNo);
//$invoiceNo = $invoiceNo == "" ? "" : sprintf('%05d', $invoiceNo);

$curDate = date("d/m/Y");
$cusTel = $row["cus_tel"];
$cusTel = $cusTel == "" ? "" : "<br/>โทร. $cusTel";
$themeColorCode = "#000000";

$cusName = $row["company_name"] == "" ? $row["cus_name"] : $row["company_name"];

?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <style>
    #item_table {
      border-collapse: collapse;
      border-spacing: 1px !important;
    }

    #item_table th {
      padding: 3px;
      color: #ffffff;
    }

    #item_table td {
      padding: 1px !important;
      height: 20px;
    }

    .fixed {
      position: fixed;
    }
  </style>


</head>

<body>
  <!-- Address -->
  <div style="padding-top:183;padding-left:129px; width:350px;" class="fixed">
    <?php echo $cusName; ?>
  </div>
  <div style="padding-top:224;padding-left:126px; width:350px;" class="fixed">
    <?php echo $row["cus_add"]; ?>
    <?php echo $cusTel; ?>
  </div>
  <div style="padding-top:303;padding-left:210px;" class="fixed">
    <?php echo $row["cus_taxid"]; ?>
  </div>

  <!-- Invoice No -->
  <div style="padding-top:192;padding-left:550px;" class="fixed">
    <?php echo $invoiceNo; ?>
  </div>
  <div style="padding-top:241;padding-left:550px;" class="fixed">
    <?php echo $orderDate; ?>
  </div>
  <div style="padding-top:290;padding-left:550px;" class="fixed">
     <?php //echo "AWCC ".$orderId; ?>
	-
  </div>


  <!-- Item List -->
  <div style="padding-top:376;" class="fixed">
    <?php
    include("function/connect.php");

    $currentPage = $_SERVER["PHP_SELF"];

    $maxRows_rsmem = 30;
    $pageNum_rsmem = 0;
    if (isset($_GET['pageNum_rsmem'])) {
      $pageNum_rsmem = $_GET['pageNum_rsmem'];
    }
    $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


    if (isset($_GET['pageNum_rsmem'])) {
      $pageNum_rsmem = $_GET['pageNum_rsmem'];
    }
    $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
    //คิวรี่ปกติ
    $query_rsmem = "
    SELECT *
    FROM order_p a, product b
    WHERE a.pd_id = b.pd_id and a.od_code = '$_GET[code]' ORDER BY a.od_id ASC
    ";
    $query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

    $rsmem = mysqli_query($c, $query_limit_rsmem);
    $row_rsmem = mysqli_fetch_assoc($rsmem);

    if (isset($_GET['totalRows_rsmem'])) {
      $totalRows_rsmem = $_GET['totalRows_rsmem'];
    } else {
      $all_rsmem = mysqli_query($c, $query_rsmem);
      $totalRows_rsmem = mysqli_num_rows($all_rsmem);
    }
    $totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

    $queryString_rsmem = "";
    if (!empty($_SERVER['QUERY_STRING'])) {
      $params = explode("&", $_SERVER['QUERY_STRING']);
      $newParams = array();
      foreach ($params as $param) {
        if (
          stristr($param, "pageNum_rsmem") == false &&
          stristr($param, "totalRows_rsmem") == false
        ) {
          array_push($newParams, $param);
        }
      }
      if (count($newParams) != 0) {
        $queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
      }
    }
    $queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

    ?>

    <table id="item_table" align="left" cellpadding="0" cellspacing="0" border="0">
      <?php
      $total_total = 0;
      $num = 0;
      //เริ่มวน

      if ($totalRows_rsmem > 0) { // Show if recordset not empty 
      ?>
        <?php do {

        ?>

          <tr>
            <td width="35px" align="center">
              <div><span style="font-size: 14px"><?php echo $num + 1; ?></span></div>
            </td>
            <td width="399px" height="30px" style="padding-left:15px;">
              <div align="left"><span style="font-size: 14px;"><?php echo "$row_rsmem[pd_name]"; ?></span></div>
            </td>
            <td width="100px" align="center" style="padding-left: 10px">
              <div><span style="font-size: 14px"><?php echo "$row_rsmem[od_num]"; ?></span></div>
            </td>
            <!-- <td width="10%" align="center">
          <div><span style="font-size: 14px"><?php echo "$row_rsmem[pd_unit]"; ?></span></div>
        </td> -->
            <td width="60px" align="right">
              <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_priceperunit"], 2); ?></span></div>
            </td>
            <td width="133px" align="right">
              <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_total"], 2); ?></span></div>
            </td>
          </tr>
      <?php
          $total_total = $total_total + $row_rsmem["od_total"];
          $num++;
        } while ($row_rsmem = mysqli_fetch_assoc($rsmem));
      }

      $totalVat = ($total_total * 7) / 107;
      $totalBeforeVat = ($total_total * 100) / 107;
      $grandTotal = $totalBeforeVat + $totalVat;
      ?>
    </table>
  </div>


  <!-- Summary -->
  <div style="padding-top:750;padding-left:625; font-size:16px; width:100px; text-align:right;" class="fixed" align="right">
    <?php echo number_format($totalBeforeVat, 2); ?>
  </div>
  <div style="padding-top:790;padding-left:625; font-size:16px; width:100px; text-align:right;" class="fixed" align="right">
    <?php echo number_format($totalVat, 2); ?>
  </div>


  <div style="padding-top:825;padding-left:130; font-size:16px; text-align:right;" class="fixed" align="right">
    <?php echo bahttext(number_format($grandTotal, 2)); ?>
  </div>
  <div style="padding-top:825;padding-left:625; font-size:16px; width:100px; text-align:right;" class="fixed" align="right">
    <?php echo number_format($grandTotal, 2); ?>
  </div>

</body>

</html>

<?php

$html = ob_get_contents();
$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
ob_end_clean();

$mpdf = new \Mpdf\Mpdf([
  'mode' => '',
  'format' => [228.6, 279.4],
  'margin_left' => 9,
  'margin_right' => 9,
  'margin_top' => 5,
  'margin_bottom' => 9,
  'margin_header' => 0,
  'margin_footer' => 0,
  'orientation' => 'P',
]);
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont   = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
