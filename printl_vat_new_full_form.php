<?php
@session_start();

require_once __DIR__ . '/mpdf8/vendor/autoload.php';
//require_once('mpdf/mpdf.php'); //ที่อยู่ของไฟล์ mpdf.php ในเครื่องเรานะครับ
ob_start(); // ทำการเก็บค่า html นะครับ

include("function/FunctionDate.php");
include("function/connect.php");
include("function/helper.php");


$query = "
      SELECT a.od_date, a.invoice_no, b.cus_name, b.cus_add, b.cus_tel, a.od_id, b.cus_id, b.cus_taxid
      FROM order_p a, customer b
      WHERE a.cus_id = b.cus_id and a.od_code = '" . $_GET["code"] . "'
      GROUP BY a.od_date, b.cus_name, b.cus_add, b.cus_tel, a.invoice_no
      ORDER BY a.od_id ASC
	";
$result = mysqli_query($c, $query);
$row = mysqli_fetch_array($result);

$orderDate = date_create($row["od_date"]);
$orderId = $row["od_id"];
$invoiceNo = $row["invoice_no"];
$invoiceNo = $invoiceNo == "" ? "I" . date_format($orderDate, "Ym") . $orderId : sprintf('%05d', $invoiceNo);
$curDate = date("d/m/Y");

$themeColorCode = "#000000";
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title></title>
  <style>
    #item_table {
      border-collapse: collapse;
      border-spacing: 1px !important;
    }

    #item_table th {
      border: 2px solid #555555;
      border-top: 0px solid #000000;
      padding: 3px;
      color: #ffffff;
      background-color: #000000;
    }

    #item_table td {
      border-left: solid 2px #000000;
      border-right: solid 2px #000000;
      padding: 1px !important;
      height: 25px;
    }

    #footer_table {
      border-collapse: separate;
      border-spacing: 0px 0px;
      font-size: 10px;
      width: 100%;
    }

    #footer_table td {
      padding: 5px;
      border-left: solid 2px #000000;
      border-bottom: solid 2px #000000;
    }
    
    .checkbox {
      width: 50px;
      height: 50px;
      border: solid 2px #000000;
    }
  </style>
</head>

<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="110px" valign="top">
        <img src="images/company_logo.jpg" width="100px" />
      </td>
      <td valign="top">
        <span style="font-size: 20px; font-weight:700;">บริษัท ทวีสิน เบเกอรี่ จำกัด (สำนักงานใหญ่)</span><br />
        <span style="font-size: 20px; font-weight:700;">TAWESIN BAKERY CO.,LTD. (Head Office)</span><br />
        62 หมู่ 5 ต.ดอนไก่ดี อ.กระทุ่มแบน จ.สมุทรสาคร 74110<br />
        62 Moo.5, Don Kai Di, Krathum Baen, Samut Sakhon 74110<br />
        โทร. 034-878732 034-878552 086-8133039<br />
        เลขประจำตัวผู้เสียภาษี 0745561007339
      </td>
      <td valign="top">
        <table align="right" width="170">
          <tr>
            <td>
              <table align="right" border="0" cellspacing="0" cellpadding="10">
                <tr style=" border:<?php echo $themeColorCode; ?> solid 1px ; padding:25px;font-size: 13px;background: <?php echo $themeColorCode; ?>; ">
                  <td align="center">
                    <b style="color: #FFFFFF;">
                      สำหรับบริษัท<br />
                      COMPANY
                    </b>
                  </td>
                <tr>
                  <td>
                    เอกสารออกเป็นชุด<br />
                    (ไม่ใช่ใบกำกับภาษี)
                  </td>
                </tr>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="center">

      </td>
    </tr>

  </table>
  </td>
  </tr>
  </table>

  <table align="center">
    <tr>
      <td align="center">
        <h3>สำเนาใบเสร็จรับเงิน<br />COPY RECEIPT</h3>
      </td>
    </tr>
  </table>
  <table style="border-collapse: separate;border-spacing: 0px 0px; font-size: 13px; width:100%; height:300px; margin-top:8px;" cellpadding="0" cellpadding="0" border="1">
    <tr>
      <td width="62%">
        <table border="0">
          <tr>
            <td width="185px">
              <b>นามลูกค้า<br />Customer name</b>
            </td>
            <td style="padding-left:15px;">
              <?php echo $row["cus_name"]; ?><br />
            </td>
          </tr>
          <tr>
            <td>
              <b>ที่อยู่<br />Address</b>
            </td>
            <td style="padding-left:15px;">
              <?php echo $row["cus_add"]; ?>
            </td>
          </tr>
          <tr height="70px">
            <td>&ensp;</td>
            <td></td>
          </tr>
          <tr>
            <td>
              <b>เลขประจำตัวผู้เสียภาษีอากร</b>
            </td>
            <td style="padding-left:15px;">
              <?php echo $row["cus_taxid"]; ?><br />
            </td>
          </tr>
        </table>

      </td>
      <td>
        <table height="100%" style="width: 100%; border-collapse: separate;border-spacing: 0px 0px; ">
          <tr>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; border-left:solid 1px #000000;  height: 43px; " width="130px">เลขที่<br />No.</td>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; height: 44px; ">
              <?php echo $invoiceNo; ?>
            </td>
          </tr>
          <tr>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; border-left:solid 1px #000000;  height: 43px; ">วันที่<br />Date</td>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; height: 44px;">
              <?php echo $curDate; ?>
            </td>
          </tr>
          <tr>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; border-left:solid 1px #000000;  height: 43px; ">เลขที่ใบสั่งซื้อ<br />PO No.</td>
            <td style="border-bottom:solid 1px #000000; border-top:solid 1px #000000; height: 44px;">
              AWCC <?php echo $orderId; ?>
            </td>
          </tr>"
        </table>
      </td>
    </tr>
  </table>
  <?php
  include("function/connect.php");

  $currentPage = $_SERVER["PHP_SELF"];

  $maxRows_rsmem = 30;
  $pageNum_rsmem = 0;
  if (isset($_GET['pageNum_rsmem'])) {
    $pageNum_rsmem = $_GET['pageNum_rsmem'];
  }
  $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


  if (isset($_GET['pageNum_rsmem'])) {
    $pageNum_rsmem = $_GET['pageNum_rsmem'];
  }
  $startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
  //คิวรี่ปกติ
  $query_rsmem = "
			SELECT *
			FROM order_p a, product b
			WHERE a.pd_id = b.pd_id and a.od_code = '$_GET[code]' ORDER BY a.od_id ASC
";
  $query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

  $rsmem = mysqli_query($c, $query_limit_rsmem);
  $row_rsmem = mysqli_fetch_assoc($rsmem);

  if (isset($_GET['totalRows_rsmem'])) {
    $totalRows_rsmem = $_GET['totalRows_rsmem'];
  } else {
    $all_rsmem = mysqli_query($c, $query_rsmem);
    $totalRows_rsmem = mysqli_num_rows($all_rsmem);
  }
  $totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

  $queryString_rsmem = "";
  if (!empty($_SERVER['QUERY_STRING'])) {
    $params = explode("&", $_SERVER['QUERY_STRING']);
    $newParams = array();
    foreach ($params as $param) {
      if (
        stristr($param, "pageNum_rsmem") == false &&
        stristr($param, "totalRows_rsmem") == false
      ) {
        array_push($newParams, $param);
      }
    }
    if (count($newParams) != 0) {
      $queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
    }
  }
  $queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

  ?>

  <table id="item_table" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <th align="center" style="font-size: 13px;">ลำดับที่<br />No.</th>
      <th align="center" style="font-size: 13px;">รายการ<br />Description</th>
      <th align="center" style="font-size: 13px;">จำนวน<br />Quantity</th>
      <!-- <th align="center" style="font-size: 14px;">หน่วย<br />Unit</th> -->
      <th align="center" style="font-size: 13px;">ราคาต่อหน่วย<br />Unit Price</th>
      <th align="center" style="font-size: 13px;">จำนวนเงินรวมภาษีมูลค่าเพิ่ม<br />Amount Including Vat</th>
    </tr>

    <?php
    $total_total = 0;
    $num = 0;
    //เริ่มวน

    if ($totalRows_rsmem > 0) { // Show if recordset not empty 
    ?>
      <?php do {

      ?>

        <tr>
          <td width="60px" align="center">
            <div><span style="font-size: 14px"><?php echo $num + 1; ?></span></div>
          </td>
          <td width="613px" height="30px">
            <div align="left"><span style="font-size: 14px"><?php echo "$row_rsmem[pd_name]"; ?></span></div>
          </td>
          <td width="100px" align="center">
            <div><span style="font-size: 14px"><?php echo "$row_rsmem[od_num]"; ?></span></div>
          </td>
          <!-- <td width="10%" align="center">
            <div><span style="font-size: 14px"><?php echo "$row_rsmem[pd_unit]"; ?></span></div>
          </td> -->
          <td width="120px" align="right">
            <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_priceperunit"], 2); ?></span></div>
          </td>
          <td width="193px" align="right">
            <div><span style="font-size: 14px"><?php echo number_format($row_rsmem["od_total"], 2); ?></span></div>
          </td>
        </tr>
      <?php
        $total_total = $total_total + $row_rsmem["od_total"];
        $num++;
      } while ($row_rsmem = mysqli_fetch_assoc($rsmem));
    }

    $totalRowInPage = 25;
    if ($num < $totalRowInPage) {
      $generateRow = $totalRowInPage - $num;

      for ($i = 0; $i < $generateRow; $i++) {
      ?>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
    <?php
      }
    }


    $totalVat = $total_total * 0.07;
    $grandTotal = $totalVat + $total_total;
    ?>
    <tr>
      <td colspan="2" rowspan="2" align="left" style="padding:5px; height:40px; font-size: 14px; border-top:solid 2px #000000; border-bottom:solid 2px #000000;" valign="middle">
        <table>
          <tr>
            <td valign="top" style="border:none;">
              <h3> หมายเหตุ: <br /> Remark</h3>
            </td>
            <td valign="top" style="border:none; font-size:16px;">
              1. ได้รับสินค้าตามข้างต้นถูกต้องเรียบร้อย<br />
              2. ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อมีลายมือชื่อของผู้รับเงิน<br />
              3. กรณีชำระเงินด้วยเช็คจะสมบูรณ์ต่อเมื่อเช็คเรียกเก็บเงินได้เรียบร้อยแล้ว
            </td>
          </tr>
        </table>
      </td>
      <th colspan="2" align="left" style="font-size: 13px; border-top:solid 2px #000000; background-color:#CCCCCC; color:#000000;" valign="middle">
        ราคาสินค้าไม่รวมภาษีมูลค่าเพิ่ม
        <br />Amount Excluding VAT
      </th>
      <th style="font-size: 14px; font-weight:0; border-top:solid 2px #000000; background-color:#FFFFFF; color:#000000;" align="right" valign="middle">
        <?php echo number_format($total_total, 2); ?><br /><br />
      </th>
    </tr>
    <tr>
      <th colspan="2" align="left" style=" height:40px;font-size: 13px; border-top:solid 2px #000000; background-color:#CCCCCC; color:#000000;" valign="middle">
        จำนวนภาษีมูลค่าเพิ่ม VAT 7%
      </th>
      <th style="font-size: 14px; font-weight:0; border-top:solid 2px #000000; background-color:#FFFFFF; color:#000000;" align="right" valign="middle">
        <?php echo number_format($totalVat, 2); ?><br /><br />
      </th>
    </tr>
    <tr>
      <td colspan="2" align="center" style="font-size: 16px; font-weight:bold; border-top:solid 2px #000000; border-bottom:solid 2px #000000; background-color:#CCCCCC; color:#000000;" valign="middle">
        <?php echo bahttext(number_format($grandTotal, 2)); ?>
      </td>
      <th colspan="2" align="left" style=" height:40px;font-size: 13px; border-top:solid 2px #000000;" valign="middle">
        รวมราคาสินค้าที่เสียภาษีมูลค่าเพิ่ม
        <br />Total Amount Including VAT
      </th>
      <th style="font-size: 14px; font-weight:0; border-top:solid 2px #000000;" align="right" valign="middle">
        <?php echo number_format($grandTotal, 2); ?><br /><br />
      </th>
    </tr>
  </table>

  <table id="footer_table">
    <tr>
      <td align="center">
        <b>ได้รับสินค้าตามรายการข้างบนนี้ไว้ถูกต้อง<br />และอยู่ในสภาพเรียบร้อยทุกประการ<br /></b><br /><br />
        <table>
          <tr>
            <td style="border:none">............................................</td>
            <td style="border:none">........../.........../.............</td>
          </tr>
          <tr>
            <td style="border:none">ผู้รับสินค้า / Receiver By</td>
            <td style="border:none">วันที่ / Date</td>
          </tr>
          <tr>
            <td style="border:none">&ensp;</td>
          </tr>
          <tr>
            <td style="border:none">............................................</td>
            <td style="border:none">........../.........../.............</td>
          </tr>
          <tr>
            <td style="border:none">ผู้ส่งสินค้า / Deliverer By</td>
            <td style="border:none">วันที่ / Date</td>
          </tr>
        </table>
      </td>
      <td align="center">
        <table cellpadding="0" cellspacing="0" bolder="1">
          <tr>
            <td style="border:none;font-size:  10px; font-weight:bold;" align="left">ชำระโดย<br />Payment By</td>
            <td style="border:none" align="right">
              <div class="checkbox">&ensp;&ensp;&ensp;</div>
            </td>
            <td style="border:none;font-size:10px;" align="left">
              <span>เงินสด<br />Cash</span>
            </td>
            <td style="border:none" align="right">
              <div class="checkbox">&ensp;&ensp;&ensp;</div>
            </td>
            <td style="border:none;font-size:10px;" align="left">
              <span>เช็ค<br />Cheque</span>
            </td>
            <td style="border:none" align="right">
              <div class="checkbox">&ensp;&ensp;&ensp;</div>
            </td>
            <td style="border:none;font-size:10px;" align="left">
              <span>โอน<br />Transfer</span>
            </td> 
          </tr>
          <tr>
            <td colspan="3" style="border:none;" align="left"><b>ธนาคาร<br/>Bank</b> ..................................</td>
            <td colspan="4" style="border:none;" align="left"><b>สาขา<br/>Branch</b> ................................</td>
          </tr>
          <tr>
            <td colspan="3" style="border:none;" align="left"><b>เลขที่เช็ค<br/>Cheque No</b> ......................</td>
            <td colspan="4" style="border:none;" align="left"><b>ลงวันที่<br/>Due Date</b> .............................</td>
          </tr>
          <tr>
            <td colspan="6" style="border:none;" align="left"><b>จำนวนเงิน<br/>Amount</b> ...............................................................</td>
            <td style="border:none;" align="left"><b>บาท<br/>Baht</b></td>
          </tr>
          
          <tr>
            <td style="border:none">
              &ensp;&ensp;&ensp;
            </td>
          </tr>
          <tr>
            <td colspan="4" style="border:none">................................................</td>
            <td colspan="3" style="border:none">............/............/...........</td>
          </tr>
          <tr>
            <td colspan="4" style="border:none">ผู้ส่งสินค้า / Deliverer By</td>
            <td colspan="3" style="border:none">วันที่ / Date</td>
          </tr>
        </table>
      </td>
      <td align="center" valign="top" style="border-right: solid 2px #000000;">
        ในนาม <b>บริษัท ทวีสิน เบเกอรี่ จำกัด</b><br />
        For <b>TAWESIN BAKERY CO.,LTD.</b><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
        ............................................................<br /><br />
        ผู้มีอำนาจลงนาม/Authorized Signature<br />
      </td>
    </tr>
  </table>
</body>

</html>

<?php

$html = ob_get_contents();
$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
ob_end_clean();

$mpdf = new \Mpdf\Mpdf([
  'mode' => '',
  'format' => 'A4',
  'margin_left' => 5,
  'margin_right' => 5,
  'margin_top' => 5,
  'margin_bottom' => 5,
  'margin_header' => 6,
  'margin_footer' => 6,
  'orientation' => 'P',
]);
$mpdf->autoScriptToLang = true;
$mpdf->autoLangToFont   = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
