<?php

if (isset($_GET["viewMode"]) && $_GET["viewMode"] == "EXPORT") {
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=export.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
}

@session_start();
ob_start();

include("FunctionDate.php");
include("connect.php");

$queryFields = " SELECT b.cus_name, SUM(od_total) as total_amount ";
$queryTable = " FROM order_p a INNER JOIN customer b ON a.cus_id = b.cus_id ";
$queryConditions = " WHERE 1 ";
$queryGroup = "";

$billHeader = "ใบวางบิล ";
$reportType = $_GET["reportType"];
$selectedCustomer = false;
$reportColspan = 7;

if (isset($_GET["txtCusName"]) && isset($_GET["ddlCustomer"])) {
	if ($_GET["ddlCustomer"] !== "") {
		$billHeader .= $_GET["txtCusName"];
		$queryConditions .= " AND (a.cus_id = '" . $_GET["ddlCustomer"] . "') ";
		$selectedCustomer = true;
	}
}

///รายการสั่งซื้อ
if ($reportType == "ORDER") {
	$queryFields .= " ,a.od_date, a.od_code, a.invoice_no ";
	$reportColspan = $selectedCustomer ? $reportColspan : 8;
}
///สรุปยอดขาย รายลูกค้า
else if ($reportType == "CUSTOMER_SALE") {
	$queryFields .= "";
	$billHeader = "สรุปยอดขาย";
}
///ยอดขายรายวัน
else if ($reportType == "DAILY_SALE") {
	$queryFields .= " ,a.od_date, a.od_code, a.invoice_no ";
	$reportColspan = $selectedCustomer ? $reportColspan : 8;
}



if (isset($_GET["txtStartDate"]) && isset($_GET["txtEndDate"])) {
	$startDate = $_GET["txtStartDate"];
	$endDate = $_GET["txtEndDate"];
	$startDates = explode("/", $startDate);
	$endDates = explode("/", $endDate);

	$startDate = date("Y-m-d", strtotime($startDates[2] . "-" . $startDates[1] . "-" . $startDates[0]));
	$endDate = date("Y-m-d", strtotime($endDates[2] . "-" . $endDates[1] . "-" . $endDates[0]));


	$queryConditions .= " AND (a.od_date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59') ";

	$startDay = (int)$startDates[0];
	$startMonth = (int)$startDates[1];
	$startYear = $startDates[2];
	$endDay = (int)$endDates[0];
	$endMonth = (int)$endDates[1];
	$endYear = $endDates[2];

	if (($startYear . $startMonth) == ($endYear . $endMonth)) {
		$billHeader .= " วันที่ $startDay - $endDay " . $thaiMonths[((int)$startMonth) - 1] . " $startYear";
	} else if ($startYear == $endYear) {
		$billHeader .= " วันที่ $startDay " . $thaiMonths[((int)$startMonth) - 1] . " - $endDay "  . $thaiMonths[((int)$endMonth) - 1] . " $startYear";
	} else {
		$billHeader .= " วันที่ $startDay " . $thaiMonths[((int)$startMonth) - 1] . " $startYear - $endDay "  . $thaiMonths[((int)$endMonth) - 1] . " $endYear";
	}
}

if (isset($_GET["orderType"]) && isset($_GET["orderType"])) {
	$orderType = $_GET["orderType"];

	if($orderType == "INCLUDE_INVOICE"){
		$queryConditions .= " AND (a.invoice_no is not null AND a.invoice_no != '') ";
	} else if($orderType == "EXCLUDE_INVOICE"){
		$queryConditions .= " AND (a.invoice_no is null OR a.invoice_no = '') ";
	}	
}

if ($reportType == "ORDER") {
	$queryGroup = " GROUP BY a.od_code, a.cus_id, a.invoice_no ORDER BY a.od_date ASC ";
} else if ($reportType == "CUSTOMER_SALE") {
	$queryGroup = " GROUP BY a.cus_id ORDER BY a.od_date ASC ";
} else if ($reportType == "DAILY_SALE") {
	$queryGroup = " GROUP BY a.od_date, a.cus_id, a.invoice_no ORDER BY a.od_date ASC ";
}

$query = "$queryFields$queryTable$queryConditions$queryGroup";

//echo $query;
$result = mysqli_query($c, $query);
?>

<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title></title>
</head>

<body>
	<?php if ($reportType == "CUSTOMER_SALE") { ?>
		<table width="1000px" border="1" align="center" cellpadding="0" cellspacing="0">
			<tr height="45px">
				<td colspan="3" align="center"><span style="font-size: 23px; font-weight:bold;">ทวีสินเบเกอรี่</span></td>
			</tr>
			<tr height="45px">
				<td colspan="3" align="center">
					<span style="font-size: 18px; font-weight:bold;">
						<?php echo $billHeader; ?>
					</span>
				</td>
			</tr>
			<tr style="font-weight:bold; font-size:19px;">
				<td align="center">ลำดับ</td>
				<td align="center">ลูกค้า</td>
				<td align="center">ยอดขาย</td>
			</tr>

			<?php
			$totalAmount = 0;
			$num = 0;
			?>
			<?php while ($row_rsmem = mysqli_fetch_assoc($result)) {
				if ($reportType == "ORDER") {
					$row_rsmem["od_date"] = $row_rsmem["od_code"];
				}
			?>

				<tr>
					<td align="center">
						<span style="font-size: 19px"><?php echo $num + 1; ?></span>
					</td>
					<td align="center">
						<span style="font-size: 19px"><?php echo $row_rsmem["cus_name"]; ?></span>
					</td>
					<td align="center">
						<span style="font-size: 19px"><?php echo number_format($row_rsmem["total_amount"], 2); ?></span>
					</td>
				</tr>
			<?php
				$totalAmount += $row_rsmem["total_amount"];
				$num++;
			} ?>

			<tr>
				<td colspan="2" align="right" style="font-weight:bold; font-size:19px;" align="center"><b>รวม</b></td>
				<td align="center" style="font-weight:bold; font-size:19px;" align="center">
					<?php echo number_format($totalAmount, 2); ?></td>
			</tr>

		</table>
	<?php } else { ?>
		<table width="1000px" border="1" align="center" cellpadding="0" cellspacing="0">
			<tr height="45px">
				<td colspan="<?php echo $reportColspan; ?>" align="center"><span style="font-size: 23px; font-weight:bold;">ทวีสินเบเกอรี่</span></td>
			</tr>
			<tr height="45px">
				<td colspan="<?php echo $reportColspan; ?>" align="center">
					<span style="font-size: 18px; font-weight:bold;">
						<?php echo $billHeader; ?>
					</span>
				</td>
			</tr>
			<tr style="font-weight:bold; font-size:19px;">
				<td align="center">ลำดับ</td>
				<td align="center">เลขที่ใบกำกับภาษี</td>
				<td align="center">วันที่</td>
				<?php if (!$selectedCustomer) {
					echo "<td align='center'>ลูกค้า</td>";
				} ?>
				<td align="center">ยอดขาย</td>
				<td align="center">ยอดของคืน</td>
				<td align="center">ยอดเบิกเงิน</td>
				<td align="center">ยอดขายสุทธิ</td>
			</tr>

			<?php
			$totalAmount = 0;
			$num = 0;
			?>
			<?php while ($row_rsmem = mysqli_fetch_assoc($result)) {
				if ($reportType == "ORDER") {
					$row_rsmem["od_date"] = $row_rsmem["od_code"];
				}

				//$invoiceNo = $row_rsmem["invoice_no"] == "" ? "" : sprintf('%05d', $row_rsmem["invoice_no"]);
				$invoiceNo = $row_rsmem["invoice_no"];
			?>

				<tr>
					<td align="center">
						<span style="font-size: 19px"><?php echo $num + 1; ?></span>
					</td>
					<td align="center">
						<span style="font-size: 19px"><?php echo $invoiceNo; ?></span>
					</td>
					<td align="center">
						<span style="font-size: 19px"><?php echo $row_rsmem["od_date"]; ?></span>
					</td>
					
					<?php if (!$selectedCustomer) { ?>
						<td align="center">
							<span style="font-size: 19px"><?php echo $row_rsmem["cus_name"]; ?></span>
						</td>
					<?php } ?>
					<td align="center">
						<span style="font-size: 19px"><?php echo number_format($row_rsmem["total_amount"], 2); ?></span>
					</td>
					<td align="center">
						<span style="font-size: 19px">0.0</span>
					</td>
					<td align="center">
						<span style="font-size: 19px">0.0</span>
					</td>
					<td align="center">
						<span style="font-size: 19px"><?php echo number_format($row_rsmem["total_amount"], 2); ?></span>
					</td>
				</tr>
			<?php
				$totalAmount += $row_rsmem["total_amount"];
				$num++;
			} ?>

			<tr>
				<td colspan="<?php echo $reportColspan - 1; ?>" align="right" style="font-weight:bold; font-size:19px;" align="center"><b>รวม</b></td>
				<td align="center" style="font-weight:bold; font-size:19px;" align="center">
					<?php echo number_format($totalAmount, 2); ?></td>
			</tr>

		</table>
	<?php } ?>
</body>

</html>