<?php
require_once("connect.php");

function getProducts($type = "PO", $cusId = "")
{
    global $c;
    $whereInCondition = "";

    if ($cusId != "") {
        $whereInCondition = " AND product_id in (SELECT product_id FROM po_customer_product WHERE cus_id = '$cusId') ";
    }

    $query = "SELECT * FROM po_product WHERE status = 1 AND product_type='$type'  $whereInCondition ORDER BY created_datetime DESC";
    $result = mysqli_query($c, $query);
    return fetchData($result);
}

function getCustomers($type = "PO", $incProduct = true)
{
    global $c;

    $query = "SELECT * FROM po_customer ORDER BY created_datetime DESC";
    $result = mysqli_query($c, $query);
    $resultArray = array();

    while ($obResult = mysqli_fetch_array($result)) {
        if ($incProduct) {
            $cusProductData =  getCustomerProduct($obResult["cus_id"]);
            $obResult["customer_product"] = $cusProductData;
        }
        array_push($resultArray, $obResult);
    }

    return $resultArray;
}

function getCustomerProduct($cusId)
{
    global $c;

    $query = "SELECT * FROM po_customer_product cp
                    INNER JOIN po_customer cus ON cp.cus_id = cus.cus_id 
                    INNER JOIN po_product pd ON cp.product_id = pd.product_id
                    WHERE cp.cus_id = '$cusId'";
    $result = mysqli_query($c, $query);
    return fetchData($result);
}

function getOrders($docType = "PURCHASE", $startDate = "", $endDate = "", $cusId = "")
{
    global $c;

    $whereCondition = "";

    if ($startDate != "") {
        if ($endDate == "") {
            $endDate = $startDate;
        }

        $startDates = explode("/", $startDate);
        $endDates = explode("/", $endDate);

        $startDate = date("Y-m-d", strtotime($startDates[2] . "-" . $startDates[1] . "-" . $startDates[0]));
        $endDate = date("Y-m-d", strtotime($endDates[2] . "-" . $endDates[1] . "-" . $endDates[0]));

        $whereCondition .= " AND (created_datetime BETWEEN '$startDate 00:00:00'  AND '$endDate 23:59:59') ";
    }

    if ($cusId != "") {
        $whereCondition .= " AND (cus_id = '$cusId') ";
    }

    $query = "SELECT * FROM po_order WHERE document_type = '$docType' $whereCondition ORDER BY created_datetime DESC";
    $result = mysqli_query($c, $query);
    $resultArray = array();

    while ($obResult = mysqli_fetch_array($result)) {
        $detailQuery = "SELECT * FROM po_order_detail WHERE order_id = '" . $obResult["order_id"] . "'";
        $detailResult = mysqli_query($c, $detailQuery);

        $detailRows = array();
        $totalQty = 0;
        $totalAmount = 0;

        while ($obDetailResult = mysqli_fetch_array($detailResult)) {
            $qty = $obDetailResult["qty"];
            $price = $obDetailResult["price"];
            $totalAmount += ($qty * $price);
            $totalQty += $qty;
            array_push($detailRows, $obDetailResult);
        }

        $obResult["total_qty"] = $totalQty;
        $obResult["total_amount"] = $totalAmount;
        $obResult["order_detail"] = $detailRows;
        array_push($resultArray, $obResult);
    }

    return $resultArray;
}

function fetchData($result)
{
    $resultArray = array();

    while ($obResult = mysqli_fetch_array($result)) {
        array_push($resultArray, $obResult);
    }

    return $resultArray;
}
