<?php
/*********************************
* The PHP Analog Clock           *
* http://analogclock.caiphp.com/ *
* cai@caiphp.com                 *
**********************************
*   Description                  *
*******************************************************************************
*     The PHP Analog Clock is the most versatile PHP clock available on the   *
* internet.  You can change the way the entire clock looks, from its face to  *
* its hands.                                                                  *
*******************************************************************************
*   Requirements                 *                                            *
*******************************************************************************
* - GD version 2.0 or higher (PHP-bundled version recommended)                *
* - PHP 4.3 or higher                                                         *
*******************************************************************************
*   History                      *                                            *
*******************************************************************************
* v1.1  - 02-Oct-05 - Cleaned up how the clock works (it's much better now!)  *
*                   - New types of hands (lines, triangles, diamonds)         *
*                   - Basic bullets                                           *
*                   - Correctly calculates clock center on non-square clocks  *
*                   - Anti-aliasing is now optional                           *
* v1.0  - 01-Aug-04 - Complete redo                                           *
*                   - Object-oriented application                             *
*                   - Hand thickness                                          *
*                   - Label support                                           *
*                   - Much more...                                            *
**********************************                                            *
*   Older versions               *                                            *
**********************************                                            *
* v0.3  - 22-Oct-03 - Timezone support added (+ or - GMT)                     *
*                   - Line thickness (looks horrible, however)                *
* v0.2  - 21-Oct-03 - Made hands graphical                                    *
* v0.1  - 20-Oct-03 - First released version                                  *
*******************************************************************************
*   Bug report                   *                                            *
*******************************************************************************
*   I have done my best to keep this software bug free, but a few may have    *
* made it into the release.  If you happen to find one, please describe, in   *
* detail, what you did to get the bug, how repeatable it is, and provide the  *
* errors, if any are given, and email them to cai@caiphp.com.  I will attempt *
* to fix them, and give credit you in this header.  Happy bug hunting!        *
*******************************************************************************
*   Donations                    *                                            *
*******************************************************************************
*   I am providing this software free of charge, for any use, as long as you  *
* give credit where credit is due.  If you like this software, and are kind   *
* enough to support it, please donate whatever you can afford.  There is a    *
* PayPal donate button on the links section of http://www.caiphp.com/, or you *
* can donate straight to cai@caiphp.com through PayPal.  Thanks!              *
******************************************************************************/
class AnalogClock
{
  // These are for the clock system
  var $im, $background, $timezone;
  // This is called whenever a new clock is made
  function AnalogClock($width, $height=0, $fill='FFFFFF')
  {
    $this->clockwidth = $width; // Clock width
    $this->clockheight = ($height == 0)?$width:$height; // Clock height
    $this->timezone = substr(date('O'), 0, 3); // Timezone
    $this->im = imagecreatetruecolor($this->clockwidth, $this->clockheight);
    imagefill($this->im, 0, 0, $this->AllocateColor($fill));
  }
/******************************************************************************
* Alias (on)                                                                  *
*******************************************************************************
*     Add anti-aliasing to the clock.  Only the PHP-bundled version of gd has *
* anti-aliasing support, so this becomes optional.  There is only one         *
* parameter, which defaults to TRUE.                                          *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - This is a new function                                              *
******************************************************************************/
  function Alias ($on=TRUE)
  {
    imageantialias($this->im, $on); // Antialiasing
  }
/******************************************************************************
* Background (file, [stretch])                                                *
*******************************************************************************
*     Draws a background onto the clock.  There are two parameters: the image *
* to draw, and whether or not to stretch that image across the clock image.   *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - No longer requires DrawFace(), as it draws immediately              *
******************************************************************************/
  function Background($file, $stretch=FALSE)
  {
    $this->background = imagecreatefromstring(file_get_contents($file));
    if ($stretch == FALSE)
    {
      imagecopy($this->im, $this->background, 0, 0, 0, 0, imagesx($this->background), imagesy($this->background));
    }
    else
    {
      imagecopyresampled($this->im, $this->background, 0, 0, 0, 0, $this->clockwidth, $this->clockheight, imagesx($this->background), imagesy($this->background));
    }
  }
/******************************************************************************
* NewHand (type, style, color, length, width, [filled])                       *
*******************************************************************************
*     Create a new hand to be drawn.  The type is which hand (hour, min, or   *
* sec).  Color is the color of the hand.  Length is the pixel length, from    *
* the center (certain hand styles will draw further back).  Width is the      *
* pixel width of the current hand.  Make filled TRUE if you want the hand to  *
* be filled with the color.  The style can be one of several:                 *
* - line - A line drawn from behind the center point by 1/3 of the length.    *
* - triangle - Three points out from the center.                              *
* - diamond - Four points from the center.                                    *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - Completely redone                                                   *
*         No longer waits for DrawHands()                                     *
*         Made it a little easier to read through                             *
******************************************************************************/
  function NewHand($type, $style, $color, $length, $width, $filled=FALSE)
  {
    switch ($type)
    {
      case 'hour':
        $point = (gmdate('g') + $this->timezone);
        if (date('I') == 1)
        {
          $point += 1;
        }
        $point = ($point + date('i') / 60) * 2 * M_PI / 12;
        break;
      case 'min':
        $point = (date('i') + date('s') / 60) * 2 * M_PI / 60;
        break;
      case 'sec':
        $point = date('s') * 2 * M_PI / 60;
        break;
    }
    switch ($style)
    {
      case 'line':
        $startx = $this->AngleToPixelX(-($length / 3), $point);
        $starty = $this->AngleToPixelY(-($length / 3), $point);
        $endx = $this->AngleToPixelX($length, $point);
        $endy = $this->AngleToPixelY($length, $point);
        if ($width == 1)
        {
          imageline($this->im, $startx, $starty, $endx, $endy, $this->AllocateColor($color));
          return;
        }
        // This segment of code was borrowed from the PHP manual
        // (I felt no need to rewrite it, as it does an excellent job.)
        if ($startx != $endx)
          $k = ($starty - $endy) / ($startx - $endx);
        else
          $k = $starty - $endy;
        $a = (($width / 2) - 0.5) / sqrt(1 + pow($k, 2));
        $points = array(
          round($startx - (1 + $k) * $a), round($starty + (1 - $k) * $a),
          round($startx - (1 - $k) * $a), round($starty - (1 + $k) * $a),
          round($endx + (1 + $k) * $a), round($endy - (1 - $k) * $a),
          round($endx + (1 - $k) * $a), round($endy + (1 + $k) * $a),
        );
        // End segment
        $numpoints = 4;
        break;
      case 'triangle':
        $x1 = $this->AngleToPixelX($width / 2, $point - (M_PI / 2));
        $y1 = $this->AngleToPixelY($width / 2, $point - (M_PI / 2));
        $x2 = $this->AngleToPixelX($width / 2, $point + (M_PI / 2));
        $y2 = $this->AngleToPixelY($width / 2, $point + (M_PI / 2));
        $x3 = $this->AngleToPixelX($length, $point);
        $y3 = $this->AngleToPixelY($length, $point);
        $points = array(
          $x1, $y1,
          $x2, $y2,
          $x3, $y3
        );
        $numpoints = 3;
        break;
      case 'diamond':
        $x1 = $this->AngleToPixelX(-$width, $point);
        $y1 = $this->AngleToPixelY(-$width, $point);
        $x2 = $this->AngleToPixelX($width / 2, $point - (M_PI / 2));
        $y2 = $this->AngleToPixelY($width / 2, $point - (M_PI / 2));
        $x3 = $this->AngleToPixelX($length, $point);
        $y3 = $this->AngleToPixelY($length, $point);
        $x4 = $this->AngleToPixelX($width / 2, $point + (M_PI / 2));
        $y4 = $this->AngleToPixelY($width / 2, $point + (M_PI / 2));
        $points = array(
          $x1, $y1,
          $x2, $y2,
          $x3, $y3,
          $x4, $y4
        );
        $numpoints = 4;
        break;
    }
    if ($filled == TRUE)
    {
      imagefilledpolygon($this->im, $points, $numpoints, $this->AllocateColor($color));
    }
    imagepolygon($this->im, $points, $numpoints, $this->AllocateColor($color));
  }
/******************************************************************************
* DrawFace (fill, border)                                                     *
*******************************************************************************
*     Draws a basic clock face.  Takes the face and border color.  Only use   *
* this if you don't want to use a background image.                           *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - No longer draws the background image                                *
*         No longer required to be called                                     *
******************************************************************************/
  function DrawFace($fill, $border)
  {
    imagefilledarc($this->im, $this->clockwidth/2, $this->clockheight/2, $this->clockwidth-1, $this->clockheight-1, 0, 360, $this->AllocateColor($fill), 4);
    imagearc($this->im, $this->clockwidth/2, $this->clockheight/2, $this->clockwidth-1, $this->clockheight-1, 0, 360, $this->AllocateColor($border));
  }
/******************************************************************************
* DrawBullets (color, distance, [width, [height]])                            *
*******************************************************************************
*     Draws bullets for each hour.  Rather limited, so it's best to use an    *
* image for the background with the bullets already drawn on.                 *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - This is a new function                                              *
******************************************************************************/
  function DrawBullets($color, $distance, $width=4, $height=4)
  {
    for ($i = 0; $i < 12; $i++)
    {
      $num = $i * 2 * M_PI / 12;
      $centerx = $this->AngleToPixelX($distance, $num);
      $centery = $this->AngleToPixelY($distance, $num);
      imagefilledellipse($this->im, $centerx, $centery, $width, $height, $this->AllocateColor($color));
    }
  }
/******************************************************************************
* DrawOverlay (file, [x, [y]])                                                *
*******************************************************************************
*     Draw an overlay onto the clock.  This preserves alpha transparency, so  *
* using a PNG file will look the best.  Additionally, you can specify the X   *
* and Y coordinates.                                                          *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* None.                                                                       *
******************************************************************************/
  function DrawOverlay($file, $x=0, $y=0)
  {
    $overlay = imagecreatefromstring(file_get_contents($file));
    imagecopy($this->im, $overlay, $x, $y, 0, 0, imagesx($overlay), imagesy($overlay));
    imagedestroy($overlay);
  }
/******************************************************************************
* AngleToPixelX (distance, angle)                                             *
*******************************************************************************
*     Calculate a pixel point from the center of the clock given a distance   *
* and angle.  Returns the X part of the point.                                *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - Calculates from the correct center                                  *
******************************************************************************/
  function AngleToPixelX($distance, $angle)
  {
    return intval(($this->clockwidth / 2) + ($distance * sin($angle)));
  }
/******************************************************************************
* AngleToPixelY (distance, angle)                                             *
*******************************************************************************
*     Calculate a pixel point from the center of the clock given a distance   *
* and an angle.  Returns the Y part of the point.                             *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - Calculates from the correct center                                  *
******************************************************************************/
  function AngleToPixelY($distance, $angle)
  {
    return intval(($this->clockheight / 2) - ($distance * cos($angle)));
  }
/******************************************************************************
* DrawLabel (font, X, Y, size, angle, string, color)                          *
*******************************************************************************
*     Draw a TrueType Font label on the clock image.  You must specify the    *
* font file, the X and Y coordinates, font point size, angle, and the text to *
* write on the label.                                                         *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
* v1.1  - Added color as a new parameter.                                     *
******************************************************************************/
  function DrawLabel($font, $x, $y, $size, $angle, $string, $color)
  {
    imagettftext($this->im, $size, $angle, $x, $y, $this->AllocateColor($color), $font, $string);
  }
/******************************************************************************
* DrawPNG ()                                                                  *
*******************************************************************************
*     Output a PNG image of the clock.                                        *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
*     None.                                                                   *
******************************************************************************/
  function DrawPNG()
  {
    header('Content-type: image/png');
    imagepng($this->im);
    imagedestroy($this->im);
  }
/******************************************************************************
* DrawJPEG ([quality])                                                        *
*******************************************************************************
*     Output a JPEG image of the clock.  Quality of the JPEG, which defaults  *
* at 75%, will make the image look better or worse.                           *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
*     None.                                                                   *
******************************************************************************/
  function DrawJPEG($quality=75)
  {
    header('Content-type: image/jpeg');
    ob_start();
    imagejpeg($this->im, '', $quality);
    $imagedata = ob_get_contents();
    ob_end_clean();
    echo str_replace('CREATOR:','CREATOR: Analog Clock using',$imagedata);
    imagedestroy($this->im);
  }
/******************************************************************************
* DrawGIF ()                                                                  *
*******************************************************************************
*     Output a GIF image of the clock.  Note that if you do not have version  *
* 2.0.28 or higher of gd, you will probably not have GIF support.             *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
*     None.                                                                   *
******************************************************************************/
  function DrawGIF()
  {
    header('Content-type: image/gif');
    imagegif($this->im);
    imagedestroy($this->im);
  }
/******************************************************************************
* FetchImage ()                                                               *
*******************************************************************************
*     Use this function if you plan on using the clock image as a gd image    *
* resource.  You can use any of gd's functions on this resource.              *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
*     None.                                                                   *
******************************************************************************/
  function FetchImage()
  {
    return $this->im;
  }
/******************************************************************************
* AllocateColor (color)                                                       *
*******************************************************************************
*     Color is a hex representation of an RGB color.  Since the clock uses    *
* a truecolor image, any hex color will work.                                 *
**********************************                                            *
*   Revision History             *                                            *
**********************************                                            *
*     None.                                                                   *
******************************************************************************/
  function AllocateColor($col)
  {
    return imagecolorclosest($this->im, hexdec(substr($col,0,2)), hexdec(substr($col,2,2)), hexdec(substr($col,4,2)));
  }
}
?>
