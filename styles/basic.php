<?php
// A PHP Analog Clock skin
// http://analogclock.caiphp.com/

require_once 'clock.php'; // Include the base analog clock script

$clock = new AnalogClock(128); // Create a 128x128 pixel clock
$clock->timezone = -5; // GMT -0600

/* These affect the way the clock looks */
$clock->Alias(TRUE); // Turn on anti-aliasing
$clock->DrawFace('FFFFCC', '000000'); // Draw the face
$clock->DrawBullets('000000', 50); // 4x4 pixel bullets

/* The clock's hands */
$clock->NewHand('hour', 'diamond', '000000', 30, 6, TRUE);
$clock->NewHand('min', 'line', '000000', 45, 3, TRUE);

$clock->DrawPNG(); // Output as a PNG
?>
