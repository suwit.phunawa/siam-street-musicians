<?php
// A PHP Analog Clock skin
// http://analogclock.caiphp.com/

require_once 'clock.php'; // Include the base analog clock script

$clock = new AnalogClock(128); // Create a 128x128 pixel clock
$clock->timezone = -5; // GMT -0600

/* These affect the way the clock looks */
$clock->Alias(TRUE); // Turn on anti-aliasing
$clock->Background('images/blue.png'); // Add a background image

/* The clock's hands */
$clock->NewHand('min', 'line', '798eab', 45, 3, TRUE);
$clock->NewHand('hour', 'diamond', '96b1d4', 30, 6, TRUE);

$clock->DrawPNG(); // Output as a PNG
?>
