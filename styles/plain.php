<?php
// A PHP Analog Clock skin
// http://analogclock.caiphp.com/

require_once 'clock.php'; // Include the base analog clock script

$clock = new AnalogClock(128); // Create a 128x128 pixel clock
$clock->timezone = -5; // GMT -0600


/* These affect the way the clock looks */
$clock->Alias(TRUE); // Turn on anti-aliasing
$clock->Background('images/plain.png'); // Add a background image

/* The clock's hands */
$clock->NewHand('hour', 'line', '000000', 20, 3, TRUE);
$clock->NewHand('min', 'line', '000000', 35, 3, TRUE);

$clock->DrawOverlay('images/plain-overlay.png');
$clock->DrawPNG(); // Output as a PNG
?>