<?php
@session_start();

include("../function/connect.php");


$docType = $_POST["txt_doc_type"];
$customerId = isset($_POST["customer_id"]) ? $_POST["customer_id"] : "";
$customerName = isset($_POST["customer_name"]) ? $_POST["customer_name"] : "";

$poDate = isset($_POST["po_date"]) ? $_POST["po_date"] : "";

$productIds = $_POST["txt_product_id"];
$productNames = $_POST["txt_product_name"];
$unitNames = $_POST["txt_unit_name"];
$qtys = $_POST["txt_qty"];

if ($docType == "PURCHASE") {
	$prices = $_POST["txt_price"];
}

if($poDate != ""){
	$poDates = explode("/", $poDate);

	$curTime = date("H:i:s");
	$poDate = date("Y-m-d", strtotime($poDates[2] . "-" . $poDates[1] . "-" . $poDates[0]));
	$poDate .= " $curTime";
} else {
	$poDate = date("Y-m-d H:i:s");
}

//Create header order
$query = "insert into po_order (document_type, cus_id, cus_name, created_datetime) ";
$query .= " values ('$docType', '$customerId', '$customerName', '$poDate') ";
$result = mysqli_query($c, $query);
$createdId = 0;

if ($result) {
	$createdId = mysqli_insert_id($c);
} else {
	exit();
}

for ($i = 0; $i < count($productNames); $i++) {
	$productId = $productIds[$i];
	$productName = $productNames[$i];
	$unitName = $unitNames[$i];
	$qty = $qtys[$i];

	$price = 0;

	if ($docType == "PURCHASE") {
		$price = $prices[$i];
	}

	$query = "insert into po_order_detail (order_id, product_id, product_name, unit_name, price, qty) ";
	$query .= " VALUES ('$createdId', '$productId', '$productName', '$unitName', $price, $qty)";

	$result = mysqli_query($c, $query);
}


mysqli_close($c);
echo "success";