<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: ../admin.php");
}

require("../function/po_data.php");

$productData = getProducts("PD");

$activePage = "PD_PRODUCT";

$product = array();
$isCreated = isset($_GET["is_created"]) ? $_GET["is_created"] : false;
$isDeleted = isset($_GET["is_deleted"]) ? $_GET["is_deleted"] : false;
$isUpdated = isset($_GET["is_updated"]) ? $_GET["is_updated"] : false;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>จัดการสินค้า</title>

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        input {
            font-size: 16px !important;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include "pd_sidebar.php"; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column ">

            <!-- Main Content -->
            <div class="container-fluid container-md">
                <br />

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">จัดการสินค้า</h1>
                    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#addProductModal">
                        <i class="fas fa-plus"></i> เพิ่มสินค้าใหม่
                    </button>
                </div>

                <!-- Content Row -->
                <div class="row">
                    <div class="col-xl-12 col-md-12 mb-4">
                        <div class="card border-left-dark shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-12">


                                        <?php if ($isCreated) { ?>
                                            <div class="alert alert-success row" role="alert" id="result_alert">
                                                เพิ่มสินค้าใหม่เรียบร้อย
                                            </div>
                                        <?php } ?>

                                        <?php if ($isDeleted) { ?>
                                            <div class="alert alert-danger row" role="alert" id="result_alert">
                                                ลบสินค้าเรียบร้อย
                                            </div>
                                        <?php } ?>

                                        <?php if ($isUpdated) { ?>
                                            <div class="alert alert-warning row" role="alert" id="result_alert">
                                                แก้ไขข้อมูลสินค้าเรียบร้อย
                                            </div>
                                        <?php } ?>


                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>ชื่อสินค้า</th>
                                                        <th>หน่วย</th>
                                                        <th class="text-center">ลบ</th>
                                                        <th class="text-center">แก้ไข</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($productData as $row) {
                                                        $productId = $row["product_id"];
                                                        $productName = $row["product_name"];
                                                        $unitName = $row["unit_name"];
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $productName; ?></td>
                                                            <td><?php echo $unitName; ?></td>

                                                            <td align="center">
                                                                <button type="button" class="btn btn-danger" onclick="deleteProduct('<?php echo $productId; ?>','<?php echo $productName; ?>')">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </td>
                                                            <td align="center">
                                                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#editProductModal_<?php echo $productId; ?>">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>

                                                                <!-- Modal -->
                                                                <div class="modal fade" id="editProductModal_<?php echo $productId; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลสินค้า
                                                                                    :
                                                                                    <?php echo $productName; ?></h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pl-5 pr-5">
                                                                                <form class="user pl-5 pr-5" id="editForm_<?php echo $productId; ?>" method="post" action="update_product.php">
                                                                                    <input type="hidden" value="<?php echo $productId; ?>" name="txt_product_id" id="txt_product_id_<?php echo $productId; ?>" />

                                                                                    <div class="row">
                                                                                        <label class="p-0 m-0">
                                                                                            ชื่อสินค้า
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_product_name" value="<?php echo $productName; ?>" id="txt_product_name_<?php echo $productId; ?>" type="text" onkeyup="handleEnter(event)" />
                                                                                    </div>
                                                                                    <div class="row mt-3">
                                                                                        <label class="p-0 m-0">
                                                                                            หน่วย
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_unitname" value="<?php echo $unitName; ?>" id="txt_unitname_<?php echo $productId; ?>" type="text" onkeyup="handleEnter(event)" />
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                                                                <button type="button" class="btn btn-primary" onclick="submitForm('_<?php echo $productId; ?>', '#editForm_<?php echo $productId; ?>')">ยืนยัน</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->


            <!-- Modal -->
            <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">เพิ่มสินค้าใหม่</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body pl-5 pr-5">
                            <form class="user pl-5 pr-5" id="addForm" method="post" action="create_product.php">
                                <input type="hidden" name="product_type" value="PD" />

                                <div class="row">
                                    <label class="p-0 m-0">
                                        ชื่อสินค้า
                                    </label>
                                    <input class="form-control" name="txt_product_name" id="txt_product_name" type="text" onkeyup="handleEnter(event)" />
                                </div>
                                <div class="row mt-3">
                                    <label class="p-0 m-0">
                                        หน่วย
                                    </label>
                                    <input class="form-control" name="txt_unitname" id="txt_unitname" type="text" onkeyup="handleEnter(event)" />
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="button" class="btn btn-primary" onclick="submitForm()">ยืนยัน</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include("../footer.php"); ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="../#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../Scripts/swal.js"></script>

    <!-- Page level plugins -->
    <script src="../js/datatables/jquery.dataTables.min.js"></script>
    <script src="../js/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "order": []
            });
            $("#result_alert").fadeOut(5000);
        });

        function handleEnter(event) {
            if (event.key === "Enter") {
                const form = document.getElementById("addForm");
                const index = [...form].indexOf(event.target);
                form.elements[index + 1].focus();
            }
        }

        function submitForm(productId = "", formId = "#addForm") {
            let productName = $("#txt_product_name" + productId).val();
            let unitName = $("#txt_unitname" + productId).val();

            if (!productName || productName === "") {
                alertError("กรุณากรอกชื่อสินค้า !!");
            } else if (!unitName || unitName === "") {
                alertError("กรุณากรอกหน่วย !!");
                //} else if (!price || parseFloat(price == 0) || price == "0.00" || price == "0") {
                //   alertError("กรุณากรอกราคา !!");
            } else {
                $(formId).submit();
            }
        }

        function deleteProduct(productId, productName) {
            Swal.fire({
                icon: `warning`,
                title: `ลบสินค้า`,
                text: `ต้องการลบสินค้า ${productName} ใช่หรือไม่ ?`,
                showCancelButton: true,
                confirmButtonText: "ยืนยัน",
                cancelButtonText: "ปิด",
                confirmButtonColor: "#d33",
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location = "delete_product.php?product_type=PD&product_id=" + productId;
                }
            });
        }
    </script>
</body>

</html>