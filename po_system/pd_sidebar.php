<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <i class="fas fa-screwdriver"></i>
        <div class="sidebar-brand-text mx-3">สั่งผลิตสินค้า</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  <?php if ($activePage == "PRODUCTION") {
                                echo "active";
                            } ?>">
        <a class="nav-link" href="./production.php">
            <i class="fas fa-screwdriver"></i>
            <span>สั่งผลิตสินค้า</span>
        </a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  <?php if ($activePage == "PRODUCTION_ORDER") {
                                echo "active";
                            } ?>">
        <a class="nav-link" href="./production_order.php">
            <i class="fas fa-list-ul"></i>
            <span>รายการสั่งผลิต</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php if ($activePage == "PD_PRODUCT") {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="./pd_product.php">
            <i class="fas fa-file-download"></i>
            <span>จัดการสินค้า</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php if ($activePage == "REPORT") {
                            echo "active";
                        } ?>">
        <a class="nav-link" href="./report.php?docType=PD">
            <i class="fas fa-chart-bar"></i>
            <span>ออกรายงาน</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link" href="../">
            <i class="fas fa-tv"></i>
            <span>กลับเมนูหลัก</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>