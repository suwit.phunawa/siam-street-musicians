<table width="1000px" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr height="45px">
        <td colspan="<?php echo $reportColspan; ?>" align="center"><span style="font-size: 23px; font-weight:bold;">ทวีสินเบเกอรี่</span></td>
    </tr>
    <tr height="45px">
        <td colspan="<?php echo $reportColspan; ?>" align="center">
            <span style="font-size: 18px; font-weight:bold;">
                <?php echo $billHeader; ?>
            </span>
        </td>
    </tr>
    <tr style="font-weight:bold; font-size:19px;">
        <td align="center">ลำดับ</td>
        <td align="center">ใบสั่งซื้อ</td>
        <td align="center">ร้านค้า</td>
        <td align="center">วันที่</td>
        <td align="center">ชื่อสินค้า</td>
        <td align="center">หน่วย</td>
        <?php if ($docType == "PURCHASE") { ?>
            <td align="center">ราคา</td>
        <?php } ?>
        <td align="center">จำนวน</td>
        <?php if ($docType == "PURCHASE") { ?>
            <td align="center">ราคารวม</td>
        <?php } ?>
    </tr>

    <?php
    $totalAmount = 0;
    $num = 0;
    $totalQty = 0;
    ?>
    <?php while ($row = mysqli_fetch_assoc($result)) {
        $price = $row["price"];
        $qty = $row["qty"];
        $totalPrice = $price * $qty;
        $orderNo = "PO" . str_pad($row["order_id"], 8, '0', STR_PAD_LEFT);
    ?>

        <tr>
            <td align="center">
                <span style="font-size: 19px"><?php echo $num + 1; ?></span>
            </td>
            <td align="left">
                <span style="font-size: 19px"><?php echo $orderNo; ?></span>
            </td>
            <td align="left">
                <span style="font-size: 19px"><?php echo $row["cus_name"]; ?></span>
            </td>
            <td align="center">
                <span style="font-size: 19px"><?php echo strval($row["created_datetime"]); ?></span>
            </td>
            <td align="left">
                <span style="font-size: 19px"><?php echo $row["product_name"]; ?></span>
            </td>
            <td align="center">
                <span style="font-size: 19px"><?php echo $row["unit_name"]; ?></span>
            </td>

            <?php if ($docType == "PURCHASE") { ?>
                <td align="center">
                    <span style="font-size: 19px"><?php echo number_format($price, 2); ?></span>
                </td>
            <?php } ?>

            <td align="center">
                <span style="font-size: 19px"><?php echo number_format($qty, 0); ?></span>
            </td>

            <?php if ($docType == "PURCHASE") { ?>
                <td align="center">
                    <span style="font-size: 19px"><?php echo number_format($totalPrice, 2); ?></span>
                </td>
            <?php } ?>
        </tr>
    <?php
        $totalAmount += $totalPrice;
        $totalQty += $qty;
        $num++;
    } ?>

    <tr>
        <td colspan="<?php echo $reportFooterColspan; ?>" align="right" style="font-weight:bold; font-size:19px;" align="center"><b>รวมทั้งสิ้น</b>
        </td>
        <td align="center" style="font-weight:bold; font-size:19px;" align="center">
            <?php echo number_format($totalQty, 0); ?></td>

        <?php if ($docType == "PURCHASE") { ?>
            <td align="center" style="font-weight:bold; font-size:19px;" align="center">
                <?php echo number_format($totalAmount, 2); ?></td>
        <?php } ?>
    </tr>

</table>
