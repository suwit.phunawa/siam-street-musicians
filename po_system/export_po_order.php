<?php
if (isset($_GET["viewMode"]) && $_GET["viewMode"] == "EXPORT") {
	header("Content-Type: application/xls");
	header("Content-Disposition: attachment; filename=export.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
}

@session_start();
ob_start();

include("../function/FunctionDate.php");
include("../function/connect.php");

$reportType = isset($_GET["reportType"]) ? $_GET["reportType"] : "ORDER";

$startDate = isset($_GET["start_date"]) ? $_GET["start_date"] : "";
$endDate = isset($_GET["end_date"]) ? $_GET["end_date"] : "";
$cusId  = isset($_GET["cus_id"]) ? $_GET["cus_id"] : "";
$cusName  = isset($_GET["cus_name"]) ? $_GET["cus_name"] : "";
$productId  = isset($_GET["product_id"]) ? $_GET["product_id"] : "";
$productName  = isset($_GET["product_name"]) ? $_GET["product_name"] : "";

$docType = isset($_GET["doc_type"]) ? $_GET["doc_type"] : "PURCHASE";

$whereDateCondition = "";
$whereCusCondition = "";
$billHeader = $docType == "PURCHASE" ? "ใบสั่งซื้อสินค้า " : "ใบสั่งผลิตสินค้า ";
$reportColspan = $docType == "PURCHASE" ? 9 : 7;
$reportFooterColspan = $docType == "PURCHASE" ? 7 : 6;

include("report/header_date_period.php");

if ($cusId === "") {
	$cusName = "";
} else {
	$cusName = " (ร้านค้า: $cusName)";
	$whereCusCondition .= " AND (od.cus_id = '$cusId') ";
}

if ($productId === "") {
	$productName = "";
} else {
	$productName = " (สินค้า: $productName)";
	$whereCusCondition .= " AND (odt.product_id = '$productId') ";
}


if ($reportType == "ORDER") {
	$queryFields = " SELECT * ";
	$queryTable = " FROM po_order od INNER JOIN po_order_detail odt ";
	$queryTable .= " ON od.order_id = odt.order_id ";
	$queryGroup = "";
} else {
	$billHeader = str_replace("ใบ", "รายการ", $billHeader);

	$queryFields = " SELECT od.created_datetime, od.order_id ";
	$queryTable = " FROM po_order_detail odt
					INNER JOIN po_order od ON odt.order_id = od.order_id
					INNER JOIN po_product pd ON pd.product_id = odt.product_id ";
	$queryGroup = "";

	$reportColspan = $docType == "PURCHASE" ? 6 : 4;
	$reportFooterColspan = $docType == "PURCHASE" ? 5 : 3;

	if ($docType == "PURCHASE") {
		$queryFields .= ",  sum(odt.price * odt.qty) totalPrice,  odt.price, odt.qty ";
		$queryTable .= " INNER JOIN po_customer cus ON od.cus_id = cus.cus_id ";
	} else {
		$queryFields .= ",  sum(odt.qty) totalQty ";
	}

	if ($reportType == "CUSTOMER") {
		$billHeader .= " $cusName";
		$queryFields .= " ,cus.cus_name ";
		$queryGroup = " GROUP BY od.cus_id, od.created_datetime ";
		$reportColspan += ($cusId == "" ? 1 : 0);
		$reportFooterColspan += ($cusId == "" ? 1 : 0);
	} else if ($reportType == "PRODUCT") {
		$billHeader .= " $productName";
		$queryFields .= " ,pd.product_name, pd.unit_name ";
		$queryGroup = " GROUP BY odt.product_id, od.created_datetime ";
		$reportColspan += ($productId == "" ? 2 : 1);
		$reportFooterColspan += ($productId == "" ? 2 : 1);
	} else if ($reportType == "TOTAL_PRODUCT") {
		$billHeader .= " $productName";
		$queryFields = " SELECT  pd.product_name, pd.unit_name, sum(odt.qty) totalQty ";

		if ($docType == "PURCHASE") {
			$queryFields .= ", sum(odt.price * odt.qty) totalPrice ";
			$reportColspan -= ($productId == "" ? 1 : 2);
			$reportFooterColspan -= ($productId == "" ? 1 : 2);
		} else {
			$reportColspan -= ($productId == "" ? 0 : 1);
			$reportFooterColspan -= ($productId == "" ? 0 : 1);
		}

		$queryGroup = " GROUP BY odt.product_id ";
	}
}

$queryOrder = " ORDER BY od.created_datetime DESC ";
$queryConditions = " WHERE od.document_type = '$docType' $whereDateCondition $whereCusCondition ";

$query = "$queryFields$queryTable$queryConditions$queryGroup$queryOrder";
$result = mysqli_query($c, $query);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title></title>
</head>

<body>

	<?php if ($reportType == "ORDER") {
		include("report/order_view.php");
	} else if ($docType == "PURCHASE") {
		include("report/purchase_report_view.php");
	} else if ($docType == "PRODUCTION") {
		include("report/production_report_view.php");
	} ?>

</body>

</html>