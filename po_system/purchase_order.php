<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: ../admin.php");
}

date_default_timezone_set('Asia/Bangkok');

require("../function/po_data.php");
require("../function/FunctionDate.php");

$activePage = "PURCHASE_ORDER";

$customerData = getCustomers("PO", false);
$customerId = isset($_GET["customer_ddl"]) ? $_GET["customer_ddl"] : "";

$startDate = isset($_GET["txtStartDate"]) ? $_GET["txtStartDate"] : "";
$endDate = isset($_GET["txtEndDate"]) ? $_GET["txtEndDate"] : "";
$searchTimePeriodType = isset($_GET["search_time_period_type"]) ? $_GET["search_time_period_type"] : "DAY";
$curMonth = isset($_GET["search_month_ddl"]) ? $_GET["search_month_ddl"] : date("m") - 1;
$curYear = isset($_GET["search_year_ddl"]) ? $_GET["search_year_ddl"] : date("Y");

if ($searchTimePeriodType == "YEAR") {
    $startDate = "01/01/$curYear";
    $endDate = "31/12/$curYear";
} else if ($searchTimePeriodType == "MONTH") {
    $month = str_pad($curMonth + 1, 2, '0', STR_PAD_LEFT);
    $engMonth = $engMonths[$curMonth];
    $monthYearStr = ($curYear) . "-" . ($curMonth + 1) . "-01";
    $monthYearTime  = strtotime($monthYearStr);
    $monthYearTime = strtotime('-1 second', strtotime('+1 month', $monthYearTime));
    $lastDayOfMonth = date('d/m/Y', $monthYearTime);

    $startDate = "01/$month/$curYear";
    $endDate = $lastDayOfMonth;
}

$orderData = getOrders("PURCHASE", $startDate, $endDate, $customerId);
$isDeleted = isset($_GET["is_deleted"]) ? $_GET["is_deleted"] : false;

$yearOptionsContent = "";


for ($i = 2023; $i <= 2030; $i++) {
    $yearOptionsContent .= "<option value='$i'";
    $yearOptionsContent .= ($i == $curYear ? " selected " : "");
    $yearOptionsContent .= " >$i</option>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>รายการสั่งซื้อ</title>

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


    <style>
        input {
            font-size: 16px !important;
        }

        .select2-container--default .select2-selection--single {
            padding: 4px;
            height: 40px;
            text-align: left;
            width: 100% !important;
        }

        .select2-container {
            width: 210px !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            font-size: 30px;
            height: 40px;
            margin-right: 5px;
            position: absolute;
        }

        .flipped {
            rotate: 90deg;
            font-size: 20px;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include "po_sidebar.php"; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div class="container-fluid container-md">
                <br />
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">รายการสั่งซื้อ</h1>
                </div>

                <!-- Content Row -->
                <div class="row">

                    <div class="col-xl-12 col-md-12 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-12">
                                        <?php if ($isDeleted) { ?>
                                            <div class="alert alert-danger row" role="alert" id="result_alert">
                                                ลบใบสั่งซื้อเรียบร้อย
                                            </div>
                                        <?php } ?>

                                        <div>
                                            <div class="row pl-3">
                                                <form class="" action="" method="GET" id="search_form">
                                                    <input type="hidden" name="hideStartDate" id="hideStartDate" value="<?php echo $startDate; ?>" class="form-control ml-1">
                                                    <input type="hidden" name="hideEndDate" id="hideEndDate" value="<?php echo $endDate; ?>" class="form-control ml-1">

                                                    <div class="form-inline">
                                                        <div class="form-group mb-2">
                                                            <b>ค้นหาจาก</b>
                                                        </div>
                                                        <div class="form-group mx-sm-2 mb-2">
                                                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                                <label class="btn btn-secondary active">
                                                                    <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option1" value="DAY" autocomplete="off" <?php if ($searchTimePeriodType == "DAY") {
                                                                                                                                                                                                    echo " checked ";
                                                                                                                                                                                                } ?>>
                                                                    วันที่
                                                                </label>
                                                                <label class="btn btn-secondary">
                                                                    <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option2" value="MONTH" autocomplete="off" <?php if ($searchTimePeriodType == "MONTH") {
                                                                                                                                                                                                        echo " checked ";
                                                                                                                                                                                                    } ?>>
                                                                    เดือน
                                                                </label>
                                                                <label class="btn btn-secondary">
                                                                    <input type="radio" onclick="submitSearchForm()" name="search_time_period_type" id="option3" value="YEAR" autocomplete="off" <?php if ($searchTimePeriodType == "YEAR") {
                                                                                                                                                                                                        echo " checked ";
                                                                                                                                                                                                    } ?>>
                                                                    ปี
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-inline">
                                                        <i class="fas fa-level-up-alt flipped ml-3 mb-2 text-primary"></i>

                                                        <?php if ($searchTimePeriodType == "DAY") { ?>
                                                            <div class="form-group mb-2 ml-4">
                                                                วันที่เริ่มต้น :
                                                                <input type="text" name="txtStartDate" id="txtStartDate" value="<?php echo $startDate; ?>" class="form-control ml-1">
                                                            </div>
                                                            <div class="form-group mx-sm-1 mb-2 pl-3">
                                                                สิ้นสุด :
                                                                <input type="text" name="txtEndDate" id="txtEndDate" value="<?php echo $endDate; ?>" class="form-control ml-1">
                                                            </div>
                                                        <?php } else if ($searchTimePeriodType == "MONTH") { ?>
                                                            <div class="form-group mb-2 ml-4">
                                                                เดือน :
                                                                <select name="search_month_ddl" class="form-control ml-2">
                                                                    <?php foreach ($thaiMonths as $key => $value) {
                                                                        echo "<option value='$key'";
                                                                        echo $curMonth == $key ? " selected " : "";
                                                                        echo ">$value</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        <?php }
                                                        if ($searchTimePeriodType == "YEAR" || $searchTimePeriodType == "MONTH") { ?>
                                                            <div class="form-group mb-2 ml-4">
                                                                ปี :
                                                                <select name="search_year_ddl" class="form-control ml-2">
                                                                    <?php echo $yearOptionsContent; ?>
                                                                </select>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <hr>
                                                    <div class="form-inline mt-2">
                                                        <div class="form-group mb-2  mr-4">
                                                            <b style="margin-right:14px;">ค้นหาจากร้านค้า</b>
                                                        </div>
                                                        <div class="form-group  mb-2">
                                                            <select class="form-control" name="customer_ddl" id="customer_ddl">
                                                                <option value="" selected>ทุกร้าน</option>
                                                                <?php
                                                                foreach ($customerData as $row) {
                                                                    echo "<option value='" . $row["cus_id"] . "'";

                                                                    if ($row["cus_id"] == $customerId) {
                                                                        echo " selected ";
                                                                    }

                                                                    echo ">" . $row["cus_name"] . "</option>";
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-inline mt-2">
                                                        <div class="form-group ">
                                                            <button class="btn btn-light" style="border:1px solid #ccc;" type="submit" id="search_bumit_btn">ค้นหา</button>
                                                        </div>
                                                        <div class="form-group ml-2">
                                                            <button class="btn btn-primary" type="button" onclick="exportExcel(event)">ออกรายงาน</button>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <br />
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>ลำดับ</th>
                                                    <th>เลขที่เอกสาร</th>
                                                    <th>ร้านค้า</th>
                                                    <th>วันที่สั่งซื้อ</th>
                                                    <th>จำนวนรวม</th>
                                                    <th>ราคารวม</th>
                                                    <th class="text-center">รายละเอียด</th>
                                                    <th class="text-center">ลบ</th>
                                                    <th class="text-center d-none">ออกรายงาน</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $rowCount = 1;
                                                foreach ($orderData as $row) {
                                                    $orderId = $row["order_id"];
                                                    $docNo = "PO" . str_pad($orderId, 8, '0', STR_PAD_LEFT);
                                                    $totalAmount = $row["total_amount"];
                                                ?>
                                                    <tr>
                                                        <td><?php echo $rowCount; ?></td>
                                                        <td><?php echo $docNo; ?></td>
                                                        <td><?php echo $row["cus_name"]; ?></td>
                                                        <td>
                                                            <?php
                                                            $time_input = strtotime($row["created_datetime"]);
                                                            $date_input = getDate($time_input);
                                                            echo $date_input["mday"] . "/" . $date_input["mon"] . "/" . $date_input["year"];
                                                            echo " " . $date_input["hours"] . ":" . $date_input["minutes"];
                                                            ?>
                                                        </td>
                                                        <td><?php echo $row["total_qty"]; ?></td>
                                                        <td><?php echo number_format($totalAmount, 2); ?></td>
                                                        <td align="center">
                                                            <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#detailModal_<?php echo $orderId; ?>">
                                                                <i class="fas fa-search"></i>
                                                            </button>

                                                            <!-- Modal -->
                                                            <div class="modal fade" id="detailModal_<?php echo $orderId; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                                รายละเอียดใบสั่งซื้อ
                                                                                :
                                                                                <?php echo $docNo; ?></h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body pl-5 pr-5">
                                                                            <div class="row">
                                                                                <table class="table table-bordered" width="100%" cellspacing="0">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>ชื่อสินค้า</th>
                                                                                            <th>หน่วย</th>
                                                                                            <th class="text-right">ราคา
                                                                                            </th>
                                                                                            <th class="text-right">จำนวน
                                                                                            </th>
                                                                                            <th class="text-right">
                                                                                                ราคารวม</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php foreach ($row["order_detail"] as $detailRow) {
                                                                                            $productName = $detailRow["product_name"];
                                                                                            $unitName = $detailRow["unit_name"];
                                                                                            $price = $detailRow["price"];
                                                                                            $qty = $detailRow["qty"];
                                                                                            $totalPrice = $price * $qty;

                                                                                            echo "<tr>";
                                                                                            echo "<td>$productName</td>";
                                                                                            echo "<td>$unitName</td>";
                                                                                            echo "<td align='right'>" . number_format($price, 0) . "</td>";
                                                                                            echo "<td align='right'>" . number_format($qty, 0) . "</td>";
                                                                                            echo "<td align='right'>" . number_format($totalPrice, 2) . "</td>";
                                                                                            echo "</tr>";
                                                                                        }
                                                                                        ?>
                                                                                    <tbody>
                                                                                </table>

                                                                            </div>

                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <div class="row mt-2 col-12">
                                                                                <div class="col">
                                                                                    <h5 class="text-success">
                                                                                        รวมทั้งสิ้น :
                                                                                        <?php echo number_format($totalAmount, 2); ?>
                                                                                    </h5>
                                                                                </div>
                                                                                <div class="col" align="right">
                                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <button type="button" class="btn btn-danger" onclick="deleteOrder('<?php echo $orderId; ?>','<?php echo $docNo; ?>')">
                                                                <i class="fas fa-trash-alt"></i>
                                                            </button>
                                                        </td>
                                                        <td align="center" class="d-none">
                                                            <button type="button" class="btn btn-primary" onclick="printReport('<?php echo $orderId; ?>')">
                                                                <i class="fas fa-print"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                <?php
                                                    $rowCount++;
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Footer -->
            <?php include("../footer.php"); ?>
            <!-- End of Footer -->
        </div>
        <!-- End of Main Content -->


    </div>
    <!-- End of Content Wrapper -->



    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="../#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->

    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../Scripts/swal.js"></script>

    <!-- Page level plugins -->
    <script src="../js/datatables/jquery.dataTables.min.js"></script>
    <script src="../js/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "order": []
            });

            $("#txtStartDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $("#txtEndDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $("#result_alert").fadeOut(5000);

            $("#customer_ddl").select2();
        });

        function deleteOrder(orderId, docNo) {
            Swal.fire({
                icon: `warning`,
                title: `ลบใบสั่งซื้อ`,
                text: `ลบใบสั่งซื้อเลขที่ ${docNo} ใช่หรือไม่ ?`,
                showCancelButton: true,
                confirmButtonText: "ยืนยัน",
                cancelButtonText: "ปิด",
                confirmButtonColor: "#d33",
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location = "delete_purchase_order.php?doc_type=PURCHASE&order_id=" + orderId;
                }
            });
        }

        function exportExcel() {
            
            let startDate = $("#txtStartDate").val();
            let endDate = $("#txtEndDate").val();
            const cusId = $("#customer_ddl").val();
            
            if (!startDate && startDate !== "") {
                
                startDate = $("#hideStartDate").val();
                endDate = $("#hideEndDate").val();
            }

            window.location = "export_po_order.php?viewMode=EXPORT&doc_type=PURCHASE&start_date=" + startDate + "&end_date=" + endDate +
                "&cus_id=" + cusId;

            setInterval(function() {
                $("#search_bumit_btn").click();
            }, 1000);
        }

        function submitSearchForm() {
            $("#search_form").submit();
        }
    </script>
</body>

</html>