<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <i class="fas fa-truck"></i>
        <div class="sidebar-brand-text mx-3">สั่งซื้อ</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php if($activePage == "PURCHASE") { echo "active";} ?>">
        <a class="nav-link" href="./purchase.php" style="font-size: 10px !important;">
            <i class="fas fa-truck"></i>
            <span>สั่งซื้อสินค้า</span>
        </a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item <?php if($activePage == "PURCHASE_ORDER") { echo "active";} ?>">
        <a class="nav-link" href="./purchase_order.php">
            <i class="fas fa-list-ul"></i>
            <span>รายการสั่งซื้อ</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  <?php if($activePage == "PRODUCT") { echo "active";} ?>">
        <a class="nav-link" href="./product.php">
            <i class="fas fa-box-open"></i>
            <span>จัดการสินค้า</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  <?php if($activePage == "PO_CUSTOMER") { echo "active";} ?>">
        <a class="nav-link" href="./po_customer.php">
            <i class="fas fa-users"></i>
            <span>จัดการร้านค้า</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider">


    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item  <?php if($activePage == "REPORT") { echo "active";} ?>">
        <a class="nav-link" href="./report.php">
        <i class="fas fa-chart-bar"></i>
            <span>ออกรายงาน</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link" href="../">
            <i class="fas fa-tv"></i>
            <span>กลับเมนูหลัก</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>