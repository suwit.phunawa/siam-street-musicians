<?php
@session_start();

include("../function/connect.php");

$productId = isset($_GET["product_id"]) ? $_GET["product_id"] : "";
$productType = isset($_GET["product_type"]) ? $_GET["product_type"] : "PO";

if ($productId == "") {
	echo "<script language='javascript'> alert('Data Invalid !'); window.history.back(); </script>";
	exit();
}

$query = "delete from po_product where product_id = '$productId'";

$result = mysqli_query($c, $query);

mysqli_close($c);

$redirectPage = $productType == "PD" ? "pd_product" : "product";
echo "<meta  http-equiv='refresh' content='1;url=$redirectPage.php?&is_deleted=true'>";
