<?php
@session_start();

include("../function/connect.php");

$productId = isset($_POST["product_ddl"]) ? $_POST["product_ddl"] : "";
$productName = isset($_POST["product_name"]) ? $_POST["product_name"] : "";
$unitName = isset($_POST["txt_unitname"]) ? $_POST["txt_unitname"] : "";
$price = isset($_POST["txt_price"]) ? $_POST["txt_price"] : "0";
$qty = isset($_POST["txt_qty"]) ? $_POST["txt_qty"] : "";
$docType = isset($_POST["doc_type"]) ? $_POST["doc_type"] : "";

if (
	$productName == "" || $productId == "" || $unitName == "" || $qty == ""
	//|| ($docType == "PURCHASE" &&  ($price == "0" || $price == ""))
) {
	echo "<script language='javascript'> alert('Data Invalid !'); window.history.back(); </script>";
	exit();
}


$query = "insert into po_order (document_type, product_id, product_name, unit_name, price, qty, created_datetime) ";
$query .= " VALUES ('$docType', '$productId', '$productName',";
$query .= "'$unitName', $price, $qty, '" . date("Y-m-d H:i:s") . "')";

$result = mysqli_query($c, $query);

mysqli_close($c);

$redirectPage = $docType == "PURCHASE" ? "purchase_order.php" : "purchase_order.php";

echo "<meta  http-equiv='refresh' content='1;url=$redirectPage?product_id=$productId&is_created=true'>";
