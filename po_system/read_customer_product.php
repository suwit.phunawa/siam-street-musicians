<?php
@session_start();

include("../function/connect.php");
include("../function/po_data.php");

$cusId = isset($_POST["cus_id"]) ? $_POST["cus_id"] : "";
$customerData = getCustomerProduct($cusId);

?>

<table class="table table-bordered" width="100%" cellspacing="0">
	<thead>
		<tr>
			<th>ชื่อสินค้า</th>
			<th>หน่วย</th>
			<th>ราคา</th>
			<th class="text-center">ลบ</th>
		</tr>
	</thead>
	<tbody>

		<?php
		foreach ($customerData as $row) {
			$productId = $row["product_id"];
			echo "<tr>
							<td>" . $row["product_name"] . "</td>
							<td>" . $row["unit_name"] . "</td>
							<td>" . $row["price"] . "</td>
							<td align='center'>";
		?>
			<button type="button" class="btn btn-danger" onclick="deleteCustomerProduct('<?php echo $cusId; ?>', '<?php echo $productId; ?>')">
				<i class='fas fa-trash-alt'></i>
			</button>
		<?php
			echo "</td>
						</tr>";
		}
		?>

	</tbody>
</table>