<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: ../admin.php");
}

require("../function/po_data.php");

$activePage = "PURCHASE";
$productData = getProducts();
$customerData = getCustomers("PO", false);
$curDate = date("d/m/Y");
$isCreated = false;


if (isset($_GET["is_created"]) && $_GET["is_created"]) {
    $isCreated = $_GET["is_created"];
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>สั่งซื้อสินค้า</title>

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        input {
            font-size: 16px !important;
        }

        .select2-container--default .select2-selection--single {
            padding: 4px;
            height: 40px;
            text-align: left;
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            font-size: 30px;
            height: 40px;
            margin-right: 5px;
        }

        .removeProduct {
            cursor: pointer;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include "po_sidebar.php"; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column ">

            <!-- Main Content -->
            <div class="container-fluid container-md">
                <br />
                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">สั่งซื้อสินค้า</h1>
                </div>

                <!-- Content Row -->
                <div class="row">

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-12 col-md-12 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">


                                <?php if ($isCreated) { ?>
                                    <div class="row pl-3 pr-3">
                                        <div class="alert alert-success col-md-12" role="alert" id="success_alert">
                                            สร้างรายการสั่งซื้อเรียบร้อย
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row no-gutters">
                                    <div class="col-md-6">
                                        <div class="p-5">

                                            <div class="text-center">
                                                <h1 class="h4 text-gray-900 mb-4">กรอกข้อมูลสั่งซื้อสินค้า</h1>
                                            </div>
                                            <form class="user" id="form" method="post" action="create_purchase.php">
                                                <input type="hidden" name="product_name" id="product_name" />

                                                <input type="hidden" name="doc_type" value="PURCHASE" />


                                                <div class="row">
                                                    <label class="p-0 m-0">
                                                        วันที่สั่งซื้อ
                                                    </label>
                                                    <input type="text" name="txt_purchase_date" id="txt_purchase_date" value="<?php echo $curDate; ?>" class="form-control ml-1">
                                                </div>

                                                <div class="row  mt-3">
                                                    <label class="p-0 m-0">
                                                        ชื่อร้านค้า
                                                    </label>
                                                    <select class="form-control" name="customer_ddl" id="customer_ddl" onchange="setProduct()">
                                                        <option value="" disabled selected>- กรุณาเลือกร้านค้า -</option>
                                                        <?php
                                                        foreach ($customerData as $row) {
                                                            echo "<option value='" . $row["cus_id"] . "'";
                                                            echo ">" . $row["cus_name"] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="row mt-3" id="product_ddl_content">
                                                </div>
                                                <div class="row mt-3">
                                                    <label class="p-0 m-0">
                                                        หน่วย
                                                    </label>
                                                    <input class="form-control" name="txt_unitname" id="txt_unitname" value="" type="text" readonly>
                                                </div>
                                                <div class="row mt-3">
                                                    <label class="p-0 m-0">
                                                        จำนวน
                                                    </label>
                                                    <input class="form-control" type="number" name="txt_qty" id="txt_qty" value="" onkeyup="calculateTotalPrice(event)">
                                                </div>
                                                <div class="row mt-3">
                                                    <label class="p-0 m-0">
                                                        ราคา
                                                    </label>
                                                    <input class="form-control" type="number" name="txt_price" id="txt_price" onkeyup="calculateTotalPrice(event)" value="" onkeyup="">
                                                </div>
                                                <div class="row mt-3">
                                                    <label class="p-0 m-0">
                                                        รวม
                                                    </label>
                                                    <input class="form-control" type="number" name="txt_totalprice" id="txt_totalprice" readonly>
                                                </div>
                                                <div class="row mt-4">
                                                    <button type="button" onclick="addProduct()" class="btn btn-primary btn-block" style="font-size:large;">
                                                        เพิ่มสินค้า
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="p-5">
                                            <!-- Illustrations -->
                                            <div class="card shadow">
                                                <div class="card-header py-3">
                                                    <h6 class="m-0 font-weight-bold text-primary"> ใบสั่งซื้อ <span id="selected_customer_name"></span></h6>
                                                </div>
                                                <div class="card-body">
                                                    <form id="productsForm">
                                                        <input type="hidden" value="PURCHASE" name="txt_doc_type" />
                                                        <input type="hidden" name="customer_name" id="customer_name" />
                                                        <input type="hidden" name="customer_id" id="customer_id" />
                                                        <input type="hidden" name="po_date" id="purchase_date" />

                                                        <div id="products" class="mt-2">
                                                            <!-- Products will be dynamically appended here -->
                                                        </div>
                                                    </form>

                                                    <div class="row mb-4" align="right">
                                                        <div class="col">
                                                            <h5 class="text-success">รวมทั้งสิ้น : <span id="totalPOPrice">0.00<span></h5>
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="submitForm()" id="submit_btn" class="btn btn-success btn-block" style="font-size:large;">
                                                        ยืนยันการสั่งซื้อ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php include("../footer.php"); ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="../#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="../Scripts/swal.js"></script>
    <script>
        $(document).ready(function() {
            calculateTotalPrice();
            setUnitName();
            $("#success_alert").fadeOut(5000);
            $("#submit_btn").hide();
            $("#product_ddl_content").hide();
            $("#txt_purchase_date").datepicker({
                dateFormat: 'dd/mm/yy'
            });

            $('#products').on('click', '.removeProduct', function() {
                $(this).closest('.product').remove();
                checkProductForm();
            });

            $('#product_ddl').select2();
        });

        function setUnitName() {
            let productId = $("#product_ddl :selected").val();

            if (productId && productId != "") {
                $.ajax({
                    url: '../function/po_product.php?product_id=' + productId,
                    type: 'GET',
                    dataType: 'json',
                    success: function(product) {
                        $("#txt_unitname").val(product.unit_name);
                        $("#txt_price").val(parseFloat(product.price));
                    },
                    error: function(xhr, status, error) {
                        alertError(error);
                    }
                });
            }
        }

        function setProduct() {
            let cusId = $("#customer_ddl :selected").val();
            let cusName = $("#customer_ddl :selected").text();
            
            $("#selected_customer_name").html("ร้าน" + cusName);
            $("#customer_name").val(cusName);
            $("#customer_id").val(cusId);
            
            if (cusId && cusId != "") {

                $.ajax({
                    url: 'read_product.php?cus_id=' + cusId,
                    type: 'GET',
                    success: function(content) {
                        $("#product_ddl_content").show();
                        $("#product_ddl_content").html(content);
                        setUnitName();
                    },
                    error: function(xhr, status, error) {
                        alertError(error);
                    }
                });
            }
        }

        function calculateTotalPrice(event) {
            let price = $("#txt_price").val();
            let qty = $("#txt_qty").val();

            console.log(price);
            console.log(qty);
            let total = (price || 0) * (qty || 0);
            $("#txt_totalprice").val(total.toFixed(2));

            if (event) {
                handleEnter(event);
            }
        }

        function resetForm() {
            $("#txt_qty").val("");
            $("#txt_totalprice").val("");
        }

        function handleEnter(event) {
            if (event.key === "Enter") {
                const form = document.getElementById("form");
                const index = [...form].indexOf(event.target);
                form.elements[index + 1].focus();
            }
        }

        function checkProductForm() {
            var productData = $('#productsForm').serialize();

            if (!productData || productData === "" || productData === "txt_doc_type=PURCHASE") {
                $("#submit_btn").hide();
                $('#customer_ddl').prop("disabled", false);
            } else {
                $("#submit_btn").show();
                $('#customer_ddl').prop("disabled", true);
            }
        }

        function addProduct() {
            let cusId = $("#customer_ddl :selected").val();
            let productId = $("#product_ddl :selected").val();
            let productName = $("#product_ddl :selected").text();
            let qty = $("#txt_qty").val();
            let unitName = $("#txt_unitname").val();
            let price = $("#txt_price").val();

            if (!cusId || cusId === "") {
                alertError("กรุณาเลือกร้านค้า");
            } else if (!productId || productId === "") {
                alertError("กรุณาเลือกสินค้า");
            } else if (!qty || parseFloat(qty == 0) || qty == "0.00") {
                alertError("กรุณากรอก จำนวน");
            } else {

                var productElement =
                    ` <div class="product">
                        <input type="hidden" name="txt_product_id[]" value="${productId}" />
                        <input type="hidden" name="txt_product_name[]" value="${productName}" />
                        <input type="hidden" name="txt_unit_name[]" value="${unitName}" />
                        <input type="hidden" name="txt_qty[]" value="${qty}" />
                        <input type="hidden" name="txt_price[]" value="${price}" />

                        <div class="row">
                            <div class="col-4">
                                ${productName}
                            </div>
                            <div class="col text-right">
                                x<span class="qty">${qty}</span>
                            </div>
                            <div class="col text-right">
                                ฿<span class="price">${price}</span>
                            </div>
                            <div class="col-2 text-center">
                                <span class="text-danger removeProduct" onclick="reducePrice(${price*qty})"><i class="fas fa-times"></i></span>
                            </div>
                        </div>
                        <hr>
                      </div>`;

                $('#products').append(productElement);

                let curTotalPOPrice = $("#totalPOPrice").text();
                let poPrice = qty * price;
                let totalPOPrice = parseFloat(curTotalPOPrice) + poPrice;
                $("#totalPOPrice").text(totalPOPrice);

                resetForm();
                checkProductForm();
            }
        }

        function reducePrice(price) {
            let curTotalPOPrice = $("#totalPOPrice").text();
            let totalPOPrice = parseFloat(curTotalPOPrice) - price;
            $("#totalPOPrice").text(totalPOPrice);
        }

        function submitForm() {
            let purchaseDate = $("#txt_purchase_date").val();
            $("#purchase_date").val(purchaseDate);
            
            var productData = $('#productsForm').serialize();

            if (!productData || productData === "") {
                alertError("กรุณาเพิ่มสินค้าที่ต้องการสั่งซื้อ");
                return false;
            }

            $.ajax({
                type: 'POST',
                url: 'create_po_order.php',
                data: productData,
                success: function(response) {
                    if (response === "success") {
                        window.location = "purchase.php?is_created=true";
                    } else {
                        console.log("response", response);
                        alertError("ผิดพลาด!! ไม่สามารถบันทึกข้อมูลได้");
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    </script>
</body>

</html>