<?php
if ($startDate != "") {
	if ($endDate == "") {
		$endDate = $startDate;
	}

	$startDates = explode("/", $startDate);
	$endDates = explode("/", $endDate);

	$startDate = date("Y-m-d", strtotime($startDates[2] . "-" . $startDates[1] . "-" . $startDates[0]));
	$endDate = date("Y-m-d", strtotime($endDates[2] . "-" . $endDates[1] . "-" . $endDates[0]));

	$whereDateCondition .= " AND (od.created_datetime BETWEEN '$startDate 00:00:00'  AND '$endDate 23:59:59') ";

	$startDay = (int)$startDates[0];
	$startMonth = (int)$startDates[1];
	$startYear = $startDates[2];
	$endDay = (int)$endDates[0];
	$endMonth = (int)$endDates[1];
	$endYear = $endDates[2];

	if (($startYear . $startMonth) == ($endYear . $endMonth)) {
		$datePeriod = $startDate == $endDate ? $startDay : "$startDay - $endDay";
		$billHeader .= " วันที่ $datePeriod " . $thaiMonths[((int)$startMonth) - 1] . " $startYear";
	} else if ($startYear == $endYear) {
		$billHeader .= " วันที่ $startDay " . $thaiMonths[((int)$startMonth) - 1] . " - $endDay "  . $thaiMonths[((int)$endMonth) - 1] . " $startYear";
	} else {
		$billHeader .= " วันที่ $startDay " . $thaiMonths[((int)$startMonth) - 1] . " $startYear - $endDay "  . $thaiMonths[((int)$endMonth) - 1] . " $endYear";
	}
}
