<table width="1000px" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr height="45px">
        <td colspan="<?php echo $reportColspan; ?>" align="center"><span style="font-size: 23px; font-weight:bold;">ทวีสินเบเกอรี่</span></td>
    </tr>
    <tr height="45px">
        <td colspan="<?php echo $reportColspan; ?>" align="center">
            <span style="font-size: 18px; font-weight:bold;">
                <?php echo $billHeader; ?>
            </span>
        </td>
    </tr>
    <tr style="font-weight:bold; font-size:19px;">
        <td align="center">ลำดับ</td>
        <?php if ($productId == "" &&    ($reportType == "PRODUCT" || $reportType == "TOTAL_PRODUCT")) { ?>
            <td align="center">สินค้า</td>
        <?php } ?>

        <?php if ($reportType != "TOTAL_PRODUCT") { ?>
            <td align="center">ใบสั่งผลิต</td>
            <td align="center">วันที่</td>
        <?php } ?>
        <td align="center">หน่วย</td>
        <td align="center">จำนวนรวม</td>
    </tr>

    <?php
    $totalAmount = 0;
    $num = 0;
    $totalQty = 0;
    ?>
    <?php while ($row = mysqli_fetch_assoc($result)) {
        $totalQty = $row["totalQty"];
        $orderNo = $reportType == "TOTAL_PRODUCT" ? "" : "PO" . str_pad($row["order_id"], 8, '0', STR_PAD_LEFT);
    ?>

        <tr>
            <td align="center">
                <span style="font-size: 19px"><?php echo $num + 1; ?></span>
            </td>
            <?php if ($productId == "" &&  ($reportType == "PRODUCT" || $reportType == "TOTAL_PRODUCT")) { ?>
                <td align="left">
                    <span style="font-size: 19px"><?php echo $row["product_name"]; ?></span>
                </td>
            <?php } ?>

            <?php if ($reportType != "TOTAL_PRODUCT") { ?>
                <td align="left">
                    <span style="font-size: 19px"><?php echo $orderNo; ?></span>
                </td>
                <td align="center">
                    <span style="font-size: 19px"><?php echo strval($row["created_datetime"]); ?></span>
                </td>
            <?php } ?>

            <td align="center">
                <span style="font-size: 19px"><?php echo $row["unit_name"]; ?></span>
            </td>
            <td align="center">
                <span style="font-size: 19px"><?php echo number_format($totalQty, 0); ?></span>
            </td>
        </tr>
    <?php
        $totalAmount += $totalQty;
        $num++;
    } ?>

    <tr>
        <td colspan="<?php echo $reportFooterColspan; ?>" align="right" style="font-weight:bold; font-size:19px;" align="center"><b>รวมทั้งสิ้น</b>
        </td>
        <td align="center" style="font-weight:bold; font-size:19px;" align="center">
            <?php echo number_format($totalAmount, 0); ?></td>
    </tr>

</table>