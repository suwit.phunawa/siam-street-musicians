<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: ../admin.php");
}

require("../function/po_data.php");

$activePage = "PO_CUSTOMER";

$customerData = getCustomers();
$productData = getProducts();

$customer = array();
$isCreated = isset($_GET["is_created"]) ? $_GET["is_created"] : false;
$isDeleted = isset($_GET["is_deleted"]) ? $_GET["is_deleted"] : false;
$isUpdated = isset($_GET["is_updated"]) ? $_GET["is_updated"] : false;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>จัดการร้านค้า</title>

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link href="../css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <link href="../css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        input {
            font-size: 16px !important;
        }

        .select2-container--default .select2-selection--single {
            padding: 4px;
            height: 40px;
            text-align: left;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            font-size: 30px;
            height: 40px;
            margin-right: 5px;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include "po_sidebar.php"; ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column ">

            <!-- Main Content -->
            <div class="container-fluid container-md">
                <br />

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">จัดการร้านค้า</h1>
                    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#addProductModal">
                        <i class="fas fa-plus"></i> เพิ่มร้านค้าใหม่
                    </button>
                </div>

                <!-- Content Row -->
                <div class="row">
                    <div class="col-xl-12 col-md-12 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col-md-12">


                                        <?php if ($isCreated) { ?>
                                            <div class="alert alert-success row" role="alert" id="result_alert">
                                                เพิ่มร้านค้าใหม่เรียบร้อย
                                            </div>
                                        <?php } ?>

                                        <?php if ($isDeleted) { ?>
                                            <div class="alert alert-danger row" role="alert" id="result_alert">
                                                ลบร้านค้าเรียบร้อย
                                            </div>
                                        <?php } ?>

                                        <?php if ($isUpdated) { ?>
                                            <div class="alert alert-warning row" role="alert" id="result_alert">
                                                แก้ไขข้อมูลร้านค้าเรียบร้อย
                                            </div>
                                        <?php } ?>


                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>ชื่อร้านค้า</th>
                                                        <th>ที่อยู่</th>
                                                        <th>เบอร์โทรศัพท์</th>
                                                        <th>เลขประจำตัวเสียภาษี</th>
                                                        <th class="text-center">กำหนดสินค้า</th>
                                                        <th class="text-center">ลบ</th>
                                                        <th class="text-center">แก้ไข</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($customerData as $row) {
                                                        $cusId = $row["cus_id"];
                                                        $cusName = $row["cus_name"];
                                                        $cusPhone = $row["cus_phone"];
                                                        $cusAddress = $row["cus_address"];
                                                        $cusTaxId = $row["cus_taxid"];

                                                    ?>
                                                        <tr>
                                                            <td><?php echo $cusName; ?></td>
                                                            <td><?php echo $cusAddress; ?></td>
                                                            <td><?php echo $cusPhone; ?></td>
                                                            <td><?php echo $cusTaxId; ?></td>
                                                            <td align="center">
                                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#mapProductModal_<?php echo $cusId; ?>">
                                                                    <i class="fas fa-cart-plus"></i>
                                                                </button>

                                                                <!-- Modal -->
                                                                <div class="modal fade" id="mapProductModal_<?php echo $cusId; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">กำหนดสินค้า
                                                                                    :
                                                                                    <?php echo $cusName; ?></h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pl-3 pr-3 user">
                                                                                <form class="form-inline user pl-3 pr-3" action="" method="GET">
                                                                                    <div class="row">
                                                                                        <form class="form-inline" action="" method="GET">
                                                                                            <div class="form-group mb-1">
                                                                                                <b>เลือกสินค้า</b>
                                                                                            </div>
                                                                                            <div class="form-group mb-2 ml-3">
                                                                                                <select class="form-control product_ddl text-left" name="product_ddl_<?php echo $cusId; ?>" id="product_ddl_<?php echo $cusId; ?>" style="width:220px; height:200px !important;">
                                                                                                    <?php
                                                                                                    foreach ($productData as $productRow) {
                                                                                                        echo "<option value='" . $productRow["product_id"] . "' ";
                                                                                                        echo ">" . $productRow["product_name"] . "</option>";
                                                                                                    }
                                                                                                    ?>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group mx-sm-2 mb-2 ml-1">
                                                                                                <button type="button" class="btn btn-primary" id="delete_cus_product_btn_<?php echo $cusId; ?>" onclick="addCustomerProduct('<?php echo $cusId; ?>')">เพิ่มสินค้า</button>
                                                                                            </div>
                                                                                    </div>
                                                                                </form>
                                                                                <hr>

                                                                                <div class="row p-2">
                                                                                    <div class="col-12">
                                                                                        <div class="card">
                                                                                            <div class="card-header py-3">
                                                                                                <h6 class="m-0 font-weight-bold text-primary text-left"> รายการสินค้า</h6>
                                                                                            </div>
                                                                                            <div class="card-body" id="customer_product_table_<?php echo $cusId; ?>">
                                                                                                <table class="table table-bordered" width="100%" cellspacing="0">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <th>ชื่อสินค้า</th>
                                                                                                            <th>หน่วย</th>
                                                                                                            <th>ราคา</th>
                                                                                                            <th class="text-center">ลบ</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody>
                                                                                                        <?php
                                                                                                        foreach ($row["customer_product"] as $cusProductRow) {
                                                                                                            $cusProductId = $cusProductRow["product_id"];
                                                                                                            echo "<tr>
                                                                                                                            <td>" . $cusProductRow["product_name"] . "</td>
                                                                                                                            <td>" . $cusProductRow["unit_name"] . "</td>
                                                                                                                            <td>" . $cusProductRow["price"] . "</td>
                                                                                                                            <td align='center'>";
                                                                                                        ?>
                                                                                                            <button type="button" class="btn btn-danger" onclick="deleteCustomerProduct('<?php echo $cusId; ?>', '<?php echo $cusProductId; ?>')">
                                                                                                                <i class='fas fa-trash-alt'></i>
                                                                                                            </button>
                                                                                                        <?php
                                                                                                            echo "</td>
                                                                                                                        </tr>";
                                                                                                        }
                                                                                                        ?>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td align="center">
                                                                <button type="button" class="btn btn-danger" onclick="deleteCustomer('<?php echo $cusId; ?>','<?php echo $cusName; ?>')">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </td>
                                                            <td align="center">
                                                                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#editCustomerModal_<?php echo $cusId; ?>">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>

                                                                <!-- Modal -->
                                                                <div class="modal fade" id="editCustomerModal_<?php echo $cusId; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">แก้ไขข้อมูลร้านค้า
                                                                                    :
                                                                                    <?php echo $cusName; ?></h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body pl-5 pr-5">
                                                                                <form class="user pl-5 pr-5" id="editForm_<?php echo $cusId; ?>" method="post" action="update_customer.php">
                                                                                    <input type="hidden" value="<?php echo $cusId; ?>" name="txt_customer_id" id="txt_customer_id_<?php echo $cusId; ?>" />

                                                                                    <div class="row">
                                                                                        <label class="p-0 m-0">
                                                                                            ชื่อร้านค้า <span style="color:red;">*</span>
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_customer_name" value="<?php echo $cusName; ?>" id="txt_customer_name_<?php echo $cusId; ?>" type="text" onkeyup="handleEnter(event, editForm_<?php echo $cusId; ?>)" />
                                                                                    </div>
                                                                                    <div class="row mt-3">
                                                                                        <label class="p-0 m-0">
                                                                                            โทรศัพท์
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_customer_phone" value="<?php echo $cusPhone; ?>" id="txt_customer_phone_<?php echo $cusId; ?>" type="text" onkeyup="handleEnter(event, editForm_<?php echo $cusId; ?>)" />
                                                                                    </div>
                                                                                    <div class="row mt-3">
                                                                                        <label class="p-0 m-0">
                                                                                            ที่อยู่
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_customer_address" value="<?php echo $cusAddress; ?>" id="txt_customer_address_<?php echo $cusId; ?>" type="text" onkeyup="handleEnter(event, editForm_<?php echo $cusId; ?>)" />
                                                                                    </div>
                                                                                    <div class="row mt-3">
                                                                                        <label class="p-0 m-0">
                                                                                            เลขประจำตัวผู้เสียภาษี
                                                                                        </label>
                                                                                        <input class="form-control" name="txt_customer_taxid" value="<?php echo $cusTaxId; ?>" id="txt_customer_taxid_<?php echo $cusId; ?>" type="text" />
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                                                                                <button type="button" class="btn btn-primary" onclick="submitForm('_<?php echo $cusId; ?>', '#editForm_<?php echo $cusId; ?>')">ยืนยัน</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->


            <!-- Modal -->
            <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">เพิ่มร้านค้าใหม่</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body pl-5 pr-5">
                            <form class="user pl-5 pr-5" id="addForm" method="post" action="create_customer.php">

                                <div class="row">
                                    <label class="p-0 m-0">
                                        ชื่อร้านค้า <span style="color:red;">*</span>
                                    </label>
                                    <input class="form-control" name="txt_customer_name" id="txt_customer_name" type="text" onkeyup="handleEnter(event, addForm)" />
                                </div>
                                <div class="row mt-3">
                                    <label class="p-0 m-0">
                                        โทรศัพท์
                                    </label>
                                    <input class="form-control" name="txt_customer_phone" id="txt_customer_phone" type="text" onkeyup="handleEnter(event, addForm)" />
                                </div>
                                <div class="row mt-3">
                                    <label class="p-0 m-0">
                                        ที่อยู่
                                    </label>
                                    <input class="form-control" name="txt_customer_address" id="txt_customer_address" type="text" onkeyup="handleEnter(event, addForm)" />
                                </div>
                                <div class="row mt-3">
                                    <label class="p-0 m-0">
                                        เลขประจำตัวผู้เสียภาษี
                                    </label>
                                    <input class="form-control" name="txt_customer_taxid" id="txt_customer_taxid" type="text" onkeyup="handleEnter(event, addForm)" />
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>
                            <button type="button" class="btn btn-primary" onclick="submitForm()">ยืนยัน</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php include("../footer.php"); ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="../#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin-2.min.js"></script>


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../Scripts/swal.js"></script>

    <!-- Page level plugins -->
    <script src="../js/datatables/jquery.dataTables.min.js"></script>
    <script src="../js/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "order": []
            });
            $("#result_alert").fadeOut(5000);
            $('.product_ddl').select2();
        });

        function handleEnter(event, formId) {
            if (event.key === "Enter") {
                const form = document.getElementById(formId);
                const index = [...form].indexOf(event.target);
                form.elements[index + 1].focus();
            }
        }

        function submitForm(productId = "", formId = "#addForm") {
            let customerName = $("#txt_customer_name" + productId).val();

            if (!customerName || customerName === "") {
                alertError("กรุณากรอกชื่อร้านค้า !!");
            } else {
                $(formId).submit();
            }
        }

        function deleteCustomer(cusId, cusName) {
            Swal.fire({
                icon: `warning`,
                title: `ลบร้านค้า`,
                text: `ต้องการลบร้านค้า ${cusName} ใช่หรือไม่ ?`,
                showCancelButton: true,
                confirmButtonText: "ยืนยัน",
                cancelButtonText: "ปิด",
                confirmButtonColor: "#d33",
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location = "delete_customer.php?customer_id=" + cusId;
                }
            });
        }

        function addCustomerProduct(cusId) {
            productId = $("#product_ddl_"+cusId+ " :selected").val();

            $.ajax({
                type: 'POST',
                url: 'create_customer_product.php',
                data: {
                    cus_id: cusId,
                    product_id: productId
                },
                success: function(response) {
                    console.log("response", response);
                    if (response === "success") {
                        fetchCustomerProduct(cusId);
                    } else {
                        alertError("สินค้านี้ถูกเพิ่มไปแล้ว");
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }

        function deleteCustomerProduct(cusId, productId) {
            $("#delete_cus_product_btn_"+cusId).hide();
            $.ajax({
                type: 'POST',
                url: 'delete_customer_product.php',
                data: {
                    cus_id: cusId,
                    product_id: productId
                },
                success: function(response) {
                    console.log("response", response);
                    if (response === "success") {
                        fetchCustomerProduct(cusId);
                    } else {
                        alertError("ไม่สามารถลบสินค้าได้");
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
            
        }

        function fetchCustomerProduct(cusId) {
            $.ajax({
                type: 'POST',
                url: 'read_customer_product.php',
                data: {
                    cus_id: cusId
                },
                success: function(content) {
                    $("#customer_product_table_" + cusId).html(content);
                    $("#delete_cus_product_btn_"+cusId).show();
                }
            });
        }
    </script>
</body>

</html>