<?php
@session_start();

include("../function/connect.php");
include("../function/po_data.php");

$cusId = isset($_GET["cus_id"]) ? $_GET["cus_id"] : "";
$productData = getProducts("PO", $cusId);

?>

<label class="p-0 m-0">
	ชื่อสินค้า
</label>
<select class="form-control" name="product_ddl" id="product_ddl" onchange="setUnitName()">
	<?php
	foreach ($productData as $row) {
		echo "<option value='" . $row["product_id"] . "'";
		echo ">" . $row["product_name"] . "</option>";
	}
	?>
</select>