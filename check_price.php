<?php
@session_start();
if (!isset($_SESSION["LOGGED_IN_USER"])) {
	header("location: admin.php");
}

include("function/connect.php");
$activePage = "CHECKPRICE";






?>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<!-- Custom fonts for this template-->
	<link href="css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link href="css/sb-admin-2.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<link href="css/dataTables.bootstrap4.min.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	<style type="text/css">
		td {
			font-family: Tahoma;
			font-size: 14px;
			color: #828282;
			text-decoration: none;
			border: none;
		}

		font {
			font-family: Tahoma;
			font-size: 14px;
			color: #1890E1;
			text-decoration: none;
			border: none;
			font-weight: bold;
		}

		.f {
			font-family: Tahoma;
			font-size: 14px;
			color: #FF2D00;
			text-decoration: none;
			border: none;
			font-weight: normal;
		}

		a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: bold;
			color: #FB5E3C;
			text-decoration: none;
			border: none;
		}

		.a {
			font-family: Tahoma;
			font-size: 14px;
			font-weight: normal;
			color: #FFFFFF;
			text-decoration: none;
			border: none;
		}

		.a1 {
			font-family: Tahoma;
			font-size: 14px;
			color: #40A3D8;
			font-weight: normal;
			text-decoration: none;
			border: none;
		}

		img {
			text-decoration: none;
			border: none;
		}

		.style2 {
			color: #FB5E3C
		}
	</style>
	<title>ORDER SYSTEM</title>
</head>


<body bgcolor="#FFFFFF">
	<div id="wrapper">

		<!-- Sidebar -->
		<?php include "sidebar.php"; ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column ">

			<!-- Main Content -->
			<div class="container-fluid container-md">
				<br />
				<!-- Page Heading -->
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h1 class="h3 mb-0 text-gray-800">เช็คราคาสินค้า</h1>
				</div>

				<!-- Content Row -->
				<div class="row">

					<!-- Earnings (Monthly) Card Example -->
					<div class="col-xl-12 col-md-12 mb-4">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">

								<div class="row no-gutters">
									<div class="col-md-12">
										<div class="p-0">

											<table width="90%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">

												<tr>
													<td colspan="2">&nbsp;</td>
												</tr>
												<tr>
													<td colspan="2">
														<form method="GET">
															<table>
																<tr>
																	<td width="100px">ชื่อสินค้า</td>
																	<td>
																		<select name="ddlProduct" class="form-control" id="ddlProduct">
																			<?php
																			$firstProductId = "";

																			$query = "	select * 
																						from product a 
																						where a.pd_status = '1' 
																						and not exists( select * from customer_price b 
																						where b.pd_id = a.pd_id and b.cp_status = '1' and b.cus_id = '" . $_GET["id"] . "' )";

																			$result = mysqli_query($c, $query);

																			while ($row = mysqli_fetch_array($result)) {
																				if ($firstProductId == "") {
																					$firstProductId = $row["pd_id"];
																				}

																				$selectedId = isset($_GET["ddlProduct"]) ? $_GET["ddlProduct"] : "";

																			?>
																				<option value="<?php echo $row["pd_id"]; ?>"
																					<?php if ($selectedId == $row["pd_id"]) {
																						echo " selected ";
																					}
																					?>>
																					<?php echo $row["pd_name"] . ' / ' . $row["pd_unit"]; ?>
																				</option>
																			<?php
																			}
																			?>
																		</select>
																	</td>
																	<td style="padding-left:10px;"><button type="submit" class="btn btn-dark">ค้นหา</button></td>
																</tr>
															</table>
														</form>
													</td>
												</tr>
												<tr>
													<td colspan="2">

														<br />

														<br />
														<?php
														if (isset($_GET["ddlProduct"])) {
															$productId =  $_GET["ddlProduct"];
														} else {
															$productId = $firstProductId;
														}

														$currentPage = $_SERVER["PHP_SELF"];

														$maxRows_rsmem = 20;
														$pageNum_rsmem = 0;
														if (isset($_GET['pageNum_rsmem'])) {
															$pageNum_rsmem = $_GET['pageNum_rsmem'];
														}
														$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;


														if (isset($_GET['pageNum_rsmem'])) {
															$pageNum_rsmem = $_GET['pageNum_rsmem'];
														}
														$startRow_rsmem = $pageNum_rsmem * $maxRows_rsmem;
														//คิวรี่ปกติ
														$query_rsmem = " 
																SELECT *, a.pd_id as pdid, c.cus_name
																FROM customer_price a, product b, customer c
																

																where a.cp_status = '1' and b.pd_status = '1' 
																	and a.pd_id = b.pd_id 
																	and a.cus_id = c.cus_id
																	and b.pd_id = '$productId'
																order by a.cp_id desc
																";

														$query_limit_rsmem = sprintf("%s LIMIT %d, %d", $query_rsmem, $startRow_rsmem, $maxRows_rsmem);

														$rsmem = mysqli_query($c, $query_limit_rsmem);
														$row_rsmem = mysqli_fetch_assoc($rsmem);

														if (isset($_GET['totalRows_rsmem'])) {
															$totalRows_rsmem = $_GET['totalRows_rsmem'];
														} else {
															$all_rsmem = mysqli_query($c, $query_rsmem);
															$totalRows_rsmem = mysqli_num_rows($all_rsmem);
														}
														$totalPages_rsmem = ceil($totalRows_rsmem / $maxRows_rsmem) - 1;

														$queryString_rsmem = "";
														if (!empty($_SERVER['QUERY_STRING'])) {
															$params = explode("&", $_SERVER['QUERY_STRING']);
															$newParams = array();
															foreach ($params as $param) {
																if (
																	stristr($param, "pageNum_rsmem") == false &&
																	stristr($param, "totalRows_rsmem") == false
																) {
																	array_push($newParams, $param);
																}
															}
															if (count($newParams) != 0) {
																$queryString_rsmem = "&" . htmlentities(implode("&", $newParams));
															}
														}
														$queryString_rsmem = sprintf("&totalRows_rsmem=%d%s", $totalRows_rsmem, $queryString_rsmem);

														?>
														<table id="box-table-a" width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="table table-bordered">
															<tr class="a">
																<th width="150" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>ชื่อร้านค้า</h5>
																</th>
																<th width="150" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>ชื่อสินค้า</h5>
																</th>
																<td width="127" align="right" valign="middle" style="color: white;" bgcolor="#B5A8A8">
																	<h5>ราคา</h5>
																	</th>
																<td width="89" align="center" valign="middle" style="color: white;" bgcolor="#B5A8A8">
																	<h5>หน่วย</h5>
																	</th>
																<th width="62" align="center" valign="middle" bgcolor="#B5A8A8">
																	<h5>จัดการ</h5>
																</th>
															</tr>
															<?php


															//เริ่มวน

															if ($totalRows_rsmem > 0) { // Show if recordset not empty 
															?>
																<?php do {
																	$cpId = $row_rsmem["cp_id"];
																?>
																	<tr>
																		<td>
																			<div align="left"><?php echo $row_rsmem["cus_name"]; ?></div>
																		</td>
																		<td>
																			<div align="left"><?php echo $row_rsmem["pd_name"]; ?></div>
																		</td>
																		<td align="right">
																			<div align="right" class="d-flex flex-row">
																				<span style="margin-left:auto;" id="cus_price_span_<?php echo $cpId; ?>"><?php echo $row_rsmem["cp_price"]; ?></span>
																				<input type="number" class="form-control" style="display:none;" id="cus_price_txt_<?php echo $cpId; ?>"
																					value="<?php echo $row_rsmem["cp_price"]; ?>" />
																				<button type="button" class="btn btn-success ml-1" style="display:none;"
																					onclick="saveCustomerPrice('<?php echo $cpId; ?>')" id="cus_price_btn_<?php echo $cpId; ?>">
																					<i class="fas fa-save"></i>
																				</button>
																			</div>
																		</td>
																		<td>
																			<div align="center"><?php echo $row_rsmem["pd_unit"]; ?></div>
																		</td>
																		<td>
																			<div align="center" class="style10 ">
																				<button type="button" class="btn btn-primary" width="16" height="16" style="cursor:hand"
																					onclick="editCustomerPrice('<?php echo $cpId; ?>');">
																					<i class="fas fa-pencil-alt"></i>
																				</button>
																			</div>
																		</td>
																	</tr>
																<?php } while ($row_rsmem = mysqli_fetch_assoc($rsmem)); ?>
															<?php } // Show if recordset not empty 
															?>
															<?php if ($totalRows_rsmem == 0) { // Show if recordset empty 
															?>
															<?php } // Show if recordset empty 
															?>
														</table>

														<br />
														<p align="right" class="style16">&nbsp;
															จำนวน <?php echo ($startRow_rsmem + 1) ?> ถึง <?php echo min($startRow_rsmem + $maxRows_rsmem, $totalRows_rsmem) ?> จาก <?php echo $totalRows_rsmem ?></p>
														<table border="0" width="40%" align="right">
															<tr>
																<td width="25%" height="47" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																											?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, 0, $queryString_rsmem); ?>" class="style16">หน้าแรก</a>
																	<?php } // Show if not first page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem > 0) { // Show if not first page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, max(0, $pageNum_rsmem - 1), $queryString_rsmem); ?>" class="style16">ก่อนหน้า</a>
																	<?php } // Show if not first page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, min($totalPages_rsmem, $pageNum_rsmem + 1), $queryString_rsmem); ?>" class="style16">ถัดไป</a>
																	<?php } // Show if not last page 
																	?>
																</td>
																<td width="23%" align="right"><?php if ($pageNum_rsmem < $totalPages_rsmem) { // Show if not last page 
																								?>
																		<a href="<?php printf("%s?pageNum_rsmem=%d%s", $currentPage, $totalPages_rsmem, $queryString_rsmem); ?>" class="style16">หน้าสุดท้าย</a>
																	<?php } // Show if not last page 
																	?>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
												<tr>
													<td height="100%" colspan="2"></td>
												</tr>
												<tr>
													<td colspan="2">&nbsp;
													</td>
												</tr>
											</table>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End of Main Content -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="../#page-top">
		<i class="fas fa-angle-up"></i>
	</a>


	<!-- Bootstrap core JavaScript-->
	<script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="js/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="js/sb-admin-2.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<script src="Scripts/swal.js"></script>

	<script>
		$(document).ready(function() {
			$('#ddlProduct').select2();
		});

		function handleEnter(event) {
			if (event.key === "Enter") {
				const form = document.getElementById('form1')
				const index = [...form].indexOf(event.target);
				form.elements[index + 1].focus();
				//event.preventDefault();
			}
		}


		function editCustomerPrice(id) {
			$("#cus_price_txt_" + id).show();
			$("#cus_price_btn_" + id).show();
			$("#cus_price_span_" + id).hide();
		}

		function saveCustomerPrice(id) {
			var price = $("#cus_price_txt_" + id).val();

			$.ajax({
				type: 'POST',
				url: 'service/update_customer_price.php',
				data: {
					customer_price_id: id,
					customer_price: price
				},
				success: function(response) {
					Swal.fire({
						icon: "success",
						title: "บันทึกสำเร็จ!"
					});

					$("#cus_price_txt_" + id).hide();
					$("#cus_price_btn_" + id).hide();

					$("#cus_price_span_" + id).text(price);
					$("#cus_price_span_" + id).show();
				},
				error: function(xhr, status, error) {
					console.error(xhr.responseText);
				}
			});

		}
	</script>

</body>